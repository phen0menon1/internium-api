import { PrismaClient, Prisma } from '@prisma/client';

const prisma = new PrismaClient();

const data: Prisma.SpecializationCreateInput[] = [
  {
    position: 0,
    title: 'Разработка программного обеспечения',
    abilities: {
      create: [
        {
          position: 0,
          title: 'Бэкенд разработчик',
          engTranslationTitle: 'Backend Developer',
        },
        {
          position: 1,
          title: 'Фронтенд разработчик',
          engTranslationTitle: 'Frontend Developer',
        },
        {
          position: 2,
          title: 'Веб-разработчик',
          engTranslationTitle: 'Web Developer',
        },
        {
          position: 3,
          title: 'Фулстек разработчик',
          engTranslationTitle: 'Fullstack Developer',
        },
        {
          position: 4,
          title: 'Разработчик мобильных приложений',
          engTranslationTitle: 'Mobile Application Developer',
        },
        {
          position: 5,
          title: 'Разработчик приложений',
          engTranslationTitle: 'Application Developer',
        },
        {
          position: 6,
          title: 'Разработчик игр',
          engTranslationTitle: 'Game Developer',
        },
        {
          position: 7,
          title: 'Десктоп разработчик',
          engTranslationTitle: 'Software Developer',
        },
        {
          position: 8,
          title: 'Разработчик баз данных',
          engTranslationTitle: 'Database Developer',
        },
        {
          position: 9,
          title: 'HTML-верстальщик',
          engTranslationTitle: 'HTML Coding',
        },
        {
          position: 10,
          title: 'Инженер встраиваемых систем',
          engTranslationTitle: 'Embedded Software Engineer',
        },
        {
          position: 11,
          title: 'Программист 1С',
          engTranslationTitle: '1C Developer',
        },
        {
          position: 12,
          title: 'Системный инженер',
          engTranslationTitle: 'System Software Engineer',
        },
        {
          position: 13,
          title: 'Архитектор программного обеспечения',
          engTranslationTitle: 'Software Architect',
        },
        {
          position: 14,
          title: 'ERP-программист',
          engTranslationTitle: 'ERP Developer',
        },
        {
          position: 15,
          title: 'Технический директор',
          engTranslationTitle: 'Chief Technology Officer (CTO)',
        },
        {
          position: 16,
          title: 'Архитектор баз данных',
          engTranslationTitle: 'Database Architect',
        },
        {
          position: 17,
          title: 'Другое',
          engTranslationTitle: 'Other',
        },
      ],
    },
    engTranslationTitle: 'Software Development',
  },
  {
    position: 1,
    title: 'Контроль качества, тестирование',
    abilities: {
      create: [
        {
          position: 0,
          title: 'Инженер по ручному тестированию',
          engTranslationTitle: 'Manual Test Engineer',
        },
        {
          position: 1,
          title: 'Инженер по автомат. тестированию',
          engTranslationTitle: 'Test Automation Engineer',
        },
        {
          position: 2,
          title: 'Инженер по обеспечению качества',
          engTranslationTitle: 'Quality Assurance Engineer',
        },
        {
          position: 3,
          title: 'UX-тестировщик',
          engTranslationTitle: 'UX Tester',
        },
        {
          position: 4,
          title: 'Инженер по производительности',
          engTranslationTitle: 'Software Performance Engineer',
        },
        {
          position: 5,
          title: 'Аналитик по обеспечению качества',
          engTranslationTitle: 'Quality Assurance Analyst',
        },
        {
          position: 6,
          title: 'Менеджер по обеспечению качества',
          engTranslationTitle: 'Quality Assurance Manager',
        },
        {
          position: 7,
          title: 'Директор по обеспечению качества',
          engTranslationTitle: 'Quality Assurance Director',
        },
        {
          position: 8,
          title: 'Другое',
          engTranslationTitle: 'Other',
        },
      ],
    },
    engTranslationTitle: 'Quality Assurance',
  },
  {
    position: 2,
    title: 'Администрирование',
    abilities: {
      create: [
        {
          position: 0,
          title: 'Девопс инженер',
          engTranslationTitle: 'DevOps',
        },
        {
          position: 1,
          title: 'Системный администратор',
          engTranslationTitle: 'System Administration',
        },
        {
          position: 2,
          title: 'Администратор серверов',
          engTranslationTitle: 'Server Administrator',
        },
        {
          position: 3,
          title: 'Администратор баз данных',
          engTranslationTitle: 'Database Administrator',
        },
        {
          position: 4,
          title: 'Инженер по данным',
          engTranslationTitle: 'Data Engineer',
        },
        {
          position: 5,
          title: 'Администратор сетей',
          engTranslationTitle: 'Network Administrator',
        },
        {
          position: 6,
          title: 'Администратор сайта',
          engTranslationTitle: 'Site Administrator',
        },
        {
          position: 7,
          title: 'Другое',
          engTranslationTitle: 'Other',
        },
      ],
    },
    engTranslationTitle: 'Administration',
  },
  {
    position: 3,
    title: 'Дизайн',
    abilities: {
      create: [
        {
          position: 0,
          title: 'Продуктовый дизайнер',
          engTranslationTitle: 'Product Designer',
        },
        {
          position: 1,
          title: 'UI/UX дизайнер',
          engTranslationTitle: 'UI/UX Designer',
        },
        {
          position: 2,
          title: 'Веб дизайнер',
          engTranslationTitle: 'Web Designer',
        },
        {
          position: 3,
          title: 'Графический дизайнер',
          engTranslationTitle: 'Graphic Designer',
        },
        {
          position: 4,
          title: 'Дизайнер приложений',
          engTranslationTitle: 'Application Designer',
        },
        {
          position: 5,
          title: 'Дизайнер иллюстратор',
          engTranslationTitle: 'Designer Illustrator',
        },
        {
          position: 6,
          title: 'Дизайнер игр',
          engTranslationTitle: 'Game Designer',
        },
        {
          position: 7,
          title: 'Нарративный дизайнер',
          engTranslationTitle: 'Narrative Designer',
        },
        {
          position: 8,
          title: 'Flash-аниматор',
          engTranslationTitle: 'Flash Animator',
        },
        {
          position: 9,
          title: '3d-аниматор',
          engTranslationTitle: '3d Animator',
        },
        {
          position: 10,
          title: 'Моушен дизайнер',
          engTranslationTitle: 'Motion Designer',
        },
        {
          position: 11,
          title: '3d моделлер',
          engTranslationTitle: '3d Modeler',
        },
        {
          position: 12,
          title: 'Художник компьютерной графики',
          engTranslationTitle: 'Computer Graphics Artist',
        },
        {
          position: 13,
          title: 'Арт директор',
          engTranslationTitle: 'Art Director',
        },
        {
          position: 14,
          title: 'Другое',
          engTranslationTitle: 'Other',
        },
      ],
    },
    engTranslationTitle: 'Design',
  },
  {
    position: 4,
    title: 'Менеджмент',
    abilities: {
      create: [
        {
          position: 0,
          title: 'Менеджер проекта',
          engTranslationTitle: 'Project Manager',
        },
        {
          position: 1,
          title: 'Директор проекта',
          engTranslationTitle: 'Project Director',
        },
        {
          position: 2,
          title: 'Менеджер продукта',
          engTranslationTitle: 'Product Manager',
        },
        {
          position: 3,
          title: 'Scrum-мастер',
          engTranslationTitle: 'Scrum Master',
        },
        {
          position: 4,
          title: 'Директор продукта',
          engTranslationTitle: 'Product Director',
        },
        {
          position: 5,
          title: 'Менеджер сообщества',
          engTranslationTitle: 'Community manager',
        },
        {
          position: 6,
          title: 'Релиз менеджер',
          engTranslationTitle: 'Release Manager',
        },
        {
          position: 7,
          title: 'Программный менеджер',
          engTranslationTitle: 'Program Manager',
        },
        {
          position: 8,
          title: 'Директор по информационным технологиям',
          engTranslationTitle: 'Chief information officer (CIO)',
        },
        {
          position: 9,
          title: 'Исполнительный директор',
          engTranslationTitle: 'Chief Operating Officer (COO)',
        },
        {
          position: 10,
          title: 'Генеральный директор',
          engTranslationTitle: 'Chief Executive Officer (CEO)',
        },
        {
          position: 11,
          title: 'Финансовый директор',
          engTranslationTitle: 'Chief Financial Officer (CFO)',
        },
        {
          position: 12,
          title: 'Другое',
          engTranslationTitle: 'Other',
        },
      ],
    },
    engTranslationTitle: 'Management',
  },
  {
    position: 5,
    title: 'Аналитика',
    abilities: {
      create: [
        {
          position: 0,
          title: 'Веб аналитик',
          engTranslationTitle: 'Web Analyst',
        },
        {
          position: 1,
          title: 'Системный аналитик',
          engTranslationTitle: 'Systems Analyst',
        },
        {
          position: 2,
          title: 'Аналитик мобильных приложений',
          engTranslationTitle: 'Mobile Analyst',
        },
        {
          position: 3,
          title: 'Бизнес-аналитик',
          engTranslationTitle: 'Business Analyst',
        },
        {
          position: 4,
          title: 'Аналитик по данным',
          engTranslationTitle: 'Data Analyst',
        },
        {
          position: 5,
          title: 'Гейм-аналитик',
          engTranslationTitle: 'Game Analyst',
        },
        {
          position: 6,
          title: 'Учёный по данным',
          engTranslationTitle: 'Data Scientist',
        },
        {
          position: 7,
          title: 'Инженер по данным',
          engTranslationTitle: 'Data Engineer',
        },
        {
          position: 8,
          title: 'Программный аналитик',
          engTranslationTitle: 'Software Analyst',
        },
        {
          position: 9,
          title: 'Продуктовый аналитик',
          engTranslationTitle: 'Product Analyst',
        },
        {
          position: 10,
          title: 'BI-разработчик',
          engTranslationTitle: 'BI Developer',
        },
        {
          position: 11,
          title: 'Веб-аналитик',
          engTranslationTitle: 'Web Analyst',
        },
        {
          position: 12,
          title: 'Другое',
          engTranslationTitle: 'Other',
        },
      ],
    },
    engTranslationTitle: 'Analytics',
  },
  {
    position: 6,
    title: 'Кадры',
    abilities: {
      create: [
        {
          position: 0,
          title: 'Менеджер по персоналу',
          engTranslationTitle: 'HR Manager',
        },
        {
          position: 1,
          title: 'Менеджер по найму',
          engTranslationTitle: 'Recruitment Manager',
        },
        {
          position: 2,
          title: 'Директор по персоналу',
          engTranslationTitle: 'HR Director',
        },
        {
          position: 3,
          title: 'Директор по найму',
          engTranslationTitle: 'Recruitment Director',
        },
        {
          position: 4,
          title: 'Аналитик по персоналу',
          engTranslationTitle: 'HR Analyst',
        },
        {
          position: 5,
          title: 'Другое',
          engTranslationTitle: 'Other',
        },
      ],
    },
    engTranslationTitle: 'Human Resources',
  },
  {
    position: 7,
    title: 'Поддержка',
    abilities: {
      create: [
        {
          position: 0,
          title: 'Инженер технической поддержки',
          engTranslationTitle: 'Technical Support Engineer',
        },
        {
          position: 1,
          title: 'Менеджер технической поддержки',
          engTranslationTitle: 'Technical Support Manager',
        },
        {
          position: 2,
          title: 'Директор технической поддержки',
          engTranslationTitle: 'Technical Support Director',
        },
        {
          position: 3,
          title: 'Аналитик технической поддержки',
          engTranslationTitle: 'Technical Support Analyst',
        },
        {
          position: 4,
          title: 'Менеджер по обслуживанию клиентов',
          engTranslationTitle: 'Customer Service Manager',
        },
        {
          position: 5,
          title: 'Директор по обслуживанию клиентов',
          engTranslationTitle: 'Customer Service Director',
        },
        {
          position: 6,
          title: 'Модератор',
          engTranslationTitle: 'Moderator',
        },
        {
          position: 7,
          title: 'Другое',
          engTranslationTitle: 'Other',
        },
      ],
    },
    engTranslationTitle: 'Support',
  },
  {
    position: 8,
    title: 'Маркетинг',
    abilities: {
      create: [
        {
          position: 0,
          title: 'Менеджер по маркетингу',
          engTranslationTitle: 'Marketing Manager',
        },
        {
          position: 1,
          title: 'Директор по маркетингу',
          engTranslationTitle: 'Marketing Director',
        },
        {
          position: 2,
          title: 'Маркетинговый аналитик',
          engTranslationTitle: 'Marketing Analyst',
        },
        {
          position: 3,
          title: 'SEO-специалист',
          engTranslationTitle: 'SEO Specialist',
        },
        {
          position: 4,
          title: 'SMM-специалист',
          engTranslationTitle: 'SMM Specialist',
        },
        {
          position: 5,
          title: 'Таргетолог',
          engTranslationTitle: 'Targetologist',
        },
        {
          position: 6,
          title: 'Другое',
          engTranslationTitle: 'Other',
        },
      ],
    },
    engTranslationTitle: 'Marketing',
  },
  {
    position: 9,
    title: 'Контент',
    abilities: {
      create: [
        {
          position: 0,
          title: 'Технический писатель',
          engTranslationTitle: 'Technical Writer',
        },
        {
          position: 1,
          title: 'Создатель контента',
          engTranslationTitle: 'Content Writer',
        },
        {
          position: 2,
          title: 'Менеджер по контенту',
          engTranslationTitle: 'Content Manager',
        },
        {
          position: 3,
          title: 'Директор по контенту',
          engTranslationTitle: 'Content Director',
        },
        {
          position: 4,
          title: 'Копирайтер',
          engTranslationTitle: 'Copywriter',
        },
        {
          position: 5,
          title: 'Другое',
          engTranslationTitle: 'Other',
        },
      ],
    },
    engTranslationTitle: 'Content',
  },
  {
    position: 10,
    title: 'Продажи',
    abilities: {
      create: [
        {
          position: 0,
          title: 'Менеджер по работе с клиентами',
          engTranslationTitle: 'Account Manager',
        },
        {
          position: 1,
          title: 'Директор по работе с клиентами',
          engTranslationTitle: 'Account Director',
        },
        {
          position: 2,
          title: 'Менеджер по продажам',
          engTranslationTitle: 'Sales manager',
        },
        {
          position: 3,
          title: 'Директор по продажам',
          engTranslationTitle: 'Sales Director',
        },
        {
          position: 4,
          title: 'Аналитик продаж',
          engTranslationTitle: 'Sales Analyst',
        },
        {
          position: 5,
          title: 'Другое',
          engTranslationTitle: 'Other',
        },
      ],
    },
    engTranslationTitle: 'Sales',
  },
  {
    position: 11,
    title: 'Информационная безопасность',
    abilities: {
      create: [
        {
          position: 0,
          title: 'Пентестер',
          engTranslationTitle: 'Pentester',
        },
        {
          position: 1,
          title: 'Администратор защиты',
          engTranslationTitle: 'Security Administrator',
        },
        {
          position: 2,
          title: 'Специалист по реверс-инжинирингу',
          engTranslationTitle: 'Reverse Engineer',
        },
        {
          position: 3,
          title: 'Инженер по безопасности',
          engTranslationTitle: 'Security Engineer',
        },
        {
          position: 4,
          title: 'Антифрод аналитик',
          engTranslationTitle: 'Antifraud Analyst',
        },
        {
          position: 5,
          title: 'Другое',
          engTranslationTitle: 'Other',
        },
      ],
    },
    engTranslationTitle: 'Information Security',
  },
  {
    position: 12,
    title: 'Офис',
    abilities: {
      create: [
        {
          position: 0,
          title: 'Офис менеджер',
          engTranslationTitle: 'Office Manager',
        },
        {
          position: 1,
          title: 'Бухгалтер',
          engTranslationTitle: 'Accountant',
        },
        {
          position: 2,
          title: 'Юрист',
          engTranslationTitle: 'Lawyer',
        },
        {
          position: 3,
          title: 'Другое',
          engTranslationTitle: 'Other',
        },
      ],
    },
    engTranslationTitle: 'Office',
  },
];

export async function seedSpecializations() {
  console.log('Started seeding for specializations & abilities');
  data.forEach(async (item) => {
    const specializationExist = await prisma.specialization.findFirst({
      where: { title: item.title },
    });
    if (specializationExist) return;
    await prisma.specialization.create({ data: item });
  });
  console.log('Seeding success for specializations & abilities');
}

async function removeSpecializations() {
  await prisma.vacancyAbility.deleteMany({});
  await prisma.ability.deleteMany({});
  await prisma.specialization.deleteMany({});
}

export async function startSpecializationsSeeding(shouldClearExisting = false) {
  try {
    if (shouldClearExisting) {
      await removeSpecializations();
    }
    await seedSpecializations();
  } catch (e) {
    console.error(e);
    process.exit(1);
  } finally {
    await prisma.$disconnect();
  }
}
