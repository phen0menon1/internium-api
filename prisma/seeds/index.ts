import { startSpecializationsSeeding } from './abilities-and-specialization';

const shouldClearExistingSpecializations = Boolean(process.env.SHOULD_CLEAR_EXISTING_SPECIALIZATIONS || false);

(async () => {
  await startSpecializationsSeeding(shouldClearExistingSpecializations);
})();
