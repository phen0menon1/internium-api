-- AlterTable
ALTER TABLE "Company" ADD COLUMN     "hidden" BOOLEAN NOT NULL DEFAULT false,
ADD COLUMN     "shortDescription" VARCHAR(255),
ADD COLUMN     "website" VARCHAR(100);
