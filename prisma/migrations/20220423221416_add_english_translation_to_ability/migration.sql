/*
  Warnings:

  - Added the required column `engTranslationTitle` to the `Ability` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE "Ability" ADD COLUMN     "engTranslationTitle" VARCHAR(100) NOT NULL;
