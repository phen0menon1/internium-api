-- CreateTable
CREATE TABLE "VacancyInvitation" (
    "id" SERIAL NOT NULL,
    "vacancyId" INTEGER NOT NULL,
    "internId" INTEGER NOT NULL,
    "archived" BOOLEAN NOT NULL DEFAULT false,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,

    CONSTRAINT "VacancyInvitation_pkey" PRIMARY KEY ("id")
);

-- CreateIndex
CREATE UNIQUE INDEX "VacancyInvitation_internId_vacancyId_key" ON "VacancyInvitation"("internId", "vacancyId");

-- AddForeignKey
ALTER TABLE "VacancyInvitation" ADD CONSTRAINT "VacancyInvitation_internId_fkey" FOREIGN KEY ("internId") REFERENCES "Intern"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "VacancyInvitation" ADD CONSTRAINT "VacancyInvitation_vacancyId_fkey" FOREIGN KEY ("vacancyId") REFERENCES "Vacancy"("id") ON DELETE RESTRICT ON UPDATE CASCADE;
