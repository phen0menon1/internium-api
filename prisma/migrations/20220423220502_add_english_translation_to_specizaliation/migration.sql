/*
  Warnings:

  - Added the required column `engTranslationTitle` to the `Specialization` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE "Specialization" ADD COLUMN     "engTranslationTitle" VARCHAR(100) NOT NULL;
