/*
  Warnings:

  - Added the required column `phone` to the `VerificationMessage` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE "VerificationMessage" ADD COLUMN     "phone" VARCHAR(32) NOT NULL;
