/*
  Warnings:

  - Added the required column `message` to the `VacancyAnnouncement` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE "VacancyAnnouncement" ADD COLUMN     "message" VARCHAR(2048) NOT NULL;
