/*
  Warnings:

  - A unique constraint covering the columns `[internToVacancyInternId,internToVacancyVacancyId]` on the table `InternReactions` will be added. If there are existing duplicate values, this will fail.
  - A unique constraint covering the columns `[internToVacancyInternId,internToVacancyVacancyId]` on the table `VacancyInvitation` will be added. If there are existing duplicate values, this will fail.

*/
-- CreateIndex
CREATE UNIQUE INDEX "InternReactions_internToVacancyInternId_internToVacancyVaca_key" ON "InternReactions"("internToVacancyInternId", "internToVacancyVacancyId");

-- CreateIndex
CREATE UNIQUE INDEX "VacancyInvitation_internToVacancyInternId_internToVacancyVa_key" ON "VacancyInvitation"("internToVacancyInternId", "internToVacancyVacancyId");
