-- AlterTable
ALTER TABLE "CompanyUserInfo" ADD COLUMN     "avatarId" INTEGER;

-- AddForeignKey
ALTER TABLE "CompanyUserInfo" ADD CONSTRAINT "CompanyUserInfo_avatarId_fkey" FOREIGN KEY ("avatarId") REFERENCES "Avatar"("id") ON DELETE SET NULL ON UPDATE CASCADE;
