/*
  Warnings:

  - You are about to drop the column `avatarId` on the `Company` table. All the data in the column will be lost.

*/
-- DropForeignKey
ALTER TABLE "Company" DROP CONSTRAINT "Company_avatarId_fkey";

-- AlterTable
ALTER TABLE "Company" DROP COLUMN "avatarId",
ADD COLUMN     "logoId" INTEGER;

-- AddForeignKey
ALTER TABLE "Company" ADD CONSTRAINT "Company_logoId_fkey" FOREIGN KEY ("logoId") REFERENCES "Avatar"("id") ON DELETE SET NULL ON UPDATE CASCADE;
