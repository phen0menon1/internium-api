/*
  Warnings:

  - You are about to drop the column `internId` on the `InternReactions` table. All the data in the column will be lost.
  - You are about to drop the column `vacancyId` on the `InternReactions` table. All the data in the column will be lost.
  - You are about to drop the column `internId` on the `VacancyInvitation` table. All the data in the column will be lost.
  - You are about to drop the column `vacancyId` on the `VacancyInvitation` table. All the data in the column will be lost.
  - Added the required column `internToVacancyInternId` to the `InternReactions` table without a default value. This is not possible if the table is not empty.
  - Added the required column `internToVacancyVacancyId` to the `InternReactions` table without a default value. This is not possible if the table is not empty.
  - Added the required column `internToVacancyInternId` to the `VacancyInvitation` table without a default value. This is not possible if the table is not empty.
  - Added the required column `internToVacancyVacancyId` to the `VacancyInvitation` table without a default value. This is not possible if the table is not empty.

*/
-- DropForeignKey
ALTER TABLE "InternReactions" DROP CONSTRAINT "InternReactions_internId_fkey";

-- DropForeignKey
ALTER TABLE "InternReactions" DROP CONSTRAINT "InternReactions_vacancyId_fkey";

-- DropForeignKey
ALTER TABLE "VacancyInvitation" DROP CONSTRAINT "VacancyInvitation_internId_fkey";

-- DropForeignKey
ALTER TABLE "VacancyInvitation" DROP CONSTRAINT "VacancyInvitation_vacancyId_fkey";

-- DropIndex
DROP INDEX "InternReactions_internId_vacancyId_key";

-- DropIndex
DROP INDEX "VacancyInvitation_internId_vacancyId_key";

-- AlterTable
ALTER TABLE "InternReactions" DROP COLUMN "internId",
DROP COLUMN "vacancyId",
ADD COLUMN     "archived" BOOLEAN NOT NULL DEFAULT false,
ADD COLUMN     "internToVacancyInternId" INTEGER NOT NULL,
ADD COLUMN     "internToVacancyVacancyId" INTEGER NOT NULL,
ADD COLUMN     "rejected" BOOLEAN NOT NULL DEFAULT false;

-- AlterTable
ALTER TABLE "VacancyInvitation" DROP COLUMN "internId",
DROP COLUMN "vacancyId",
ADD COLUMN     "internToVacancyInternId" INTEGER NOT NULL,
ADD COLUMN     "internToVacancyVacancyId" INTEGER NOT NULL;

-- CreateTable
CREATE TABLE "InternToVacancy" (
    "internId" INTEGER NOT NULL,
    "vacancyId" INTEGER NOT NULL,

    CONSTRAINT "InternToVacancy_pkey" PRIMARY KEY ("internId","vacancyId")
);

-- AddForeignKey
ALTER TABLE "InternToVacancy" ADD CONSTRAINT "InternToVacancy_internId_fkey" FOREIGN KEY ("internId") REFERENCES "Intern"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "InternToVacancy" ADD CONSTRAINT "InternToVacancy_vacancyId_fkey" FOREIGN KEY ("vacancyId") REFERENCES "Vacancy"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "InternReactions" ADD CONSTRAINT "InternReactions_internToVacancyInternId_internToVacancyVac_fkey" FOREIGN KEY ("internToVacancyInternId", "internToVacancyVacancyId") REFERENCES "InternToVacancy"("internId", "vacancyId") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "VacancyInvitation" ADD CONSTRAINT "VacancyInvitation_internToVacancyInternId_internToVacancyV_fkey" FOREIGN KEY ("internToVacancyInternId", "internToVacancyVacancyId") REFERENCES "InternToVacancy"("internId", "vacancyId") ON DELETE RESTRICT ON UPDATE CASCADE;
