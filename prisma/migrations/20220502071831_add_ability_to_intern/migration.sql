-- CreateTable
CREATE TABLE "InternAbility" (
    "id" SERIAL NOT NULL,
    "internId" INTEGER NOT NULL,
    "abilityId" INTEGER NOT NULL,

    CONSTRAINT "InternAbility_pkey" PRIMARY KEY ("id")
);

-- AddForeignKey
ALTER TABLE "InternAbility" ADD CONSTRAINT "InternAbility_internId_fkey" FOREIGN KEY ("internId") REFERENCES "Intern"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "InternAbility" ADD CONSTRAINT "InternAbility_abilityId_fkey" FOREIGN KEY ("abilityId") REFERENCES "Ability"("id") ON DELETE RESTRICT ON UPDATE CASCADE;
