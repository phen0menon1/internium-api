-- CreateTable
CREATE TABLE "VerificationMessage" (
    "id" SERIAL NOT NULL,
    "code" VARCHAR(6) NOT NULL,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "verified" BOOLEAN NOT NULL,

    CONSTRAINT "VerificationMessage_pkey" PRIMARY KEY ("id")
);
