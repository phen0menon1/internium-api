/*
  Warnings:

  - You are about to drop the column `vacancyChatRoomId` on the `VacancyChatMessage` table. All the data in the column will be lost.
  - Added the required column `companyUserId` to the `VacancyChatMessage` table without a default value. This is not possible if the table is not empty.
  - Added the required column `content` to the `VacancyChatMessage` table without a default value. This is not possible if the table is not empty.
  - Added the required column `roomId` to the `VacancyChatMessage` table without a default value. This is not possible if the table is not empty.

*/
-- DropForeignKey
ALTER TABLE "VacancyChatMessage" DROP CONSTRAINT "VacancyChatMessage_vacancyChatRoomId_fkey";

-- AlterTable
ALTER TABLE "VacancyChatMessage" DROP COLUMN "vacancyChatRoomId",
ADD COLUMN     "companyUserId" INTEGER NOT NULL,
ADD COLUMN     "content" VARCHAR(1024) NOT NULL,
ADD COLUMN     "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
ADD COLUMN     "isRead" BOOLEAN NOT NULL DEFAULT false,
ADD COLUMN     "roomId" INTEGER NOT NULL;

-- AddForeignKey
ALTER TABLE "VacancyChatMessage" ADD CONSTRAINT "VacancyChatMessage_companyUserId_fkey" FOREIGN KEY ("companyUserId") REFERENCES "CompanyUser"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "VacancyChatMessage" ADD CONSTRAINT "VacancyChatMessage_roomId_fkey" FOREIGN KEY ("roomId") REFERENCES "VacancyChatRoom"("id") ON DELETE RESTRICT ON UPDATE CASCADE;
