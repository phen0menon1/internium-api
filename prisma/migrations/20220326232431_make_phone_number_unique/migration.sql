/*
  Warnings:

  - A unique constraint covering the columns `[phone]` on the table `Intern` will be added. If there are existing duplicate values, this will fail.

*/
-- CreateIndex
CREATE UNIQUE INDEX "Intern_phone_key" ON "Intern"("phone");
