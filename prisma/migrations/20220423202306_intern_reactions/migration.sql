-- CreateTable
CREATE TABLE "InternReactions" (
    "id" SERIAL NOT NULL,
    "internId" INTEGER NOT NULL,
    "vacancyId" INTEGER NOT NULL,

    CONSTRAINT "InternReactions_pkey" PRIMARY KEY ("id")
);

-- CreateIndex
CREATE INDEX "InternReactions_internId_vacancyId_idx" ON "InternReactions"("internId", "vacancyId");

-- AddForeignKey
ALTER TABLE "InternReactions" ADD CONSTRAINT "InternReactions_internId_fkey" FOREIGN KEY ("internId") REFERENCES "Intern"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "InternReactions" ADD CONSTRAINT "InternReactions_vacancyId_fkey" FOREIGN KEY ("vacancyId") REFERENCES "Vacancy"("id") ON DELETE RESTRICT ON UPDATE CASCADE;
