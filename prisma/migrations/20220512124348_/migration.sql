/*
  Warnings:

  - A unique constraint covering the columns `[vacancyId,internId]` on the table `VacancyChatRoom` will be added. If there are existing duplicate values, this will fail.

*/
-- CreateIndex
CREATE UNIQUE INDEX "VacancyChatRoom_vacancyId_internId_key" ON "VacancyChatRoom"("vacancyId", "internId");
