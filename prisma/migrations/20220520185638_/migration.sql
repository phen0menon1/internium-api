/*
  Warnings:

  - You are about to drop the column `internId` on the `VacancyChatRoom` table. All the data in the column will be lost.
  - You are about to drop the column `vacancyId` on the `VacancyChatRoom` table. All the data in the column will be lost.
  - Added the required column `internToVacancyId` to the `VacancyChatRoom` table without a default value. This is not possible if the table is not empty.

*/
-- DropForeignKey
ALTER TABLE "VacancyChatRoom" DROP CONSTRAINT "VacancyChatRoom_internId_fkey";

-- DropForeignKey
ALTER TABLE "VacancyChatRoom" DROP CONSTRAINT "VacancyChatRoom_vacancyId_fkey";

-- DropIndex
DROP INDEX "VacancyChatRoom_vacancyId_internId_key";

-- AlterTable
ALTER TABLE "VacancyChatRoom" DROP COLUMN "internId",
DROP COLUMN "vacancyId",
ADD COLUMN     "internToVacancyId" INTEGER NOT NULL;

-- AddForeignKey
ALTER TABLE "VacancyChatRoom" ADD CONSTRAINT "VacancyChatRoom_internToVacancyId_fkey" FOREIGN KEY ("internToVacancyId") REFERENCES "InternToVacancy"("id") ON DELETE RESTRICT ON UPDATE CASCADE;
