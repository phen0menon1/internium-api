/*
  Warnings:

  - You are about to drop the column `companyUserInfoId` on the `CompanyUser` table. All the data in the column will be lost.
  - A unique constraint covering the columns `[userInfoId]` on the table `CompanyUser` will be added. If there are existing duplicate values, this will fail.

*/
-- DropForeignKey
ALTER TABLE "CompanyUser" DROP CONSTRAINT "CompanyUser_companyUserInfoId_fkey";

-- DropIndex
DROP INDEX "CompanyUser_companyUserInfoId_key";

-- AlterTable
ALTER TABLE "CompanyUser" DROP COLUMN "companyUserInfoId",
ADD COLUMN     "userInfoId" INTEGER;

-- CreateIndex
CREATE UNIQUE INDEX "CompanyUser_userInfoId_key" ON "CompanyUser"("userInfoId");

-- AddForeignKey
ALTER TABLE "CompanyUser" ADD CONSTRAINT "CompanyUser_userInfoId_fkey" FOREIGN KEY ("userInfoId") REFERENCES "CompanyUserInfo"("id") ON DELETE SET NULL ON UPDATE CASCADE;
