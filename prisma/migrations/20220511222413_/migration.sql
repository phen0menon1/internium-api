/*
  Warnings:

  - You are about to drop the column `avatar` on the `Intern` table. All the data in the column will be lost.

*/
-- AlterTable
ALTER TABLE "Intern" DROP COLUMN "avatar",
ADD COLUMN     "avatarId" INTEGER;

-- CreateTable
CREATE TABLE "Avatar" (
    "id" SERIAL NOT NULL,
    "filename" VARCHAR(255) NOT NULL,
    "path" VARCHAR(255) NOT NULL,
    "mimetype" VARCHAR(100) NOT NULL,

    CONSTRAINT "Avatar_pkey" PRIMARY KEY ("id")
);

-- AddForeignKey
ALTER TABLE "Intern" ADD CONSTRAINT "Intern_avatarId_fkey" FOREIGN KEY ("avatarId") REFERENCES "Avatar"("id") ON DELETE SET NULL ON UPDATE CASCADE;
