-- CreateTable
CREATE TABLE "VacancyAnnouncement" (
    "id" SERIAL NOT NULL,
    "vacancyId" INTEGER NOT NULL,
    "companyUserId" INTEGER NOT NULL,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,

    CONSTRAINT "VacancyAnnouncement_pkey" PRIMARY KEY ("id")
);

-- AddForeignKey
ALTER TABLE "VacancyAnnouncement" ADD CONSTRAINT "VacancyAnnouncement_companyUserId_fkey" FOREIGN KEY ("companyUserId") REFERENCES "CompanyUser"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "VacancyAnnouncement" ADD CONSTRAINT "VacancyAnnouncement_vacancyId_fkey" FOREIGN KEY ("vacancyId") REFERENCES "Vacancy"("id") ON DELETE RESTRICT ON UPDATE CASCADE;
