/*
  Warnings:

  - You are about to drop the column `internToVacancyInternId` on the `InternReactions` table. All the data in the column will be lost.
  - You are about to drop the column `internToVacancyVacancyId` on the `InternReactions` table. All the data in the column will be lost.
  - The primary key for the `InternToVacancy` table will be changed. If it partially fails, the table could be left without primary key constraint.
  - You are about to drop the column `internToVacancyInternId` on the `VacancyInvitation` table. All the data in the column will be lost.
  - You are about to drop the column `internToVacancyVacancyId` on the `VacancyInvitation` table. All the data in the column will be lost.
  - A unique constraint covering the columns `[vacancyId,internId]` on the table `InternToVacancy` will be added. If there are existing duplicate values, this will fail.
  - Added the required column `internToVacancyId` to the `InternReactions` table without a default value. This is not possible if the table is not empty.
  - Added the required column `internToVacancyId` to the `VacancyInvitation` table without a default value. This is not possible if the table is not empty.

*/
-- DropForeignKey
ALTER TABLE "InternReactions" DROP CONSTRAINT "InternReactions_internToVacancyInternId_internToVacancyVac_fkey";

-- DropForeignKey
ALTER TABLE "VacancyInvitation" DROP CONSTRAINT "VacancyInvitation_internToVacancyInternId_internToVacancyV_fkey";

-- DropIndex
DROP INDEX "InternReactions_internToVacancyInternId_internToVacancyVaca_key";

-- DropIndex
DROP INDEX "VacancyInvitation_internToVacancyInternId_internToVacancyVa_key";

-- AlterTable
ALTER TABLE "InternReactions" DROP COLUMN "internToVacancyInternId",
DROP COLUMN "internToVacancyVacancyId",
ADD COLUMN     "internToVacancyId" INTEGER NOT NULL;

-- AlterTable
ALTER TABLE "InternToVacancy" DROP CONSTRAINT "InternToVacancy_pkey",
ADD COLUMN     "id" SERIAL NOT NULL,
ADD CONSTRAINT "InternToVacancy_pkey" PRIMARY KEY ("id");

-- AlterTable
ALTER TABLE "VacancyInvitation" DROP COLUMN "internToVacancyInternId",
DROP COLUMN "internToVacancyVacancyId",
ADD COLUMN     "internToVacancyId" INTEGER NOT NULL;

-- CreateIndex
CREATE UNIQUE INDEX "InternToVacancy_vacancyId_internId_key" ON "InternToVacancy"("vacancyId", "internId");

-- AddForeignKey
ALTER TABLE "InternReactions" ADD CONSTRAINT "InternReactions_internToVacancyId_fkey" FOREIGN KEY ("internToVacancyId") REFERENCES "InternToVacancy"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "VacancyInvitation" ADD CONSTRAINT "VacancyInvitation_internToVacancyId_fkey" FOREIGN KEY ("internToVacancyId") REFERENCES "InternToVacancy"("id") ON DELETE RESTRICT ON UPDATE CASCADE;
