-- CreateTable
CREATE TABLE "VacancyChatRoom" (
    "id" SERIAL NOT NULL,
    "vacancyId" INTEGER NOT NULL,
    "internId" INTEGER NOT NULL,

    CONSTRAINT "VacancyChatRoom_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "VacancyChatMessage" (
    "id" SERIAL NOT NULL,
    "vacancyChatRoomId" INTEGER NOT NULL,
    "internId" INTEGER,
    "companyUserId" INTEGER,
    "vacancyId" INTEGER,

    CONSTRAINT "VacancyChatMessage_pkey" PRIMARY KEY ("id")
);

-- AddForeignKey
ALTER TABLE "VacancyChatRoom" ADD CONSTRAINT "VacancyChatRoom_internId_fkey" FOREIGN KEY ("internId") REFERENCES "Intern"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "VacancyChatRoom" ADD CONSTRAINT "VacancyChatRoom_vacancyId_fkey" FOREIGN KEY ("vacancyId") REFERENCES "Vacancy"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "VacancyChatMessage" ADD CONSTRAINT "VacancyChatMessage_internId_fkey" FOREIGN KEY ("internId") REFERENCES "Intern"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "VacancyChatMessage" ADD CONSTRAINT "VacancyChatMessage_companyUserId_fkey" FOREIGN KEY ("companyUserId") REFERENCES "CompanyUser"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "VacancyChatMessage" ADD CONSTRAINT "VacancyChatMessage_vacancyId_fkey" FOREIGN KEY ("vacancyId") REFERENCES "Vacancy"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "VacancyChatMessage" ADD CONSTRAINT "VacancyChatMessage_vacancyChatRoomId_fkey" FOREIGN KEY ("vacancyChatRoomId") REFERENCES "VacancyChatRoom"("id") ON DELETE RESTRICT ON UPDATE CASCADE;
