/*
  Warnings:

  - You are about to drop the column `registered` on the `CompanyUser` table. All the data in the column will be lost.
  - A unique constraint covering the columns `[email]` on the table `CompanyInvitation` will be added. If there are existing duplicate values, this will fail.
  - Made the column `birthdate` on table `CompanyUser` required. This step will fail if there are existing NULL values in that column.
  - Made the column `password` on table `CompanyUser` required. This step will fail if there are existing NULL values in that column.

*/
-- AlterTable
ALTER TABLE "CompanyUser" DROP COLUMN "registered",
ALTER COLUMN "birthdate" SET NOT NULL,
ALTER COLUMN "password" SET NOT NULL;

-- CreateIndex
CREATE UNIQUE INDEX "CompanyInvitation_email_key" ON "CompanyInvitation"("email");
