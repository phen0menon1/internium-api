-- DropForeignKey
ALTER TABLE "VacancyChatMessage" DROP CONSTRAINT "VacancyChatMessage_companyUserId_fkey";

-- AlterTable
ALTER TABLE "VacancyChatMessage" ALTER COLUMN "companyUserId" DROP NOT NULL;

-- AddForeignKey
ALTER TABLE "VacancyChatMessage" ADD CONSTRAINT "VacancyChatMessage_companyUserId_fkey" FOREIGN KEY ("companyUserId") REFERENCES "CompanyUser"("id") ON DELETE SET NULL ON UPDATE CASCADE;
