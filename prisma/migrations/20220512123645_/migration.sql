/*
  Warnings:

  - You are about to drop the column `companyUserId` on the `VacancyChatMessage` table. All the data in the column will be lost.
  - You are about to drop the column `internId` on the `VacancyChatMessage` table. All the data in the column will be lost.
  - You are about to drop the column `vacancyId` on the `VacancyChatMessage` table. All the data in the column will be lost.

*/
-- DropForeignKey
ALTER TABLE "VacancyChatMessage" DROP CONSTRAINT "VacancyChatMessage_companyUserId_fkey";

-- DropForeignKey
ALTER TABLE "VacancyChatMessage" DROP CONSTRAINT "VacancyChatMessage_internId_fkey";

-- DropForeignKey
ALTER TABLE "VacancyChatMessage" DROP CONSTRAINT "VacancyChatMessage_vacancyId_fkey";

-- AlterTable
ALTER TABLE "VacancyChatMessage" DROP COLUMN "companyUserId",
DROP COLUMN "internId",
DROP COLUMN "vacancyId";
