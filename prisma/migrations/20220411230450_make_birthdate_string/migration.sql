/*
  Warnings:

  - The `birthdate` column on the `CompanyUserInfo` table would be dropped and recreated. This will lead to data loss if there is data in the column.

*/
-- AlterTable
ALTER TABLE "CompanyUserInfo" DROP COLUMN "birthdate",
ADD COLUMN     "birthdate" VARCHAR(10);
