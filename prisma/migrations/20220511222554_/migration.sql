/*
  Warnings:

  - You are about to drop the column `avatarId` on the `Intern` table. All the data in the column will be lost.
  - You are about to drop the `Avatar` table. If the table is not empty, all the data it contains will be lost.

*/
-- DropForeignKey
ALTER TABLE "Intern" DROP CONSTRAINT "Intern_avatarId_fkey";

-- AlterTable
ALTER TABLE "Intern" DROP COLUMN "avatarId",
ADD COLUMN     "avatar" VARCHAR(255);

-- DropTable
DROP TABLE "Avatar";
