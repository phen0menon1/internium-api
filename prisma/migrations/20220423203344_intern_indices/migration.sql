/*
  Warnings:

  - A unique constraint covering the columns `[internId,vacancyId]` on the table `InternReactions` will be added. If there are existing duplicate values, this will fail.

*/
-- DropIndex
DROP INDEX "InternReactions_internId_vacancyId_idx";

-- CreateIndex
CREATE UNIQUE INDEX "InternReactions_internId_vacancyId_key" ON "InternReactions"("internId", "vacancyId");
