/*
  Warnings:

  - You are about to drop the column `email` on the `Company` table. All the data in the column will be lost.
  - You are about to drop the column `password` on the `Company` table. All the data in the column will be lost.
  - You are about to drop the column `phone` on the `Company` table. All the data in the column will be lost.
  - You are about to alter the column `city` on the `Company` table. The data in that column could be lost. The data in that column will be cast from `VarChar(128)` to `VarChar(48)`.
  - You are about to drop the column `birthdate` on the `CompanyUser` table. All the data in the column will be lost.
  - You are about to drop the column `firstName` on the `CompanyUser` table. All the data in the column will be lost.
  - You are about to drop the column `lastName` on the `CompanyUser` table. All the data in the column will be lost.
  - You are about to drop the column `middleName` on the `CompanyUser` table. All the data in the column will be lost.
  - You are about to drop the column `position` on the `CompanyUser` table. All the data in the column will be lost.
  - You are about to alter the column `email` on the `CompanyUser` table. The data in that column could be lost. The data in that column will be cast from `VarChar(255)` to `VarChar(60)`.
  - You are about to alter the column `password` on the `CompanyUser` table. The data in that column could be lost. The data in that column will be cast from `VarChar(128)` to `VarChar(48)`.
  - A unique constraint covering the columns `[companyUserInfoId]` on the table `CompanyUser` will be added. If there are existing duplicate values, this will fail.

*/
-- CreateEnum
CREATE TYPE "CompanyUserRole" AS ENUM ('OWNER', 'USER');

-- DropIndex
DROP INDEX "Company_email_key";

-- DropIndex
DROP INDEX "Company_phone_key";

-- AlterTable
ALTER TABLE "Company" DROP COLUMN "email",
DROP COLUMN "password",
DROP COLUMN "phone",
ALTER COLUMN "city" SET DATA TYPE VARCHAR(48);

-- AlterTable
ALTER TABLE "CompanyUser" DROP COLUMN "birthdate",
DROP COLUMN "firstName",
DROP COLUMN "lastName",
DROP COLUMN "middleName",
DROP COLUMN "position",
ADD COLUMN     "companyUserInfoId" INTEGER,
ADD COLUMN     "role" "CompanyUserRole" NOT NULL DEFAULT E'USER',
ALTER COLUMN "email" SET DATA TYPE VARCHAR(60),
ALTER COLUMN "password" SET DATA TYPE VARCHAR(48);

-- CreateTable
CREATE TABLE "CompanyUserInfo" (
    "id" SERIAL NOT NULL,
    "firstName" VARCHAR(48) NOT NULL,
    "lastName" VARCHAR(48) NOT NULL,
    "middleName" VARCHAR(48),
    "position" VARCHAR(100) NOT NULL,
    "phone" VARCHAR(20),
    "birthdate" TIMESTAMP(3),

    CONSTRAINT "CompanyUserInfo_pkey" PRIMARY KEY ("id")
);

-- CreateIndex
CREATE UNIQUE INDEX "CompanyUserInfo_phone_key" ON "CompanyUserInfo"("phone");

-- CreateIndex
CREATE UNIQUE INDEX "CompanyUser_companyUserInfoId_key" ON "CompanyUser"("companyUserInfoId");

-- AddForeignKey
ALTER TABLE "CompanyUser" ADD CONSTRAINT "CompanyUser_companyUserInfoId_fkey" FOREIGN KEY ("companyUserInfoId") REFERENCES "CompanyUserInfo"("id") ON DELETE SET NULL ON UPDATE CASCADE;
