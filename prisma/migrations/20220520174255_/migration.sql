/*
  Warnings:

  - Added the required column `senderId` to the `VacancyInvitation` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE "InternReactions" ADD COLUMN     "accepted" BOOLEAN NOT NULL DEFAULT false,
ADD COLUMN     "viewed" BOOLEAN NOT NULL DEFAULT false;

-- AlterTable
ALTER TABLE "VacancyInvitation" ADD COLUMN     "accepted" BOOLEAN NOT NULL DEFAULT false,
ADD COLUMN     "senderId" INTEGER NOT NULL,
ADD COLUMN     "viewed" BOOLEAN NOT NULL DEFAULT false;

-- AddForeignKey
ALTER TABLE "VacancyInvitation" ADD CONSTRAINT "VacancyInvitation_senderId_fkey" FOREIGN KEY ("senderId") REFERENCES "CompanyUser"("id") ON DELETE RESTRICT ON UPDATE CASCADE;
