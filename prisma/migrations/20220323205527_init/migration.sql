-- CreateEnum
CREATE TYPE "InternStatus" AS ENUM ('ACTIVE', 'INACTIVE');

-- CreateTable
CREATE TABLE "Intern" (
    "id" SERIAL NOT NULL,
    "email" VARCHAR(255) NOT NULL,
    "password" VARCHAR(255) NOT NULL,
    "firstName" VARCHAR(100) NOT NULL,
    "middleName" VARCHAR(100) NOT NULL,
    "lastName" VARCHAR(100) NOT NULL,
    "birthdate" TIMESTAMP(3) NOT NULL,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "gender" BOOLEAN NOT NULL,
    "location" VARCHAR(100),
    "description" VARCHAR(8192),
    "avatar" VARCHAR(255),
    "phone" VARCHAR(255) NOT NULL,
    "status" "InternStatus" NOT NULL DEFAULT E'INACTIVE',

    CONSTRAINT "Intern_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "InternExperience" (
    "id" SERIAL NOT NULL,
    "internId" INTEGER NOT NULL,
    "companyName" VARCHAR(128) NOT NULL,
    "position" VARCHAR(128) NOT NULL,
    "dateStart" TIMESTAMP(3) NOT NULL,
    "dateEnd" TIMESTAMP(3) NOT NULL,
    "description" VARCHAR(3000) NOT NULL,

    CONSTRAINT "InternExperience_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "InternEducation" (
    "id" SERIAL NOT NULL,
    "universityId" INTEGER NOT NULL,
    "dateStart" TIMESTAMP(3) NOT NULL,
    "dateEnd" TIMESTAMP(3) NOT NULL,
    "activity" VARCHAR(1000) NOT NULL,
    "internId" INTEGER NOT NULL,

    CONSTRAINT "InternEducation_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "Company" (
    "id" SERIAL NOT NULL,
    "name" VARCHAR(128) NOT NULL,
    "description" VARCHAR(3000) NOT NULL,

    CONSTRAINT "Company_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "CompanyUser" (
    "id" SERIAL NOT NULL,
    "companyId" INTEGER NOT NULL,
    "firstName" VARCHAR(100) NOT NULL,
    "lastName" VARCHAR(100) NOT NULL,
    "middleName" VARCHAR(100) NOT NULL,
    "birthdate" TIMESTAMP(3) NOT NULL,
    "position" VARCHAR(100) NOT NULL,

    CONSTRAINT "CompanyUser_pkey" PRIMARY KEY ("id")
);

-- CreateIndex
CREATE UNIQUE INDEX "Intern_email_key" ON "Intern"("email");

-- AddForeignKey
ALTER TABLE "InternExperience" ADD CONSTRAINT "InternExperience_internId_fkey" FOREIGN KEY ("internId") REFERENCES "Intern"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "InternEducation" ADD CONSTRAINT "InternEducation_internId_fkey" FOREIGN KEY ("internId") REFERENCES "Intern"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "CompanyUser" ADD CONSTRAINT "CompanyUser_companyId_fkey" FOREIGN KEY ("companyId") REFERENCES "Company"("id") ON DELETE RESTRICT ON UPDATE CASCADE;
