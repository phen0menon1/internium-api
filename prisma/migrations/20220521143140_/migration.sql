/*
  Warnings:

  - Made the column `companyUserId` on table `VacancyAnnouncement` required. This step will fail if there are existing NULL values in that column.

*/
-- DropForeignKey
ALTER TABLE "VacancyAnnouncement" DROP CONSTRAINT "VacancyAnnouncement_companyUserId_fkey";

-- AlterTable
ALTER TABLE "VacancyAnnouncement" ALTER COLUMN "companyUserId" SET NOT NULL;

-- AddForeignKey
ALTER TABLE "VacancyAnnouncement" ADD CONSTRAINT "VacancyAnnouncement_companyUserId_fkey" FOREIGN KEY ("companyUserId") REFERENCES "CompanyUser"("id") ON DELETE RESTRICT ON UPDATE CASCADE;
