-- DropForeignKey
ALTER TABLE "VacancyAnnouncement" DROP CONSTRAINT "VacancyAnnouncement_companyUserId_fkey";

-- AlterTable
ALTER TABLE "VacancyAnnouncement" ALTER COLUMN "companyUserId" DROP NOT NULL;

-- AddForeignKey
ALTER TABLE "VacancyAnnouncement" ADD CONSTRAINT "VacancyAnnouncement_companyUserId_fkey" FOREIGN KEY ("companyUserId") REFERENCES "CompanyUser"("id") ON DELETE SET NULL ON UPDATE CASCADE;
