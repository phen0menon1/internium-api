FROM node:14.18

RUN apt update && apt install --no-install-recommends -y curl ca-certificates

WORKDIR /app/internium

ADD .env nest-cli.json tsconfig.json tsconfig.build.json ./
COPY package.json yarn.lock ./
COPY prisma ./prisma/
COPY ./src ./src

RUN yarn cache clean --all
RUN yarn
RUN npx prisma generate 

EXPOSE 8100

CMD yarn start:dev