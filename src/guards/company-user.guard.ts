import { ForbiddenException, Injectable } from '@nestjs/common';
import { CompanyUserRole } from '@prisma/client';
import { CompanyUserFull, JwtValidEntity } from '../types';
import { CompanyGuard } from './company.guard';

@Injectable()
export class CompanyUserGuard extends CompanyGuard {
  handleRequest<T = CompanyUserFull>(_: unknown, user: JwtValidEntity): T {
    const companyUser = super.handleRequest(_, user);
    if (companyUser.role !== CompanyUserRole.USER) {
      throw new ForbiddenException('You have no permission for this resource');
    }
    return user.user as unknown as T;
  }
}
