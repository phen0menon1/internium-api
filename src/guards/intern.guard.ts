import { ForbiddenException, Injectable, UnauthorizedException } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { JwtAuthorizeEntity, JwtValidEntity } from '../types';

@Injectable()
export class InternGuard extends AuthGuard('jwt') {
  handleRequest(_: unknown, user: JwtValidEntity, info: Error): any {
    if (!user.model) throw new UnauthorizedException();
    if (user.model !== JwtAuthorizeEntity.INTERN) {
      throw new ForbiddenException('You have no permission to this resource');
    }
    return user.user;
  }
}
