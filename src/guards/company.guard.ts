import { ForbiddenException, Injectable, UnauthorizedException } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { CompanyUserFull, JwtAuthorizeEntity, JwtValidEntity } from '../types';

@Injectable()
export class CompanyGuard extends AuthGuard('jwt') {
  handleRequest<T = CompanyUserFull>(_: unknown, user: JwtValidEntity): T {
    if (!user.model) throw new UnauthorizedException();

    if (user.model !== JwtAuthorizeEntity.COMPANY_USER) {
      throw new ForbiddenException('You have no permission to this resource');
    }
    return user.user as unknown as T;
  }
}
