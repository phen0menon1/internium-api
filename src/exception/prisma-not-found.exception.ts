import { BadRequestException, NotFoundException } from '@nestjs/common';

export class PrismaNotFoundException extends NotFoundException {
  constructor(message: string = '') {
    super(message);
  }

  getErrorMessage() {
    return this.message;
  }
}
