import { BadRequestException } from '@nestjs/common';

export class InvalidFormException extends BadRequestException {
  constructor(private errors: { [key: string]: string }, message: string = '') {
    super(message);
  }

  getErrorMessage() {
    return this.message;
  }

  getFieldErrors() {
    return this.errors;
  }
}
