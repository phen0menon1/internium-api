import { Strategy, ExtractJwt, VerifiedCallback } from 'passport-jwt';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable, UnauthorizedException } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { JwtService } from './modules/jwt/jwt.service';
import { JwtData } from './modules/jwt/jwt.models';
import { CompanyUserService } from './modules/company-user/company-user.service';
import { InternService } from './modules/intern/intern.service';
import { JwtAuthorizeEntity, JwtValidEntity } from './types';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(
    readonly jwtService: JwtService,
    readonly configService: ConfigService,
    readonly internService: InternService,
    readonly companyUserService: CompanyUserService,
  ) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      secretOrKey: configService.get('JWT_ACCESS_SECRET'),
      ignoreExpiration: false,
    });
  }

  async getUserObjectByJwtEntity(data: JwtData): Promise<JwtValidEntity> {
    const { entity, id } = data;

    switch (entity) {
      case 'companyUser': {
        const user = await this.companyUserService.getFull(id, { shouldInclude: true });
        return { model: JwtAuthorizeEntity.COMPANY_USER, user };
      }

      case 'intern': {
        const user = await this.internService.getWithRelations(id);
        return { model: JwtAuthorizeEntity.INTERN, user };
      }

      default: {
        throw new UnauthorizedException('Invalid JWT data');
      }
    }
  }

  async validate(payload: any, done: VerifiedCallback): Promise<any> {
    const userObject = await this.getUserObjectByJwtEntity(payload as unknown as JwtData);
    done(null, userObject);
    return true;
  }
}
