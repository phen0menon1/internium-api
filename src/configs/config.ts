import type { Config } from './config.interface';

const config: Config = {
  cors: {
    enabled: true,
  },
  swagger: {
    enabled: true,
    title: 'Internium API',
    description: 'The nestjs API description',
    version: '1.5',
    path: 'api',
  },
  security: {
    expiresIn: '30m',
    refreshIn: '7d',
    bcryptSaltOrRound: 10,
  },
};

export default (): Config => config;
