import { CallHandler, ExecutionContext, Injectable, NestInterceptor } from '@nestjs/common';
import { PrismaClientKnownRequestError } from '@prisma/client/runtime';
import { catchError, Observable } from 'rxjs';
import { PRISMA_ERRORS } from '../constants/prisma';
import { InvalidFormException } from '../exception/invalid-form.exception';

@Injectable()
export class PrismaInterceptor implements NestInterceptor {
  intercept(_: ExecutionContext, next: CallHandler): Observable<any> {
    return next.handle().pipe(
      catchError((error) => {
        if (error instanceof PrismaClientKnownRequestError) {
          const { code, meta } = error;
          const fields = meta['target'];
          if (!fields) throw error;
          const customError = fields.reduce((messages: Record<string, string>, field: string) => {
            messages[field] = PRISMA_ERRORS[code].replace('{field}', field);
            return messages;
          }, {});
          throw new InvalidFormException(customError);
        } else {
          throw error;
        }
      }),
    );
  }
}
