import { Request } from 'express';
import { extname } from 'path';
import { createHash } from 'crypto';
import { BadRequestException } from '@nestjs/common';

export const imageFileEditName = (
  req: Request,
  file: Express.Multer.File,
  callback: (error: Error | null, filename: string) => void,
) => {
  const name = file.originalname.split('.')[0];
  const fileExtName = extname(file.originalname);
  const uid = createHash('md5').update(name, 'utf8').digest('hex').toString();
  const randomName = [...new Array(8).keys()].map(() => Math.round(Math.random() * 16).toString(16)).join('');
  callback(null, `${uid}${randomName}${fileExtName}`);
};

export const imageFileFilter = (
  req: any,
  file: Express.Multer.File,
  callback: (error: Error | null, acceptFile: boolean) => void,
) => {
  if (file.originalname.length > 100) {
    return callback(new BadRequestException('Invalid file length: length cannot exceed 100 characters'), false);
  }

  if (!file.originalname.match(/\.(jpg|jpeg|png|gif)$/)) {
    return callback(new BadRequestException('Invalid file extension: only jpg/jpeg/png/gif allowed'), false);
  }

  callback(null, true);
};
