export const PRISMA_ERRORS = {
  P2002: 'Value of the unique field {field} is already used',
};
