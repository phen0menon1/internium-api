import {
  Ability,
  Avatar,
  Company,
  CompanyUser,
  CompanyUserInfo,
  CompanyUserRole,
  Intern,
  InternReactions,
  InternToVacancy,
  Prisma,
  Vacancy,
  VacancyAnnouncement,
  VacancyChatMessage,
  VacancyChatRoom,
  VacancyInvitation,
} from '@prisma/client';
import { omit } from 'lodash';
import { SerializedChatMessageType } from './modules/chat/chat.dto';

export interface SerializedCompanyUser {
  id?: number;
  email?: string;
  password?: string;
  companyId?: number;
  role?: CompanyUserRole;
  userInfoId?: number;
  userInfo?: CompanyUserInfo & {
    avatar?: string;
  };
  company?: SerializedCompany;
}

export type SerializableAbilities = {
  ability: Ability;
}[];

export type SerializableVacancy = Vacancy & {
  abilities: SerializableAbilities;
  company?: SerializableCompany;
};

export type SerializableIntern = Partial<Intern> & { abilities?: SerializableAbilities; avatar?: Avatar };

export type SerializableVacancyWithChats = Vacancy & {
  internToVacancies: (InternToVacancy & {
    intern: SerializableIntern;
    VacancyChatRoom: (VacancyChatRoom & {
      messages: VacancyChatMessage[];
    })[];
  })[];
};

export type SerializableInternChat = InternToVacancy & {
  vacancy: SerializableVacancy;
  VacancyChatRoom: (VacancyChatRoom & {
    messages: VacancyChatMessage[];
  })[];
};

export type SerializableCompanyUser = Partial<CompanyUser> &
  Partial<{
    userInfo: CompanyUserInfo & { avatar?: Avatar };
    company: Partial<Company> & { logo?: Avatar };
  }>;

export type SerializableInvitation = VacancyInvitation & {
  internToVacancy: InternToVacancy & {
    intern: Intern & any;
    vacancy: Vacancy & any;
  };
};

export type SerializableAnnouncement = VacancyAnnouncement & {
  companyUser: SerializableCompanyUser;
};

export interface SerializableCompany extends Partial<Omit<Company, 'tin' | 'email' | 'logoId' | 'verified'>> {
  logo?: Avatar;
}

export type SerializableChatMessage = VacancyChatMessage & {
  room: VacancyChatRoom;
  companyUser: SerializableCompanyUser;
};

export interface SerializedIntern extends Intern {
  abilities: Ability[];
  avatar: string;
}

export interface SerializedCompany extends Omit<Company, 'tin' | 'email' | 'logoId'> {
  logo?: string;
}

export interface SerializedVacancy extends Omit<Vacancy, 'abilities'> {
  abilities: Ability[];
  company: any;
}

export interface SerializedChatMessage extends Omit<VacancyChatMessage, 'roomId'> {
  isMine?: boolean;
  type: SerializedChatMessageType;
  companyUser: SerializedCompanyUser;
}

export interface SerializedInternReaction extends Omit<InternReactions, 'internToVacancy' | 'internToVacancyId'> {
  intern: SerializedIntern;
  vacancy: SerializedVacancy;
}

export interface SerializedInvitation extends Omit<VacancyInvitation, 'internToVacancy' | 'internToVacancyId'> {
  intern: SerializedIntern;
  vacancy: SerializedVacancy;
}

export class EntityManager {
  readonly selectAbilityEntity = { select: { ability: true } };

  readonly companyEntity = {
    id: true,
    name: true,
    city: true,
    logo: true,
    logoId: true,
    hidden: true,
    website: true,
    description: true,
    shortDescription: true,
  };

  readonly selectChatroomEntity = {
    include: {
      internToVacancy: true,
    },
  };

  readonly selectCompanyEntity = {
    select: this.companyEntity,
  };

  readonly includeUserInfoEntity = {
    include: { avatar: true },
  };

  readonly companyUserEntity = {
    id: true,
    role: true,
    email: true,
    companyId: true,
    userInfoId: true,
    company: this.selectCompanyEntity,
    userInfo: this.includeUserInfoEntity,
  };

  readonly selectCompanyUserEntity = {
    select: this.companyUserEntity,
  };

  readonly companyUserEntityWithoutCompany = omit(this.companyUserEntity, 'company');

  readonly selectCompanyUserEntityWithoutCompany = {
    select: this.companyUserEntityWithoutCompany,
  };

  readonly selectLastChatroomMessage = {
    include: {
      messages: {
        distinct: ['roomId'] as Prisma.VacancyChatMessageScalarFieldEnum[],
        orderBy: {
          id: 'desc' as Prisma.SortOrder,
        },
        include: {
          companyUser: this.selectCompanyUserEntityWithoutCompany,
        },
      },
    },
  };

  readonly internEntity = {
    abilities: { include: { ability: true } },
    avatar: true,
  };

  readonly selectInternEntity = {
    include: this.internEntity,
  };

  readonly selectVacancyEntity = {
    include: {
      abilities: this.selectAbilityEntity,
      company: this.selectCompanyEntity,
    },
  };

  readonly internToVacancyEntity = {
    intern: this.selectInternEntity,
    vacancy: this.selectVacancyEntity,
  };

  readonly selectInternToVacancyEntity = {
    include: this.internToVacancyEntity,
  };

  readonly vacancyFullEntity = {
    abilities: this.selectAbilityEntity,
    company: this.selectCompanyEntity,
  };

  serializeInvitation<T extends SerializableInvitation>(invitation: T) {
    const serialized = omit(invitation, ['internToVacancy', 'internToVacancyId']) as unknown as SerializedInvitation;
    serialized.intern = this.serializeIntern(invitation.internToVacancy.intern);
    serialized.vacancy = this.serializeVacancy(invitation.internToVacancy.vacancy);
    return serialized;
  }

  serializeCompanyUserInfo<T extends CompanyUserInfo & { avatar?: Avatar }>(userInfo: T) {
    const serialized = userInfo as unknown as Omit<T, 'avatar'> & { avatar: string };
    serialized.avatar = userInfo.avatar?.filepath ?? null;
    return serialized;
  }

  serializeCompany<T extends SerializableCompany>(company: T) {
    const serialized = company as unknown as SerializedCompany;
    serialized.logo = company.logo?.filepath ?? null;
    return serialized;
  }

  serializeCompanyUser<T extends SerializableCompanyUser>(companyUser: T) {
    if (!companyUser) return null;

    const serialized = omit(companyUser, ['userInfoId']) as unknown as SerializedCompanyUser;

    if (Object.getOwnPropertyDescriptor(companyUser, 'userInfo')) {
      serialized.userInfo = companyUser.userInfo ? this.serializeCompanyUserInfo(companyUser.userInfo) : null;
    }

    if (Object.getOwnPropertyDescriptor(companyUser, 'company')) {
      serialized.company = companyUser.company ? this.serializeCompany(companyUser.company) : null;
    }

    return serialized;
  }

  serializeIntern<T extends SerializableIntern>(intern: T) {
    const serialized = intern as unknown as SerializedIntern;
    serialized['abilities'] = intern.abilities?.map(({ ability }) => ability);
    serialized['avatar'] = intern.avatar?.filepath ?? null;
    return serialized;
  }

  serializeReaction(
    reaction: InternReactions & {
      internToVacancy: InternToVacancy & {
        intern: Intern & { avatar: Avatar };
        vacancy: Vacancy & { abilities: { ability: Ability }[]; company?: SerializableCompany };
      };
    },
  ) {
    const serialized = omit(reaction, ['internToVacancy', 'internToVacancyId']) as unknown as SerializedInternReaction;
    if (reaction.internToVacancy) {
      serialized.intern = this.serializeIntern(reaction.internToVacancy.intern);
      serialized.vacancy = this.serializeVacancy(reaction.internToVacancy.vacancy);
    }
    return serialized;
  }

  serializeVacancy<T extends SerializableVacancy>(vacancy: T) {
    const serialized = omit(vacancy, 'abilities') as unknown as SerializedVacancy;

    if (vacancy.abilities) {
      serialized['abilities'] = vacancy.abilities.map(({ ability }) => ability);
    }

    if (vacancy.company) {
      serialized['company'] = this.serializeCompany(vacancy.company);
    }

    return serialized;
  }

  serializeVacancyWithChats<T extends SerializableVacancyWithChats>(vacancyWithChat: T) {
    return {
      ...omit(vacancyWithChat, 'internToVacancies'),
      chats: vacancyWithChat.internToVacancies.map((chat) => {
        const room = chat.VacancyChatRoom[0];
        return {
          ...omit(chat, ['VacancyChatRoom', 'vacancyId', 'internId']),
          room: {
            ...omit(room, ['internToVacancyId', 'messages']),
            lastMessage: room?.messages.pop() ?? null,
          },
          intern: this.serializeIntern(chat.intern),
        };
      }),
    };
  }

  serializeInternChats<T extends SerializableInternChat>(internChat: T) {
    const room = omit(internChat.VacancyChatRoom[0], ['internToVacancyId']);
    const hasRoom = Object.keys(room).length > 0;

    return {
      ...omit(internChat, 'VacancyChatRoom'),
      vacancy: this.serializeVacancy(internChat.vacancy),
      room: hasRoom ? { ...omit(room, 'messages'), lastMessage: room?.messages.pop() ?? null } : null,
    };
  }

  serializeAnnouncement<T extends SerializableAnnouncement>(
    announcement: T,
    isCompanyUser: boolean = false,
  ): SerializedChatMessage {
    return {
      id: announcement.id,
      isRead: false,
      isMine: isCompanyUser,
      type: SerializedChatMessageType.SYSTEM,
      content: announcement.message,
      createdAt: announcement.createdAt,
      companyUser: this.serializeCompanyUser(announcement.companyUser),
      companyUserId: announcement.companyUserId,
    };
  }

  serializeChatMessage<T extends SerializableChatMessage>(message: T, isMine: boolean = false): SerializedChatMessage {
    const serialized = omit(message, ['room', 'roomId', 'avatar']) as unknown as SerializedChatMessage;
    serialized.companyUser = this.serializeCompanyUser(message.companyUser);
    serialized.type = SerializedChatMessageType.REGULAR;
    serialized.isMine = isMine;
    return serialized;
  }
}
