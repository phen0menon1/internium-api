import { ArgumentsHost, Catch, ExceptionFilter } from '@nestjs/common';
import { InvalidFormException } from '../exception/invalid-form.exception';

@Catch(InvalidFormException)
export class InvalidFormExceptionFilter implements ExceptionFilter {
  catch(exception: InvalidFormException, host: ArgumentsHost) {
    const context = host.switchToHttp();
    const response = context.getResponse();
    const statusCode = exception.getStatus();

    response.status(statusCode).json({
      statusCode,
      errors: exception.getFieldErrors(),
    });
  }
}
