import { ArgumentsHost, Catch, HttpStatus } from '@nestjs/common';
import { BaseExceptionFilter } from '@nestjs/core';
import { Prisma } from '@prisma/client';
import { Response } from 'express';

/**
 * Error codes definition for Prisma Client (Query Engine)
 * https://www.prisma.io/docs/reference/api-reference/error-reference#prisma-client-query-engine
 */
@Catch(Prisma?.PrismaClientKnownRequestError)
export class PrismaClientExceptionFilter extends BaseExceptionFilter {
  catch(exception: Prisma.PrismaClientKnownRequestError, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse<Response>();

    switch (exception.code) {
      case 'P2000':
        this.catchValueTooLong(exception, response);
        break;
      case 'P2002':
        this.catchUniqueConstraint(exception, response);
        break;
      case 'P2003':
        this.catchForeignKeyConstraint(exception, response);
        break;
      case 'P2025':
        this.catchNotFound(exception, response);
        break;
      default:
        console.error(`UNHANDLED PRISMA EXCEPTION WITH ID ${exception.code}`);
        this.unhandledException(exception, host);
        break;
    }
  }

  /**
   * @param exception P2000
   * @param response 400 Bad Request
   */
  catchValueTooLong(exception: Prisma.PrismaClientKnownRequestError, response: Response) {
    const status = HttpStatus.BAD_REQUEST;
    response.status(status).json({
      statusCode: status,
      message: this.cleanUpException(exception),
    });
  }

  /**
   * @param exception P2002
   * @param response 409 Conflict
   */
  catchUniqueConstraint(exception: Prisma.PrismaClientKnownRequestError, response: Response) {
    const status = HttpStatus.CONFLICT;
    response.status(status).json({
      statusCode: status,
      message: this.cleanUpException(exception),
    });
  }

  /**
   * @param exception P2003
   * @param response 404
   */
  catchForeignKeyConstraint(exception: Prisma.PrismaClientKnownRequestError, response: Response) {
    const statusCode = HttpStatus.NOT_FOUND;
    const constraintError = exception.meta?.['field_name'] as string;
    const entityModel = constraintError.split('_')?.[0];
    const message = entityModel
      ? `Not found relation ${entityModel} with specified data`
      : `Invalid constraint field: ${constraintError}`;
    response.status(statusCode).json({
      statusCode,
      message,
    });
  }

  /**
   * @param exception P2025
   * @param response 404 Not Found
   */
  catchNotFound(_: Prisma.PrismaClientKnownRequestError, response: Response) {
    const status = HttpStatus.NOT_FOUND;
    response.status(status).json({
      statusCode: status,
      message: 'An operation failed because it depends on one or more records that were required but not found.',
    });
  }

  unhandledException(exception: Prisma.PrismaClientKnownRequestError, host: ArgumentsHost) {
    // default 500 error code
    super.catch(exception, host);
  }

  /**
   * @param exception
   * @returns replace line breaks with empty string
   */
  cleanUpException(exception: Prisma.PrismaClientKnownRequestError): string {
    return exception.message.replace(/\n/g, '');
  }
}
