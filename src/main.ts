import { ClassSerializerInterceptor, INestApplication, ValidationPipe } from '@nestjs/common';
import { HttpAdapterHost, NestFactory, Reflector } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { AppModule } from './app.module';
import { InvalidFormExceptionFilter } from './filters/invalid-form.filter';
import { PrismaInterceptor } from './interceptors/prisma.interceptor';
import * as basicAuth from 'express-basic-auth';
import { PrismaClientExceptionFilter } from './filters/prisma-client-exception.filter';
import { SecuritySchemeObject } from '@nestjs/swagger/dist/interfaces/open-api-spec.interface';
import { NestExpressApplication } from '@nestjs/platform-express';
import { join, resolve } from 'path';
import { AdminModule } from './modules/admin/admin.module';

const API_PREFIX = 'api';
const ADMIN_PREFIX = 'admin';

function initEnvironment() {
  const requiredEnvironmentVariables = [
    'SWAGGER_USERNAME',
    'SWAGGER_PASSWORD',
    'DB_DATABASE',
    'DB_USERNAME',
    'DB_PASSWORD',
    'DB_HOST',
    'DB_URL',
    'DB_PORT',
    'POSTGRES_DB',
    'POSTGRES_USER',
    'POSTGRES_PASSWORD',
    'POSTGRES_ENCRYPTION_KEY_NAME',
    'POSTGRES_ENCRYPTION_KEY_VALUE',
    'POSTGRES_ENCRYPTION_OPTIONS',
    'JWT_ACCESS_SECRET',
    'JWT_REFRESH_SECRET',
    'NODE_ENV',
    'UPLOAD_DEST',
    'ADMIN_USERNAME',
    'ADMIN_PASSWORD',
    'ADMIN_SECRET',
  ];

  const missingEnvironmentVariables = requiredEnvironmentVariables.filter(
    (variable) => !Boolean(process.env[variable]),
  );

  if (missingEnvironmentVariables.length) {
    throw new Error(`Missing env ${missingEnvironmentVariables.join(', ')} variables`);
  }
}

async function initSwagger(app: INestApplication) {
  const options = new DocumentBuilder()
    .setTitle('Internium API')
    .setDescription(
      `<h5>Glossary:</h5>
      <ul>
      <li><pre>[C] - Company guarded endpoint</pre></li>
      <li><pre>[CO] - Company Owner guarded endpoint</pre></li>
      <li><pre>[I] - Intern guarded endpoint</pre></li>
      </ul>`,
    )
    .setVersion('1.2.5')
    .addBearerAuth({ type: 'http', schema: 'Bearer', bearerFormat: 'Token' } as SecuritySchemeObject, 'Bearer')
    .build();
  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup('/api/swagger', app, document, {
    customSiteTitle: 'Internium API docs',
    customCssUrl: '/public/css/swagger.theme.css',
  });

  app.use(
    ['/api/swagger'],
    basicAuth({
      challenge: true,
      users: {
        [process.env.SWAGGER_USERNAME]: process.env.SWAGGER_PASSWORD,
      },
    }),
  );
}

async function initApi() {
  const api = await NestFactory.create<NestExpressApplication>(AppModule, {
    // logger: ['error', 'warn'],
  });
  const { httpAdapter } = api.get(HttpAdapterHost);
  api.enableCors({ origin: '*' });
  api.useStaticAssets(join(process.cwd(), 'files'), {
    index: false,
    prefix: '/files',
    dotfiles: 'deny',
    extensions: ['jpg', 'jpeg', 'png', 'gif'],
    lastModified: false,
  });
  api.useStaticAssets(join(process.cwd(), 'src', 'public'), {
    index: false,
    prefix: '/public',
  });
  api.setGlobalPrefix(API_PREFIX);
  api.useGlobalPipes(new ValidationPipe({ transform: true, whitelist: true, forbidNonWhitelisted: true }));
  api.useGlobalFilters(new InvalidFormExceptionFilter(), new PrismaClientExceptionFilter(httpAdapter));
  api.useGlobalInterceptors(new PrismaInterceptor(), new ClassSerializerInterceptor(api.get(Reflector)));
  if (process.env.NODE_ENV === 'development') {
    initSwagger(api);
  }
  await api.listen(8100);
}

async function initAdmin() {
  const admin = await NestFactory.create<NestExpressApplication>(AdminModule);
  await admin.listen(8101);
}

async function bootstrap() {
  initEnvironment();

  await initApi();
  await initAdmin();
}

bootstrap();
