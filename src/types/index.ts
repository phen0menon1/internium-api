import { SerializedCompanyUser } from '@/entity-manager';
import { Ability, Avatar, Company, CompanyUser, CompanyUserInfo, Intern } from '@prisma/client';

export enum JwtAuthorizeEntity {
  INTERN = 'intern',
  COMPANY_USER = 'companyUser',
}

export type CompanyUserFull = Required<SerializedCompanyUser>;

export interface JwtCompanyUserEntity {
  model: JwtAuthorizeEntity.COMPANY_USER;
  user: SerializedCompanyUser;
}

export type FullInternUser = Intern & {
  abilities: Ability[];
  avatar: string;
};

export interface JwtInternUserEntity {
  model: JwtAuthorizeEntity.INTERN;
  user: FullInternUser;
}

export type JwtValidEntity = JwtCompanyUserEntity | JwtInternUserEntity;
