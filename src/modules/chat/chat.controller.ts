import { CurrentUser } from '@/decorators/current-user.decorator';
import { AuthorizedGuard } from '@/guards/authorized.guard';
import { JwtValidEntity } from '@/types';
import { Body, Controller, ForbiddenException, Get, HttpCode, Post, Query, UseGuards } from '@nestjs/common';
import { ApiForbiddenResponse, ApiOkResponse, ApiOperation, ApiTags, ApiUnauthorizedResponse } from '@nestjs/swagger';
import { CreateMessageDto, GetChatMessagesParamsDto, GetChatroomResponse, MessageModel } from './chat.dto';
import { ChatService } from './chat.service';

@Controller('chats')
@ApiTags('Chats')
export class ChatController {
  constructor(private readonly chatService: ChatService) {}

  @Get('/')
  @UseGuards(AuthorizedGuard)
  @ApiOperation({ summary: 'Get chatroom messages', description: 'Get chatroom messages by vacancy and intern id.' })
  @ApiOkResponse({ description: 'List of messages', type: GetChatroomResponse })
  @ApiUnauthorizedResponse({ description: 'Unauthorized' })
  @ApiForbiddenResponse({ description: 'Only related to the chat users can access this resource' })
  async getChatroom(@CurrentUser() user: JwtValidEntity, @Query() getChatroomDto: GetChatMessagesParamsDto) {
    const chatroom = await this.chatService.getChatroom({
      internToVacancy: getChatroomDto,
    });

    const hasUserAccessToChat = this.chatService.hasUserAccessToChat(chatroom, user);

    if (!hasUserAccessToChat) {
      throw new ForbiddenException('You have no permission to this resource');
    }

    const { messages } = await this.chatService.getChatroomMessagesForCurrentUser(chatroom.id, user);

    return {
      messages,
      chatroom: {
        id: chatroom.id,
      },
    };
  }

  @Post('/')
  @HttpCode(200)
  @UseGuards(AuthorizedGuard)
  @ApiOperation({ summary: 'Create chatroom message', description: 'Get chatroom messages by vacancy and intern id.' })
  @ApiOkResponse({ description: 'Message created and returned', type: MessageModel })
  @ApiUnauthorizedResponse({ description: 'Unauthorized' })
  @ApiForbiddenResponse({ description: 'Only related to the chat users can access this resource' })
  async postMessage(@CurrentUser() user: JwtValidEntity, @Body() createMessageDto: CreateMessageDto) {
    const chatroom = await this.chatService.getChatroom({
      id: createMessageDto.roomId,
    });

    const hasUserAccessToChat = this.chatService.hasUserAccessToChat(chatroom, user);

    if (!hasUserAccessToChat) {
      throw new ForbiddenException('You have no permission to this resource');
    }

    return await this.chatService.createMessage(createMessageDto, user);
  }
}
