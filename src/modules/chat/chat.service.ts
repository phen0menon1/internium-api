import { Injectable } from '@nestjs/common';
import { Prisma } from '@prisma/client';
import { JwtAuthorizeEntity, JwtValidEntity } from '@/types';
import { PrismaService } from '../prisma/prisma.service';
import { CreateChatroomDto, CreateMessageDto } from './chat.dto';
import { EntityManager } from '@/entity-manager';

@Injectable()
export class ChatService {
  constructor(private readonly prisma: PrismaService, private readonly entityManager: EntityManager) {}

  async getChatroom(where: Prisma.VacancyChatRoomWhereInput) {
    return await this.prisma.vacancyChatRoom.findFirst({
      where,
      include: {
        internToVacancy: this.entityManager.selectInternToVacancyEntity,
      },
    });
  }

  async createChatroom({ internId, vacancyId }: CreateChatroomDto) {
    return await this.prisma.vacancyChatRoom.create({
      data: {
        internToVacancy: {
          connect: {
            vacancyId_internId: {
              internId,
              vacancyId,
            },
          },
        },
      },
    });
  }

  async getChatroomMessages(roomId: number) {
    const chatroom = await this.prisma.vacancyChatRoom.findUnique({
      where: { id: roomId },
      include: {
        internToVacancy: true,
      },
    });

    const messages = await this.prisma.vacancyChatMessage.findMany({
      where: {
        roomId,
      },
      include: {
        room: this.entityManager.selectChatroomEntity,
        companyUser: this.entityManager.selectCompanyUserEntityWithoutCompany,
      },
    });

    const announcements = await this.prisma.vacancyAnnouncement.findMany({
      where: {
        vacancyId: chatroom.internToVacancy.vacancyId,
      },
      include: {
        companyUser: this.entityManager.selectCompanyUserEntityWithoutCompany,
      },
    });

    return {
      messages,
      announcements,
    };
  }

  async getChatroomMessagesForCurrentUser(roomId: number, user: JwtValidEntity) {
    const { messages, announcements } = await this.getChatroomMessages(roomId);

    const serializedMessages = messages.map((message) =>
      this.entityManager.serializeChatMessage(message, this.isMineMessage(message, user)),
    );

    const serializedAnnouncements = announcements.map(
      (announcement) => this.entityManager.serializeAnnouncement(announcement),
      user.model === JwtAuthorizeEntity.COMPANY_USER,
    );

    const allMessages = serializedMessages
      .concat(serializedAnnouncements)
      .sort((a, b) => a.createdAt.valueOf() - b.createdAt.valueOf());

    return {
      messages: allMessages,
    };
  }

  async createMessage(createMessageDto: CreateMessageDto, user: JwtValidEntity) {
    const isCompanyUser = user.model === JwtAuthorizeEntity.COMPANY_USER;

    const restParams = {};

    if (isCompanyUser) {
      restParams['companyUser'] = {
        connect: {
          id: user.user.id,
        },
      };
    }

    const message = await this.prisma.vacancyChatMessage.create({
      data: {
        room: {
          connect: {
            id: createMessageDto.roomId,
          },
        },
        content: createMessageDto.content,
        ...restParams,
      },
      include: {
        room: this.entityManager.selectChatroomEntity,
        companyUser: this.entityManager.selectCompanyUserEntity,
      },
    });

    return this.entityManager.serializeChatMessage(message);
  }

  isMineMessage(message: Awaited<ReturnType<ChatService['getChatroomMessages']>>['messages'][0], user: JwtValidEntity) {
    switch (user.model) {
      case JwtAuthorizeEntity.INTERN: {
        return !message.companyUser && message.room.internToVacancy.internId === user.user.id;
      }
      case JwtAuthorizeEntity.COMPANY_USER: {
        return message.companyUserId === user.user.id;
      }
      default:
        return false;
    }
  }

  hasUserAccessToChat(chat: Awaited<ReturnType<ChatService['getChatroom']>>, user: JwtValidEntity) {
    switch (user.model) {
      case JwtAuthorizeEntity.INTERN: {
        return chat.internToVacancy.internId === user.user.id;
      }
      case JwtAuthorizeEntity.COMPANY_USER: {
        return chat.internToVacancy.vacancy.companyId === user.user.companyId;
      }
      default: {
        return false;
      }
    }
  }

  async getCompanyChatrooms(companyId: number) {
    const vacanciesWithChats = await this.prisma.vacancy.findMany({
      where: {
        companyId,
      },
      include: {
        internToVacancies: {
          include: {
            intern: this.entityManager.selectInternEntity,
            VacancyChatRoom: this.entityManager.selectLastChatroomMessage,
          },
        },
      },
    });

    return {
      vacancies: vacanciesWithChats.map((vacancyWithChats) =>
        this.entityManager.serializeVacancyWithChats(vacancyWithChats),
      ),
    };
  }

  async getInternChatrooms(internId: number) {
    const chatrooms = await this.prisma.internToVacancy.findMany({
      where: {
        internId,
      },
      include: {
        vacancy: this.entityManager.selectVacancyEntity,
        VacancyChatRoom: this.entityManager.selectLastChatroomMessage,
      },
    });

    return { chats: chatrooms.map((chatroom) => this.entityManager.serializeInternChats(chatroom)) };
  }
}
