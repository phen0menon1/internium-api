import { EntityManager } from '@/entity-manager';
import { Module } from '@nestjs/common';
import { PasswordModule } from '../password/password.module';
import { PrismaModule } from '../prisma/prisma.module';
import { ChatController } from './chat.controller';
import { ChatService } from './chat.service';

@Module({
  imports: [PrismaModule, PasswordModule],
  exports: [ChatService],
  providers: [ChatService, EntityManager],
  controllers: [ChatController],
})
export class ChatModule {}
