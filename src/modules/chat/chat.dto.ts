import { ApiHideProperty, ApiProperty } from '@nestjs/swagger';
import { Avatar, CompanyUserRole, VacancyChatMessage, VacancyChatRoom } from '@prisma/client';
import { Exclude, Transform, Type } from 'class-transformer';
import { IsArray, IsBoolean, IsEnum, IsNumber, IsString, MaxLength } from 'class-validator';
import { CompanyUserInfoModelPublic } from '../company-user/company-user.dto';

export class GetChatMessagesParamsDto {
  @IsNumber()
  @Transform(({ value }) => parseInt(value))
  internId: number;

  @IsNumber()
  @Transform(({ value }) => parseInt(value))
  vacancyId: number;
}

export class CreateChatroomDto {
  @IsNumber()
  vacancyId: number;

  @IsNumber()
  internId: number;
}

export class GetChatroomDto {
  @IsNumber()
  vacancyId: number;

  @IsNumber()
  internId: number;
}

export class GetChatroomByIdDto {
  @IsNumber()
  roomId: number;
}

export class CreateMessageDto {
  @IsNumber()
  roomId: number;

  @IsString()
  @MaxLength(1024)
  content: string;
}

export class MessageCompanyUserInfo {
  @IsNumber()
  id: number;

  @IsEnum(CompanyUserRole)
  @ApiProperty({ enum: CompanyUserRole })
  role: CompanyUserRole;

  @Type(() => CompanyUserInfoModelPublic)
  userInfo: CompanyUserInfoModelPublic;
}

export class MessageModel {
  @IsNumber()
  id: number;

  @IsNumber()
  roomId: number;

  @Exclude()
  @ApiHideProperty()
  room: VacancyChatRoom;

  @IsBoolean()
  isRead: boolean;

  @IsString()
  content: string;

  @IsString()
  createdAt: Date;

  @Type(() => MessageCompanyUserInfo)
  companyUser: MessageCompanyUserInfo;

  @IsBoolean()
  isMine: boolean;

  constructor(
    partial: Partial<
      MessageModel & {
        companyUser: MessageCompanyUserInfo & {
          userInfo: Partial<Omit<CompanyUserInfoModelPublic, 'avatar'>> & {
            avatar: Avatar;
          };
        };
      }
    >,
  ) {
    Object.assign(this, partial);
  }
}

export class GetChatroomResponse {
  @IsArray()
  @Type(() => MessageModel)
  messages: MessageModel[];

  chatroom: {
    id: number;
  };
}

export enum SerializedChatMessageType {
  REGULAR = 'regular',
  SYSTEM = 'system',
}
