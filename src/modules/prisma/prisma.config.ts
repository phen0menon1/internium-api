import { PrismaNotFoundException } from '@/exception/prisma-not-found.exception';
import { PrismaClientOptions } from '@prisma/client/runtime';

export const PRISMA_CLIENT_CONFIG: PrismaClientOptions = {
  rejectOnNotFound: (err) => new PrismaNotFoundException(err.message),
};
