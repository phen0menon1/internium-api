import { Global, INestApplication, Injectable, OnModuleDestroy, OnModuleInit } from '@nestjs/common';
import { Prisma, PrismaClient } from '@prisma/client';
import { PRISMA_CLIENT_CONFIG } from './prisma.config';

@Injectable()
export class PrismaService extends PrismaClient<Prisma.PrismaClientOptions> implements OnModuleInit, OnModuleDestroy {
  constructor() {
    super(PRISMA_CLIENT_CONFIG);
  }

  async onModuleInit() {
    await this.$connect();
  }

  async enableShutdownHooks(app: INestApplication) {
    this.$on('beforeExit', async () => {
      await app.close();
    });
  }

  async onModuleDestroy() {
    await this.$disconnect();
  }
}
