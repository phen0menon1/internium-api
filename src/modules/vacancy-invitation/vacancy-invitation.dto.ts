import { ApiProperty } from '@nestjs/swagger';
import { VacancyInvitation } from '@prisma/client';
import { IsBoolean, IsDate, IsNumber, IsOptional, IsString, MaxLength } from 'class-validator';

export class InvitationModel implements VacancyInvitation {
  @IsNumber()
  @ApiProperty({ example: 1 })
  id: number;

  internToVacancyId: number;

  @IsDate()
  @ApiProperty({ example: new Date() })
  createdAt: Date;

  @IsBoolean()
  archived: boolean;

  @IsBoolean()
  accepted: boolean;

  @IsBoolean()
  viewed: boolean;

  @IsBoolean()
  rejected: boolean;

  @IsNumber()
  senderId: number;
}

export class VacancyInvitationCreateDto {
  @IsNumber()
  vacancyId: number;

  @IsNumber()
  internId: number;

  @IsString()
  @MaxLength(1024)
  @IsOptional()
  message?: string;
}

export class VacancyInvitationCreateResponseDto {
  @IsBoolean()
  success: boolean;
}
