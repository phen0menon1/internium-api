import { EntityManager } from '@/entity-manager';
import { forwardRef, Module } from '@nestjs/common';
import { ChatModule } from '../chat/chat.module';
import { KafkaModule } from '../kafka/kafka.module';
import { PrismaModule } from '../prisma/prisma.module';
import { VacancyModule } from '../vacancy/vacancy.module';
import { VacancyInvitationController } from './vacancy-invitation.controller';
import { VacancyInvitationService } from './vacancy-invitation.service';

@Module({
  imports: [PrismaModule, ChatModule, forwardRef(() => VacancyModule), KafkaModule],
  providers: [VacancyInvitationService, EntityManager],
  exports: [VacancyInvitationService],
  controllers: [VacancyInvitationController],
})
export class VacancyInvitationModule {}
