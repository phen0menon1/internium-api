import { EntityManager } from '@/entity-manager';
import { CompanyUserFull, JwtAuthorizeEntity } from '@/types';
import { BadRequestException, Injectable } from '@nestjs/common';
import { Prisma } from '@prisma/client';
import { ChatService } from '../chat/chat.service';
import { KafkaProducerService } from '../kafka/kafka-producer.provider';
import { PrismaService } from '../prisma/prisma.service';
import { VacancyInvitationCreateDto } from './vacancy-invitation.dto';

@Injectable()
export class VacancyInvitationService {
  constructor(
    private readonly prisma: PrismaService,
    private readonly chatService: ChatService,
    private readonly entityManager: EntityManager,
    private readonly kafkaService: KafkaProducerService,
  ) {}

  protected async createInvitation(
    userId: number,
    data: Prisma.InternToVacancyUncheckedCreateWithoutVacancyInvitationsInput,
  ) {
    try {
      return await this.prisma.vacancyInvitation.create({
        data: {
          internToVacancy: {
            create: data,
          },
          sender: {
            connect: {
              id: userId,
            },
          },
        },
        include: {
          internToVacancy: this.entityManager.selectInternToVacancyEntity,
        },
      });
    } catch (e) {
      if (e.code === 'P2002') {
        throw new BadRequestException({
          translation: 'uniqueConstraintFailed',
          message: 'This pair of vacancy and intern already used in the database',
        });
      }
      throw e;
    }
  }

  async create({ vacancyId, internId, message }: VacancyInvitationCreateDto, user: CompanyUserFull) {
    const invitation = await this.createInvitation(user.id, { internId, vacancyId });

    const chatroom = await this.chatService.createChatroom({
      internId,
      vacancyId,
    });

    if (message) {
      // TODO: Move to kafka?
      await this.chatService.createMessage(
        {
          roomId: chatroom.id,
          content: message,
        },
        {
          model: JwtAuthorizeEntity.COMPANY_USER,
          user,
        },
      );
    }

    await this.kafkaService.produce({
      topic: 'vacancy-invitation',
      messages: [
        {
          value: JSON.stringify({
            internId,
            vacancyId,
            message,
          }),
        },
      ],
    });

    return this.entityManager.serializeInvitation(invitation);
  }

  async getById(id: number) {
    const invitation = await this.prisma.vacancyInvitation.findUnique({
      where: { id },
      include: {
        internToVacancy: this.entityManager.selectInternToVacancyEntity,
      },
    });
    return this.entityManager.serializeInvitation(invitation);
  }

  async getListByVacancyId(vacancyId: number) {
    const invitations = await this.prisma.vacancyInvitation.findMany({
      where: {
        internToVacancy: {
          vacancyId,
        },
      },
      include: {
        internToVacancy: this.entityManager.selectInternToVacancyEntity,
      },
    });

    return { invitations: invitations.map((i) => this.entityManager.serializeInvitation(i)) };
  }

  async getListByInternId(internId: number) {
    const invitations = await this.prisma.vacancyInvitation.findMany({
      where: {
        internToVacancy: {
          internId,
        },
      },
      include: {
        internToVacancy: this.entityManager.selectInternToVacancyEntity,
      },
    });
    return invitations.map((i) => this.entityManager.serializeInvitation(i));
  }

  async reject(id: number) {
    const invitation = await this.prisma.vacancyInvitation.update({
      where: { id },
      data: { rejected: true },
      include: {
        internToVacancy: this.entityManager.selectInternToVacancyEntity,
      },
    });
    return this.entityManager.serializeInvitation(invitation);
  }

  async accept(id: number) {
    const invitation = await this.prisma.vacancyInvitation.update({
      where: { id },
      data: { accepted: true },
      include: {
        internToVacancy: this.entityManager.selectInternToVacancyEntity,
      },
    });
    return this.entityManager.serializeInvitation(invitation);
  }

  async archive(id: number) {
    const invitation = await this.prisma.vacancyInvitation.update({
      where: { id },
      data: { archived: true },
      include: {
        internToVacancy: this.entityManager.selectInternToVacancyEntity,
      },
    });
    return this.entityManager.serializeInvitation(invitation);
  }
}
