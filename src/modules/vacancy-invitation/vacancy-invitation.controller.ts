import { CurrentCompanyUser } from '@/decorators/current-company-user.decorator';
import { CurrentInternUser } from '@/decorators/current-intern-user';
import { CompanyGuard } from '@/guards/company.guard';
import { InternGuard } from '@/guards/intern.guard';
import { CompanyUserFull, FullInternUser } from '@/types';
import { BadRequestException, Body, Controller, ForbiddenException, Param, Post, UseGuards } from '@nestjs/common';
import {
  ApiBadRequestResponse,
  ApiBearerAuth,
  ApiCreatedResponse,
  ApiForbiddenResponse,
  ApiOperation,
  ApiTags,
  ApiUnauthorizedResponse,
} from '@nestjs/swagger';
import { VacancyService } from '../vacancy/vacancy.service';
import { VacancyInvitationCreateDto, VacancyInvitationCreateResponseDto } from './vacancy-invitation.dto';
import { VacancyInvitationService } from './vacancy-invitation.service';

@Controller('vacancy-invitations')
@ApiTags('Vacancy Invitations')
export class VacancyInvitationController {
  constructor(
    private readonly vacancyService: VacancyService,
    private readonly vacancyInvitationService: VacancyInvitationService,
  ) {}

  @Post('/')
  @UseGuards(CompanyGuard)
  @ApiBearerAuth('Bearer')
  @ApiOperation({ summary: '[C] Create vacancy invitation for an intern' })
  @ApiCreatedResponse({
    status: 201,
    type: VacancyInvitationCreateResponseDto,
    description: 'The record has been successfully created. Automatically creates chatroom, too',
  })
  @ApiBadRequestResponse({ description: 'Invalid form data' })
  @ApiUnauthorizedResponse({ description: 'Invalid JWT signature' })
  @ApiForbiddenResponse({
    description: "['You have no permission to this resource', 'Cannot invite to the archived vacancy']",
  })
  async createInvitation(
    @CurrentCompanyUser() companyUser: CompanyUserFull,
    @Body() invitationCreateDto: VacancyInvitationCreateDto,
  ) {
    const vacancy = await this.vacancyService.getMinimal(invitationCreateDto.vacancyId);

    if (vacancy.companyId !== companyUser.companyId) {
      throw new ForbiddenException('You have no permission to this resource');
    }

    if (vacancy.archived) {
      throw new ForbiddenException('Cannot invite to the archived vacancy');
    }

    return await this.vacancyInvitationService.create(invitationCreateDto, companyUser);
  }

  @Post('/:id/revoke')
  @UseGuards(CompanyGuard)
  @ApiBearerAuth('Bearer')
  @ApiOperation({ summary: '[C] Revoke vacancy invitation' })
  @ApiCreatedResponse({
    status: 201,
    type: VacancyInvitationCreateResponseDto,
    description: 'An invitation has been successfully revoked',
  })
  @ApiBadRequestResponse({
    description:
      "['Invalid form data', 'Cannot revoke invitation on an rejected vacancy', 'Cannot revoke invitation on an archived vacancy']",
  })
  @ApiForbiddenResponse({ description: 'You have no permission for this resource' })
  @ApiUnauthorizedResponse({ description: 'Invalid JWT signature' })
  async revokeInvitation(@Param('id') id: number, @CurrentCompanyUser() user: CompanyUserFull) {
    const invitation = await this.vacancyInvitationService.getById(id);

    if (invitation.vacancy.companyId !== user.companyId) {
      throw new ForbiddenException('You have no permission to this resource');
    }

    if (invitation.rejected) {
      throw new BadRequestException('Cannot revoke invitation on an rejected invitation');
    }

    if (invitation.archived) {
      throw new BadRequestException('Cannot revoke invitation on an archived invitation');
    }

    await this.vacancyInvitationService.archive(invitation.id);

    return { success: true };
  }

  @Post('/:id/reject')
  @UseGuards(InternGuard)
  @ApiBearerAuth('Bearer')
  @ApiOperation({ summary: '[I] Reject vacancy invitation' })
  @ApiCreatedResponse({
    status: 201,
    type: VacancyInvitationCreateResponseDto,
    description: 'An invitation has been successfully rejected',
  })
  @ApiBadRequestResponse({ description: 'Invalid form data' })
  @ApiUnauthorizedResponse({ description: 'Invalid JWT signature' })
  @ApiForbiddenResponse({ description: 'You have no permission for this resource' })
  async rejectInvitation(@Param('id') id: number, @CurrentInternUser() user: FullInternUser) {
    const invitation = await this.vacancyInvitationService.getById(id);

    if (invitation.accepted) {
      throw new ForbiddenException('Cannot reject accepted invitation');
    }

    if (invitation.archived) {
      throw new ForbiddenException('Cannot reject archived invitation');
    }

    if (invitation.intern.id !== user.id) {
      throw new ForbiddenException('You have no permission to this resource');
    }

    await this.vacancyInvitationService.reject(id);

    return { success: true };
  }

  @Post('/:id/accept')
  @UseGuards(InternGuard)
  @ApiBearerAuth('Bearer')
  @ApiOperation({ summary: '[I] Accept vacancy invitation' })
  @ApiCreatedResponse({
    status: 201,
    type: VacancyInvitationCreateResponseDto,
    description: 'An invitation has been successfully accepted',
  })
  @ApiBadRequestResponse({ description: 'Invalid form data' })
  @ApiUnauthorizedResponse({ description: 'Invalid JWT signature' })
  @ApiForbiddenResponse({ description: 'You have no permission for this resource' })
  async acceptInvitation(@Param('id') id: number, @CurrentInternUser() user: FullInternUser) {
    const invitation = await this.vacancyInvitationService.getById(id);

    if (invitation.archived) {
      throw new ForbiddenException('Cannot accept archived invitation');
    }

    if (invitation.rejected) {
      throw new ForbiddenException('Cannot accept rejected invitation');
    }

    if (invitation.accepted) {
      throw new ForbiddenException('Invitation is already accepted');
    }

    if (invitation.intern.id !== user.id) {
      throw new ForbiddenException('You have no permission to this resource');
    }

    await this.vacancyInvitationService.accept(id);

    return { success: true };
  }
}
