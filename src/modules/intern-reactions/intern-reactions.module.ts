import { EntityManager } from '@/entity-manager';
import { forwardRef, Module } from '@nestjs/common';
import { ChatModule } from '../chat/chat.module';
import { JwtModule } from '../jwt/jwt.module';
import { PrismaModule } from '../prisma/prisma.module';
import { VacancyModule } from '../vacancy/vacancy.module';
import { InternReactionsController } from './intern-reactions.controller';
import { InternReactionsService } from './intern-reactions.service';

@Module({
  imports: [PrismaModule, JwtModule, forwardRef(() => VacancyModule), ChatModule],
  providers: [InternReactionsService, EntityManager],
  exports: [InternReactionsService],
  controllers: [InternReactionsController],
})
export class InternReactionsModule {}
