import { EntityManager } from '@/entity-manager';
import { BadRequestException, Injectable } from '@nestjs/common';
import { Intern, InternReactions, InternToVacancy, Prisma, Vacancy } from '@prisma/client';
import { omit } from 'lodash';

import { PrismaService } from 'src/modules/prisma/prisma.service';
import { ChatService } from '../chat/chat.service';
import { InternCreateReactionDto } from './intern-reactions.dto';

@Injectable()
export class InternReactionsService {
  constructor(
    private readonly prisma: PrismaService,
    private readonly chatService: ChatService,
    private readonly entityManager: EntityManager,
  ) {}

  protected async createReaction(data: Prisma.InternToVacancyUncheckedCreateWithoutVacancyInvitationsInput) {
    try {
      return await this.prisma.internReactions.create({
        data: {
          internToVacancy: {
            create: data,
          },
        },
        include: {
          internToVacancy: this.entityManager.selectInternToVacancyEntity,
        },
      });
    } catch (e) {
      if (e.code === 'P2002') {
        throw new BadRequestException({
          translation: 'uniqueConstraintFailed',
          message: 'This pair of vacancy and intern already used in the database',
        });
      }
      throw e;
    }
  }

  async create({ vacancyId }: InternCreateReactionDto, { id: internId }: Intern) {
    const reaction = await this.createReaction({ vacancyId, internId });

    await this.chatService.createChatroom({
      internId,
      vacancyId,
    });

    return this.entityManager.serializeReaction(reaction);
  }

  // async delete(reactionData: InternDeleteReactionDto, intern: Intern): Promise<Boolean> {
  //   await this.prisma.internReactions.delete({
  //     where: {
  //       id: reactionData.reactionId,
  //     },
  //   });
  //   return true;
  // }

  async reject(id: number) {
    const reaction = await this.prisma.internReactions.update({
      where: { id },
      data: { rejected: true },
      include: {
        internToVacancy: this.entityManager.selectInternToVacancyEntity,
      },
    });
    return this.entityManager.serializeReaction(reaction);
  }

  async accept(id: number) {
    const reaction = await this.prisma.internReactions.update({
      where: { id },
      data: { accepted: true },
      include: {
        internToVacancy: this.entityManager.selectInternToVacancyEntity,
      },
    });
    return this.entityManager.serializeReaction(reaction);
  }

  async archive(id: number) {
    const reaction = await this.prisma.internReactions.update({
      where: { id },
      data: { archived: true },
      include: {
        internToVacancy: this.entityManager.selectInternToVacancyEntity,
      },
    });
    return this.entityManager.serializeReaction(reaction);
  }

  async getById(id: number) {
    const reaction = await this.prisma.internReactions.findUnique({
      where: {
        id,
      },
      include: {
        internToVacancy: this.entityManager.selectInternToVacancyEntity,
      },
    });
    return this.entityManager.serializeReaction(reaction);
  }

  async getByVacancyId(vacancyId: number) {
    const reaction = await this.prisma.internReactions.findFirst({
      where: {
        internToVacancy: {
          vacancyId,
        },
      },
      include: {
        internToVacancy: this.entityManager.selectInternToVacancyEntity,
      },
    });
    return this.entityManager.serializeReaction(reaction);
  }

  async getReactionListByVacancyId(id: number) {
    const reactions = await this.prisma.internReactions.findMany({
      where: {
        internToVacancy: {
          vacancyId: id,
        },
      },
      include: {
        internToVacancy: this.entityManager.selectInternToVacancyEntity,
      },
    });
    return { reactions: reactions.map((r) => this.entityManager.serializeReaction(r)) };
  }

  async getListByInternId(internId: number) {
    const reactions = await this.prisma.internReactions.findMany({
      where: {
        internToVacancy: {
          internId,
        },
      },
      include: {
        internToVacancy: this.entityManager.selectInternToVacancyEntity,
      },
    });
    return reactions.map((r) => this.entityManager.serializeReaction(r));
  }

  async isReactionExists(internId: number, vacancyId: number) {
    const reaction = await this.prisma.internReactions.findFirst({
      where: {
        internToVacancy: {
          internId,
          vacancyId,
        },
      },
      include: {
        internToVacancy: this.entityManager.selectInternToVacancyEntity,
      },
      rejectOnNotFound: false,
    });
    return Boolean(reaction);
  }
}
