import { CurrentCompanyUser } from '@/decorators/current-company-user.decorator';
import { CurrentInternUser } from '@/decorators/current-intern-user';
import { CompanyGuard } from '@/guards/company.guard';
import { InternGuard } from '@/guards/intern.guard';
import { CompanyUserFull, FullInternUser } from '@/types';
import { Post, Body, Param, UseGuards, Controller, ForbiddenException, BadRequestException } from '@nestjs/common';
import {
  ApiTags,
  ApiResponse,
  ApiOperation,
  ApiBearerAuth,
  ApiCreatedResponse,
  ApiForbiddenResponse,
  ApiBadRequestResponse,
  ApiUnauthorizedResponse,
} from '@nestjs/swagger';
import { Intern } from '@prisma/client';
import { VacancyInvitationCreateResponseDto } from '../vacancy-invitation/vacancy-invitation.dto';
import { VacancyService } from '../vacancy/vacancy.service';
import { InternCreateReactionDto, InternCreateReactionResponseDto } from './intern-reactions.dto';
import { InternReactionsService } from './intern-reactions.service';

@Controller('reactions')
@ApiTags('Intern Reactions')
export class InternReactionsController {
  constructor(
    private readonly vacancyService: VacancyService,
    private readonly internReactionsService: InternReactionsService,
  ) {}

  @Post('/')
  @UseGuards(InternGuard)
  @ApiOperation({ summary: '[I] Create vacancy reaction of an intern' })
  @ApiResponse({
    status: 201,
    type: InternCreateReactionResponseDto,
    description: 'The record has been successfully created. Automatically creates chatroom, too',
  })
  async create(@Body() reaction: InternCreateReactionDto, @CurrentInternUser() intern: Intern) {
    const vacancy = await this.vacancyService.getMinimal(reaction.vacancyId);
    if (vacancy.archived) {
      throw new ForbiddenException('Cannot react to the archived vacancy');
    }
    return await this.internReactionsService.create(reaction, intern);
  }

  // @Delete('/')
  // @HttpCode(200)
  // @UseGuards(InternGuard)
  // @ApiOperation({ summary: '[I] Delete vacancy reaction of an intern' })
  // @ApiResponse({
  //   status: 200,
  //   type: InternDeleteReactionResponseDto,
  //   description: 'The record has been successfully deleted',
  // })
  // async delete(@Body() deleteReactionDto: InternDeleteReactionDto, @CurrentInternUser() intern: Intern) {
  //   const reaction = await this.internReactionsService.getById(deleteReactionDto.reactionId);
  //   if (reaction.intern.id !== intern.id) {
  //     throw new ForbiddenException('You have no permission to this resource');
  //   }
  //   await this.internReactionsService.delete(deleteReactionDto, intern);
  //   return { success: true };
  // }

  @Post('/:id/reject')
  @UseGuards(CompanyGuard)
  @ApiBearerAuth('Bearer')
  @ApiOperation({ summary: '[I] Reject intern reaction' })
  @ApiCreatedResponse({
    status: 201,
    type: VacancyInvitationCreateResponseDto,
    description: 'The reaction has been successfully rejected',
  })
  @ApiBadRequestResponse({ description: 'Invalid form data' })
  @ApiUnauthorizedResponse({ description: 'Invalid JWT signature' })
  @ApiForbiddenResponse({ description: 'You have no permission for this resource' })
  async rejectReaction(@Param('id') id: number, @CurrentCompanyUser() user: CompanyUserFull) {
    const reaction = await this.internReactionsService.getById(id);

    if (reaction.vacancy.companyId !== user.companyId) {
      throw new ForbiddenException('You have no permission to this resource');
    }

    await this.internReactionsService.reject(id);

    return { success: true };
  }

  @Post('/:id/accept')
  @UseGuards(CompanyGuard)
  @ApiBearerAuth('Bearer')
  @ApiOperation({ summary: '[I] Accept intern reaction' })
  @ApiCreatedResponse({
    status: 201,
    type: VacancyInvitationCreateResponseDto,
    description: 'The reaction has been successfully accepted',
  })
  @ApiBadRequestResponse({ description: 'Invalid form data' })
  @ApiUnauthorizedResponse({ description: 'Invalid JWT signature' })
  @ApiForbiddenResponse({ description: 'You have no permission for this resource' })
  async acceptReaction(@Param('id') id: number, @CurrentCompanyUser() user: CompanyUserFull) {
    const reaction = await this.internReactionsService.getById(id);

    if (reaction.vacancy.companyId !== user.companyId) {
      throw new ForbiddenException('You have no permission to this resource');
    }

    await this.internReactionsService.accept(id);

    return { success: true };
  }

  @Post('/:id/revoke')
  @UseGuards(InternGuard)
  @ApiBearerAuth('Bearer')
  @ApiOperation({ summary: '[I] Revoke intern reaction' })
  @ApiCreatedResponse({
    status: 201,
    type: VacancyInvitationCreateResponseDto,
    description: 'The reaction has been successfully revoked',
  })
  @ApiBadRequestResponse({
    description:
      "['Invalid form data', 'Cannot revoke reaction on an rejected vacancy', 'Cannot revoke reaction on an archived vacancy']",
  })
  @ApiForbiddenResponse({ description: 'You have no permission for this resource' })
  @ApiUnauthorizedResponse({ description: 'Invalid JWT signature' })
  async revokeInvitation(@Param('id') id: number, @CurrentInternUser() user: FullInternUser) {
    const reaction = await this.internReactionsService.getById(id);

    if (reaction.intern.id !== user.id) {
      throw new ForbiddenException('You have no permission to this resource');
    }

    if (reaction.rejected) {
      throw new BadRequestException('Cannot revoke reaction on an rejected reaction');
    }

    if (reaction.archived) {
      throw new BadRequestException('Cannot revoke reaction on an archived reaction');
    }

    await this.internReactionsService.archive(reaction.id);

    return { success: true };
  }
}
