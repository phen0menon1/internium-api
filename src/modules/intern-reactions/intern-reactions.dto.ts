import { ApiProperty } from '@nestjs/swagger';
import { InternReactions } from '@prisma/client';
import { Transform } from 'class-transformer';
import { IsBoolean, IsDate, IsNumber } from 'class-validator';

export class ReactionModel implements InternReactions {
  @IsNumber()
  @ApiProperty({ example: 1 })
  id: number;

  @IsNumber()
  @ApiProperty({ example: 7 })
  internId: number;

  @IsNumber()
  @ApiProperty({ example: 11 })
  vacancyId: number;

  @IsBoolean()
  rejected: boolean;

  @IsBoolean()
  archived: boolean;

  @IsBoolean()
  accepted: boolean;

  @IsBoolean()
  viewed: boolean;

  @IsNumber()
  internToVacancyId: number;

  @IsDate()
  @ApiProperty({ example: new Date() })
  createdAt: Date;
}

export class InternCreateReactionDto {
  @IsNumber()
  vacancyId: number;
}

export class InternDeleteReactionDto {
  @IsNumber()
  reactionId: number;
}

export class InternCreateReactionResponseDto {
  @IsBoolean()
  success: boolean;
}

export class InternDeleteReactionResponseDto {
  @IsBoolean()
  success: boolean;
}

export class FilterReactionsDto {
  @IsNumber()
  @Transform(({ value }) => parseInt(value))
  vacancyId: number;
}

export class CurrentInternReactionsResponse {
  @IsNumber()
  id: number;

  @IsNumber()
  internId: number;

  @IsNumber()
  vacancyId: number;

  @IsDate()
  createdAt: Date;
}
