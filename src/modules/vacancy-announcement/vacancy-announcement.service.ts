import { CompanyUserFull } from '@/types';
import { Injectable } from '@nestjs/common';
import { CompanyUserRole } from '@prisma/client';
import { PrismaService } from '../prisma/prisma.service';
import { CreateAnnouncementDto } from './vacancy-announcement.dto';

@Injectable()
export class VacancyAnnouncementService {
  constructor(private readonly prisma: PrismaService) {}

  async createAnnouncement(createAnnouncementDto: CreateAnnouncementDto, user: CompanyUserFull) {
    await this.prisma.vacancyAnnouncement.create({
      data: {
        message: createAnnouncementDto.message,
        vacancy: {
          connect: {
            id: createAnnouncementDto.vacancyId,
          },
        },
        companyUser: {
          connect: {
            id: user.id,
          },
        },
      },
    });
  }
}
