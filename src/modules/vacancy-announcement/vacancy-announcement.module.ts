import { Module } from '@nestjs/common';
import { PrismaModule } from '../prisma/prisma.module';
import { VacancyModule } from '../vacancy/vacancy.module';
import { VacancyAnnouncementController } from './vacancy-announcement.controller';
import { VacancyAnnouncementService } from './vacancy-announcement.service';

@Module({
  imports: [PrismaModule, VacancyModule],
  providers: [VacancyAnnouncementService],
  exports: [VacancyAnnouncementService],
  controllers: [VacancyAnnouncementController],
})
export class VacancyAnnouncementModule {}
