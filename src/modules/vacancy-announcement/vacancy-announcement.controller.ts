import { CurrentCompanyUser } from '@/decorators/current-company-user.decorator';
import { CompanyGuard } from '@/guards/company.guard';
import { CompanyUserFull } from '@/types';
import { Body, Controller, ForbiddenException, HttpCode, Post, UseGuards } from '@nestjs/common';
import {
  ApiBadRequestResponse,
  ApiBearerAuth,
  ApiOkResponse,
  ApiOperation,
  ApiTags,
  ApiUnauthorizedResponse,
} from '@nestjs/swagger';
import { VacancyService } from '../vacancy/vacancy.service';
import { CreateAnnouncementDto } from './vacancy-announcement.dto';
import { VacancyAnnouncementService } from './vacancy-announcement.service';

@Controller('vacancy-announcements')
@ApiTags('Vacancy Announcements')
export class VacancyAnnouncementController {
  constructor(
    private readonly vacancyService: VacancyService,
    private readonly vacancyAnnouncementService: VacancyAnnouncementService,
  ) {}

  @Post('/')
  @HttpCode(200)
  @UseGuards(CompanyGuard)
  @ApiBearerAuth('Bearer')
  @ApiOperation({ summary: '[C] Post vacancy announcement' })
  @ApiOkResponse({ description: 'Create vacancy announcement' })
  @ApiBadRequestResponse({ description: 'Invalid form data' })
  @ApiUnauthorizedResponse({ description: 'Invalid JWT signature' })
  async createAnnouncement(
    @CurrentCompanyUser() user: CompanyUserFull,
    @Body() createAnnouncementDto: CreateAnnouncementDto,
  ) {
    const vacancy = await this.vacancyService.getMinimal(createAnnouncementDto.vacancyId);

    if (vacancy.companyId !== user.companyId) {
      throw new ForbiddenException('You have no permission to this resource');
    }

    await this.vacancyAnnouncementService.createAnnouncement(createAnnouncementDto, user);

    return { success: true };
  }
}
