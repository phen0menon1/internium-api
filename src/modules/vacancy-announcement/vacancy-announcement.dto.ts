import { IsNumber, IsString } from 'class-validator';

export class CreateAnnouncementDto {
  @IsNumber()
  vacancyId: number;

  @IsString()
  message: string;
}
