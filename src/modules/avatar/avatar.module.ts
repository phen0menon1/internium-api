import { imageFileEditName } from '@/utils/files';
import { Module } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { MulterModule } from '@nestjs/platform-express';
import { diskStorage } from 'multer';
import { CompanyUserModule } from '../company-user/company-user.module';
import { InternModule } from '../intern/intern.module';
import { JwtModule } from '../jwt/jwt.module';
import { PrismaModule } from '../prisma/prisma.module';
import { AvatarController } from './avatar.controller';

@Module({
  imports: [
    PrismaModule,
    JwtModule,
    InternModule,
    CompanyUserModule,
    MulterModule.registerAsync({
      useFactory: async (config: ConfigService) => {
        return {
          storage: diskStorage({
            destination: config.get('UPLOAD_DEST'),
            filename: imageFileEditName,
          }),
        };
      },
      inject: [ConfigService],
    }),
  ],
  providers: [],
  exports: [],
  controllers: [AvatarController],
})
export class AvatarModule {}
