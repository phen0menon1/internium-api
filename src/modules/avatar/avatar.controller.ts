import { CurrentUser } from '@/decorators/current-user.decorator';
import { AuthorizedGuard } from '@/guards/authorized.guard';
import { JwtAuthorizeEntity, JwtValidEntity } from '@/types';
import { imageFileFilter } from '@/utils/files';
import {
  Controller,
  Delete,
  ForbiddenException,
  HttpCode,
  Post,
  UnauthorizedException,
  UploadedFile,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import {
  ApiBody,
  ApiConsumes,
  ApiCreatedResponse,
  ApiForbiddenResponse,
  ApiOkResponse,
  ApiOperation,
  ApiTags,
  ApiUnauthorizedResponse,
} from '@nestjs/swagger';
import { CompanyUserRole } from '@prisma/client';
import { CompanyUserService } from '../company-user/company-user.service';
import { InternService } from '../intern/intern.service';

@Controller('avatars')
@ApiTags('Avatars')
export class AvatarController {
  constructor(private readonly internService: InternService, private readonly companyUserService: CompanyUserService) {}

  @Delete('/')
  @HttpCode(200)
  @UseGuards(AuthorizedGuard)
  @ApiOperation({ summary: '[A] Remove avatar of current user' })
  @ApiOkResponse({ description: 'Avatar deleted' })
  @ApiUnauthorizedResponse({ description: 'Unauthorized' })
  @ApiForbiddenResponse({ description: 'Company owners cannot upload avatars.' })
  async deleteAvatar(@CurrentUser() user: JwtValidEntity) {
    if (user.model === JwtAuthorizeEntity.COMPANY_USER && user.user.role === CompanyUserRole.OWNER) {
      throw new ForbiddenException('Company owners cannot upload avatars. Upload company logo instead');
    }

    switch (user.model) {
      case JwtAuthorizeEntity.INTERN: {
        await this.internService.deleteAvatar(user.user.id);
        break;
      }
      case JwtAuthorizeEntity.COMPANY_USER: {
        await this.companyUserService.deleteAvatar(user.user.id);
        break;
      }
      default: {
        throw new UnauthorizedException();
      }
    }
    return { success: true };
  }

  @Post('/')
  @UseGuards(AuthorizedGuard)
  @UseInterceptors(
    FileInterceptor('file', {
      fileFilter: imageFileFilter,
      limits: {
        fileSize: 5 * 1024 * 1024 * 8,
      },
    }),
  )
  @ApiOperation({ summary: '[A] Add avatar to current user' })
  @ApiCreatedResponse({ description: 'Avatar created' })
  @ApiUnauthorizedResponse({ description: 'Unauthorized' })
  @ApiForbiddenResponse({ description: 'Company owners cannot upload avatars. Upload company logo instead' })
  @ApiConsumes('multipart/form-data')
  @ApiBody({
    schema: {
      type: 'object',
      properties: {
        file: {
          type: 'string',
          format: 'binary',
        },
      },
    },
  })
  async createAvatar(@CurrentUser() user: JwtValidEntity, @UploadedFile() file: Express.Multer.File) {
    switch (user.model) {
      case JwtAuthorizeEntity.INTERN: {
        await this.internService.updateAvatar(user.user.id, file);
        break;
      }
      case JwtAuthorizeEntity.COMPANY_USER: {
        if (user.user.role === CompanyUserRole.OWNER) {
          throw new ForbiddenException('Company owners cannot upload avatars. Upload company logo instead');
        }
        await this.companyUserService.updateAvatar(user.user.id, file);
        break;
      }
      default: {
        throw new UnauthorizedException();
      }
    }
    return { avatar: file.path };
  }
}
