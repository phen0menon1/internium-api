import { EntityManager } from '@/entity-manager';
import { BadRequestException, Injectable } from '@nestjs/common';
import { VacancyPaidStatus } from '@prisma/client';
import { extend, omit } from 'lodash';
import { PrismaService } from '../prisma/prisma.service';
import {
  CreateVacancyDto,
  UpdateVacancyDto,
  VacanciesFilterDto,
  VacanciesSortBy,
  VacanciesSortType,
} from './vacancy.dto';

@Injectable()
export class VacancyService {
  constructor(private readonly prisma: PrismaService, private readonly entityManager: EntityManager) {}

  async getMinimal(id: number) {
    return await this.prisma.vacancy.findUnique({ where: { id } });
  }

  async getByCompanyId(id: number) {
    const vacancies = await this.prisma.company.findFirst({ where: { id } }).vacancies({
      include: this.entityManager.vacancyFullEntity,
    });

    return {
      vacancies: vacancies.map((v) => this.entityManager.serializeVacancy(v)),
    };
  }

  async get(id: number) {
    const vacancy = await this.prisma.vacancy.findUnique({
      where: { id },
      include: this.entityManager.vacancyFullEntity,
    });

    return this.entityManager.serializeVacancy(vacancy);
  }

  private getOrderByFilter = (sortBy: VacanciesSortBy, sortType: VacanciesSortType) => {
    const field = sortBy ? { updatedAt: 'updatedAt', salary: 'salary', bestMatch: 'id' }[sortBy] : 'id';
    return { [field]: sortType };
  };

  async getFiltered(filters: VacanciesFilterDto) {
    const orderBy = this.getOrderByFilter(filters.sortBy, filters.sortType);
    const contains = filters.search || '';

    const vacancies = await this.prisma.vacancy.findMany({
      where: {
        OR: [{ title: { contains, mode: 'insensitive' } }, { description: { contains, mode: 'insensitive' } }],
        abilities: {
          some: {
            OR: filters.abilities?.map((id) => ({ abilityId: id })),
          },
        },
        paid: filters.paid,
        location: filters.location,
        salary: {
          gte: filters.salaryFrom,
          lte: filters.salaryTo,
        },
        archived: false,
      },
      include: this.entityManager.vacancyFullEntity,
      orderBy,
    });

    return { vacancies: vacancies.map((v) => this.entityManager.serializeVacancy(v)) };
  }

  async update(id: number, data: UpdateVacancyDto) {
    const vacancy = await this.prisma.vacancy.findUnique({
      where: { id },
      include: {
        abilities: true,
      },
    });

    if (data.salary) data.paid = VacancyPaidStatus.PAID;

    const shouldRemoveAbilities = data.abilities.length > 0;

    const transaction = [];

    if (shouldRemoveAbilities) {
      transaction.push(
        this.prisma.vacancy.update({
          where: { id },
          data: {
            abilities: {
              deleteMany: vacancy.abilities.map(({ id }) => ({ id })),
            },
          },
        }),
      );
    }

    transaction.push(
      this.prisma.vacancy.update({
        where: { id },
        data: extend(omit(data, 'abilities'), {
          abilities: {
            create: data.abilities?.map((id) => ({
              abilityId: id,
            })),
          },
        }),
        include: this.entityManager.vacancyFullEntity,
      }),
    );

    const transactionResult = await this.prisma.$transaction(transaction);
    const updatedVacancy = transactionResult.pop();

    return this.entityManager.serializeVacancy(updatedVacancy);
  }

  async archive(vacancyId: number) {
    const vacancy = await this.prisma.vacancy.update({
      where: { id: vacancyId },
      data: {
        archived: true,
      },
      include: this.entityManager.vacancyFullEntity,
    });
    return this.entityManager.serializeVacancy(vacancy);
  }

  async create(data: CreateVacancyDto & { companyId: number }) {
    const { abilities } = data;

    if (abilities.length > 2) {
      throw new BadRequestException('Provided 3 or more abilities, allowed: 2');
    }

    if (data.salary) {
      data.paid = VacancyPaidStatus.PAID;
    }

    const vacancyParams = omit(data, 'abilities');

    const vacancy = await this.prisma.vacancy.create({
      data: extend(vacancyParams, {
        abilities: {
          create: data.abilities.map((id) => ({
            abilityId: id,
          })),
        },
      }),
      include: this.entityManager.vacancyFullEntity,
    });

    return this.entityManager.serializeVacancy(vacancy);
  }
}
