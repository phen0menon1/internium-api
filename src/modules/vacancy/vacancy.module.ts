import { EntityManager } from '@/entity-manager';
import { forwardRef, Module } from '@nestjs/common';
import { InternReactionsModule } from '../intern-reactions/intern-reactions.module';
import { PrismaModule } from '../prisma/prisma.module';
import { VacancyInvitationModule } from '../vacancy-invitation/vacancy-invitation.module';
import { VacancyController } from './vacancy.controller';
import { VacancyService } from './vacancy.service';

@Module({
  imports: [PrismaModule, forwardRef(() => InternReactionsModule), forwardRef(() => VacancyInvitationModule)],
  providers: [VacancyService, EntityManager],
  exports: [VacancyService],
  controllers: [VacancyController],
})
export class VacancyModule {}
