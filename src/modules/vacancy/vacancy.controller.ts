import { CurrentCompanyUser } from '@/decorators/current-company-user.decorator';
import { CurrentUser } from '@/decorators/current-user.decorator';
import { AuthorizedOrAnyGuard } from '@/guards/authorized-or-any.guard';
import { CompanyGuard } from '@/guards/company.guard';
import { CompanyUserFull, JwtAuthorizeEntity, JwtValidEntity } from '@/types';
import {
  Put,
  Get,
  Body,
  Post,
  Query,
  Param,
  HttpCode,
  UseGuards,
  Controller,
  ForbiddenException,
} from '@nestjs/common';
import {
  ApiTags,
  ApiOperation,
  ApiOkResponse,
  ApiBearerAuth,
  ApiCreatedResponse,
  ApiNotFoundResponse,
  ApiBadRequestResponse,
  ApiUnauthorizedResponse,
} from '@nestjs/swagger';
import { InternReactionsService } from '../intern-reactions/intern-reactions.service';
import { VacancyInvitationService } from '../vacancy-invitation/vacancy-invitation.service';
import {
  BaseVacancyEntity,
  CreateVacancyDto,
  UpdateVacancyDto,
  VacanciesFilterDto,
  VacanciesListResponse,
} from './vacancy.dto';
import { VacancyService } from './vacancy.service';

@Controller('vacancies')
@ApiTags('Vacancies')
export class VacancyController {
  constructor(
    private readonly vacancyService: VacancyService,
    private readonly internReactionsService: InternReactionsService,
    private readonly vacancyInvitationService: VacancyInvitationService,
  ) {}

  @Post('/')
  @UseGuards(CompanyGuard)
  @ApiBearerAuth('Bearer')
  @ApiOperation({ summary: '[C] Create a vacancy' })
  @ApiCreatedResponse({ description: 'The record has been successfully created.', type: BaseVacancyEntity })
  @ApiBadRequestResponse({ description: 'Invalid form data' })
  @ApiUnauthorizedResponse({ description: 'Invalid JWT signature' })
  async createVacancy(@Body() vacancyData: CreateVacancyDto, @CurrentCompanyUser() user: CompanyUserFull) {
    return await this.vacancyService.create({ ...vacancyData, companyId: user.companyId });
  }

  @Get('/:id/reactions')
  @HttpCode(200)
  @UseGuards(CompanyGuard)
  @ApiBearerAuth('Bearer')
  @ApiOperation({ summary: '[C] Get vacancy reactions' })
  @ApiOkResponse({ description: 'Return vacancy reactions' })
  @ApiBadRequestResponse({ description: 'Invalid form data' })
  @ApiUnauthorizedResponse({ description: 'Invalid JWT signature' })
  async getVacancyReactions(@Param('id') id: number, @CurrentCompanyUser() user: CompanyUserFull) {
    const vacancy = await this.vacancyService.getMinimal(id);

    if (vacancy.companyId !== user.companyId) {
      throw new ForbiddenException('You have no permission to this resource');
    }

    return await this.internReactionsService.getReactionListByVacancyId(vacancy.id);
  }

  @Get('/:id/invitations')
  @HttpCode(200)
  @UseGuards(CompanyGuard)
  @ApiBearerAuth('Bearer')
  @ApiOperation({ summary: '[C] Get vacancy invitations' })
  @ApiOkResponse({ description: 'Return vacancy invitations' })
  @ApiBadRequestResponse({ description: 'Invalid form data' })
  @ApiUnauthorizedResponse({ description: 'Invalid JWT signature' })
  async getVacancyInvitations(@Param('id') id: number, @CurrentCompanyUser() user: CompanyUserFull) {
    const vacancy = await this.vacancyService.getMinimal(id);

    if (vacancy.companyId !== user.companyId) {
      throw new ForbiddenException('You have no permission to this resource');
    }

    return await this.vacancyInvitationService.getListByVacancyId(vacancy.id);
  }

  @Post('/:id/archive')
  @HttpCode(200)
  @UseGuards(CompanyGuard)
  @ApiBearerAuth('Bearer')
  @ApiOperation({ summary: '[C] Archive invitation' })
  @ApiOkResponse({ description: 'Return archived vacancy' })
  @ApiBadRequestResponse({ description: 'Invalid form data' })
  @ApiUnauthorizedResponse({ description: 'Invalid JWT signature' })
  async archiveVacancy(@Param('id') id: number, @CurrentCompanyUser() user: CompanyUserFull) {
    const vacancy = await this.vacancyService.getMinimal(id);

    if (vacancy.archived) {
      throw new ForbiddenException('Cannot archive archived vacancy');
    }

    if (vacancy.companyId !== user.companyId) {
      throw new ForbiddenException('You have no permission to this resource');
    }

    return await this.vacancyService.archive(vacancy.id);
  }

  @Put('/:id')
  @HttpCode(200)
  @UseGuards(CompanyGuard)
  @ApiBearerAuth('Bearer')
  @ApiOperation({ summary: '[C] Update a vacancy' })
  @ApiOkResponse({ description: 'The record has been successfully updated.', type: BaseVacancyEntity })
  @ApiBadRequestResponse({ description: 'Invalid form data' })
  @ApiUnauthorizedResponse({ description: 'Invalid JWT signature' })
  async updateVacancy(
    @Param('id') id: number,
    @Body() vacancyData: UpdateVacancyDto,
    @CurrentCompanyUser() user: CompanyUserFull,
  ) {
    const vacancy = await this.vacancyService.getMinimal(id);

    if (vacancy.companyId !== user.companyId) {
      throw new ForbiddenException('You have no permission to this resource');
    }

    return await this.vacancyService.update(vacancy.id, vacancyData);
  }

  @Get('/:id')
  @HttpCode(200)
  @UseGuards(AuthorizedOrAnyGuard)
  @ApiOperation({ summary: 'Get a vacancy' })
  @ApiOkResponse({ description: 'Vacancy', type: BaseVacancyEntity })
  @ApiNotFoundResponse({ description: 'Vacancy not found' })
  @ApiUnauthorizedResponse({ description: 'Invalid JWT signature' })
  async getVacancy(@Param('id') id: number, @CurrentUser() user: JwtValidEntity) {
    const vacancy = await this.vacancyService.get(id);

    if (user?.model === JwtAuthorizeEntity.INTERN) {
      const reacted = await this.internReactionsService.isReactionExists(user.user.id, vacancy.id);
      vacancy['reacted'] = reacted;
    }

    return vacancy;
  }

  @Get('/')
  @HttpCode(200)
  @ApiOperation({ summary: 'Get list of vacancies' })
  @ApiOkResponse({ description: 'Vacancy', type: VacanciesListResponse })
  @ApiUnauthorizedResponse({ description: 'Invalid JWT signature' })
  async getVacancies(@Query() params: VacanciesFilterDto) {
    return await this.vacancyService.getFiltered(params);
  }
}
