import { BadRequestException } from '@nestjs/common';
import { ApiProperty } from '@nestjs/swagger';
import {
  Ability,
  Company,
  Intern,
  InternReactions,
  Vacancy,
  VacancyLocationStatus,
  VacancyPaidStatus,
} from '@prisma/client';
import { Transform, Type } from 'class-transformer';
import {
  ArrayMaxSize,
  ArrayMinSize,
  IsArray,
  IsBoolean,
  IsDateString,
  IsEnum,
  IsNotEmpty,
  IsNumber,
  IsOptional,
  IsString,
  MaxLength,
} from 'class-validator';
import { CompanyMetaModel } from '../company/company.dto';
import { InternResponseDto } from '../intern/intern.dto';
import { ReactionModel } from '../intern-reactions/intern-reactions.dto';
import { AbilityModel } from '../specialization/specialization.dto';
import { InvitationModel } from '../vacancy-invitation/vacancy-invitation.dto';

export enum VacanciesSortBy {
  DATE = 'updatedAt',
  SALARY = 'salary',
  BEST_MATCH = 'bestMatch',
}

export enum VacanciesSortType {
  ASC = 'asc',
  DESC = 'desc',
}

export class BaseVacancyEntity implements Vacancy {
  @IsNumber()
  id: number;

  @IsDateString()
  createdAt: Date;

  @IsDateString()
  updatedAt: Date;

  @IsNumber()
  companyId: number;

  @IsNumber()
  salary: number;

  @IsBoolean()
  archived: boolean;

  @IsString()
  title: string;

  @IsString()
  description: string;

  @IsEnum(VacancyPaidStatus)
  @ApiProperty({ enum: VacancyPaidStatus })
  paid: VacancyPaidStatus;

  @IsEnum(VacancyLocationStatus)
  @ApiProperty({ enum: VacancyLocationStatus })
  location: VacancyLocationStatus;

  @IsArray()
  @Type(() => AbilityModel)
  @ApiProperty({ type: () => [AbilityModel] })
  abilities: Ability[];

  @Type(() => CompanyMetaModel)
  @ApiProperty({ type: () => CompanyMetaModel })
  company: CompanyMetaModel;
}

export class CreateVacancyDto {
  @IsString()
  @IsNotEmpty()
  @MaxLength(100)
  title: string;

  @IsString()
  @IsNotEmpty()
  @MaxLength(5000)
  description: string;

  @IsNotEmpty()
  @IsEnum(VacancyPaidStatus)
  @ApiProperty({ enum: VacancyPaidStatus })
  paid: VacancyPaidStatus;

  @IsNumber()
  salary: number;

  @IsNotEmpty()
  @IsEnum(VacancyLocationStatus)
  @ApiProperty({ enum: VacancyLocationStatus })
  location: VacancyLocationStatus;

  @IsArray()
  @ArrayMinSize(0)
  @ArrayMaxSize(2)
  abilities: number[];
}

export class UpdateVacancyDto {
  @IsString()
  @IsNotEmpty()
  @MaxLength(100)
  @IsOptional()
  title: string;

  @IsString()
  @IsNotEmpty()
  @MaxLength(5000)
  @IsOptional()
  description: string;

  @IsNotEmpty()
  @IsEnum(VacancyPaidStatus)
  @ApiProperty({ enum: VacancyPaidStatus })
  @IsOptional()
  paid: VacancyPaidStatus;

  @IsNumber()
  @IsOptional()
  salary: number;

  @IsNotEmpty()
  @IsEnum(VacancyLocationStatus)
  @ApiProperty({ enum: VacancyLocationStatus })
  @IsOptional()
  location: VacancyLocationStatus;

  @IsArray()
  @IsOptional()
  @ArrayMinSize(1)
  @ArrayMaxSize(2)
  abilities: number[];
}

export class VacanciesFilterDto {
  @IsString()
  @IsOptional()
  @ApiProperty({
    description: 'Text to include (it searches in title/description/shortDescription)',
    example: 'ReactJS',
  })
  search?: string;

  @IsEnum(VacanciesSortType)
  @IsOptional()
  @ApiProperty({ description: 'Sorting type (ascending/descending order)', example: 'asc' })
  sortType?: VacanciesSortType;

  @IsEnum(VacanciesSortBy)
  @IsOptional()
  @ApiProperty({ description: 'Sorting field', example: 'updatedAt' })
  sortBy?: VacanciesSortBy;

  @IsOptional()
  @IsArray()
  @ApiProperty({ description: 'Array of abilities id', example: ['0', '1'] })
  // TODO: Move to universal decorator
  @Transform(({ value }) =>
    value.map((v) => {
      if (Number.isNaN(Number(v))) throw new BadRequestException(['abilities must be a valid number array value']);
      return parseInt(v);
    }),
  )
  abilities?: number[];

  @IsNumber()
  @IsOptional()
  @Transform(({ value }) => parseInt(value))
  @ApiProperty({ description: 'Salary from value', example: '1000' })
  salaryFrom?: number;

  @IsNumber()
  @IsOptional()
  @Transform(({ value }) => parseInt(value))
  @ApiProperty({ description: 'Salary to value', example: '2000' })
  salaryTo?: number;

  @IsOptional()
  @ApiProperty({ enum: VacancyLocationStatus, description: 'Location status', example: VacancyLocationStatus.HYBRID })
  location?: VacancyLocationStatus;

  @IsOptional()
  @ApiProperty({ enum: VacancyPaidStatus, description: 'Paid status', example: VacancyPaidStatus.NOT_PAID })
  paid?: VacancyPaidStatus;
}

export class VacanciesListResponse {
  @IsArray()
  @Type(() => BaseVacancyEntity)
  vacancies: BaseVacancyEntity[];
}

export class VacancyReaction extends ReactionModel {
  intern: InternResponseDto;

  constructor(
    partial: Partial<
      InternReactions & {
        intern: Partial<Intern>;
      }
    >,
  ) {
    super();
    Object.assign(this, partial);
  }
}

export class VacancyInvitation extends InvitationModel {
  intern: InternResponseDto;

  constructor(
    partial: Partial<
      InternReactions & {
        intern: Partial<Intern>;
      }
    >,
  ) {
    super();
    Object.assign(this, partial);
  }
}
