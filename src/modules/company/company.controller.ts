import { CurrentCompanyUser } from '@/decorators/current-company-user.decorator';
import { CompanyOwnerGuard } from '@/guards/company-owner.guard';
import { CompanyUserFull } from '@/types';
import { imageFileFilter } from '@/utils/files';
import {
  Body,
  Controller,
  Delete,
  ForbiddenException,
  Get,
  HttpCode,
  Param,
  Post,
  Put,
  UploadedFile,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import {
  ApiBody,
  ApiConsumes,
  ApiCreatedResponse,
  ApiForbiddenResponse,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiOperation,
  ApiTags,
  ApiUnauthorizedResponse,
} from '@nestjs/swagger';
import { CurrentCompanyUserResponse, ListCompanyUsersResponse } from '../company-user/company-user.dto';
import { CompanyUserService } from '../company-user/company-user.service';
import { BaseVacancyEntity, VacanciesListResponse } from '../vacancy/vacancy.dto';
import { VacancyService } from '../vacancy/vacancy.service';
import { CompanyFullModel, CompanyModel, CompanyRegisterDto, CompanyUpdateDto } from './company.dto';
import { CompanyService } from './company.service';

@Controller('companies')
@ApiTags('Companies')
export class CompanyController {
  constructor(
    private readonly companyService: CompanyService,
    private readonly companyUserService: CompanyUserService,
    private readonly vacancyService: VacancyService,
  ) {}

  @Post('/signup')
  @ApiOperation({ summary: 'Register a company', description: 'Register a company' })
  @ApiCreatedResponse({ description: 'The record has been successfully created', type: CompanyModel })
  async registerCompany(@Body() companyData: CompanyRegisterDto) {
    return await this.companyService.registerCompany(companyData);
  }

  @Get('/:id/vacancies')
  @HttpCode(200)
  @ApiOperation({ summary: 'Get vacancies of a company', description: 'Get vacancies of a company' })
  @ApiNotFoundResponse({ description: 'Cannot find company with specified id' })
  @ApiOkResponse({ description: 'Response contains list of company vacancies', type: VacanciesListResponse })
  async getVacancies(@Param('id') id: number) {
    return await this.vacancyService.getByCompanyId(id);
  }

  @Get('/:id/users')
  @HttpCode(200)
  @ApiOperation({ summary: 'Get users of a company', description: 'Get users of a company' })
  @ApiNotFoundResponse({ description: 'Cannot find company with specified id' })
  @ApiOkResponse({ description: 'Response contains list of company users', type: ListCompanyUsersResponse })
  async getUsers(@Param('id') id: number) {
    return await this.companyUserService.getListByCompanyId(id);
  }

  @Get('/:id')
  @HttpCode(200)
  @ApiOperation({ summary: 'Get company info', description: 'Get company info' })
  @ApiOkResponse({ description: 'Company info returned', type: CompanyFullModel })
  async getCompany(@Param('id') id: number) {
    return await this.companyService.get(id);
  }

  @Post('/:id/logo')
  @UseGuards(CompanyOwnerGuard)
  @UseInterceptors(
    FileInterceptor('file', {
      fileFilter: imageFileFilter,
      limits: {
        fileSize: 5 * 1024 * 1024 * 8,
      },
    }),
  )
  @ApiOperation({ summary: '[CO] Update company avatar', description: 'Update company avatar' })
  @ApiOkResponse({ description: 'The record has been successfully created' })
  @ApiForbiddenResponse({ description: 'You have no permission to this resource' })
  @ApiUnauthorizedResponse({ description: 'Unauthorized' })
  @ApiConsumes('multipart/form-data')
  @ApiBody({
    schema: {
      type: 'object',
      properties: {
        file: {
          type: 'string',
          format: 'binary',
        },
      },
    },
  })
  async createLogo(
    @Param('id') id: number,
    @UploadedFile() file: Express.Multer.File,
    @CurrentCompanyUser() user: CompanyUserFull,
  ) {
    if (user.companyId !== id) {
      throw new ForbiddenException('You have no permission to this resource');
    }

    return await this.companyService.updateAvatar(id, file);
  }

  @Delete('/:id/logo')
  @HttpCode(200)
  @UseGuards(CompanyOwnerGuard)
  @ApiUnauthorizedResponse({ description: 'Unauthorized' })
  @ApiOkResponse({ description: 'The record has been successfully deleted' })
  @ApiForbiddenResponse({ description: 'You have no permission to this resource' })
  @ApiOperation({ summary: '[CO] Delete company logo', description: 'Delete company logo' })
  async deleteAvatar(@Param('id') id: number, @CurrentCompanyUser() user: CompanyUserFull) {
    if (user.companyId !== id) {
      throw new ForbiddenException('You have no permission to this resource');
    }
    await this.companyService.deleteAvatar(id);
    return { success: true };
  }

  @Put('/:id')
  @HttpCode(200)
  @UseGuards(CompanyOwnerGuard)
  @ApiUnauthorizedResponse({ description: 'Unauthorized' })
  @ApiOperation({ summary: '[CO] Update a company', description: 'Update a company' })
  @ApiOkResponse({ description: 'The record has been successfully updated', type: CompanyFullModel })
  @ApiForbiddenResponse({ description: 'You have no permission to this resource' })
  async update(
    @Param('id') id: number,
    @Body() companyData: CompanyUpdateDto,
    @CurrentCompanyUser() user: CompanyUserFull,
  ) {
    if (user.companyId !== id) {
      throw new ForbiddenException('You have no permission to this resource');
    }

    return await this.companyService.updateCompany(companyData, id);
  }
}
