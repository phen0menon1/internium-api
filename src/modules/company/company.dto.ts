import { Token } from '@/dto/token.dto';
import { ApiHideProperty, ApiProperty, OmitType } from '@nestjs/swagger';
import { Avatar, Company } from '@prisma/client';
import { Exclude } from 'class-transformer';
import { IsNotEmpty, IsString, IsEmail, MaxLength, IsNumber, MinLength, IsBoolean, IsOptional } from 'class-validator';

export class CompanyModel implements Company {
  @IsNumber()
  @ApiProperty({ description: 'Company id' })
  id: number;

  @IsNumber()
  logoId: number;

  @Exclude()
  @ApiHideProperty()
  verified: boolean;

  @IsString()
  @MaxLength(128)
  @IsNotEmpty()
  @ApiProperty({ description: 'Company name' })
  name: string;

  @IsString()
  @MaxLength(128)
  @IsNotEmpty()
  city: string;

  @IsString()
  @MaxLength(13)
  @IsNotEmpty()
  @ApiHideProperty()
  @Exclude()
  tin: string;

  @IsString()
  @MaxLength(100)
  website: string;

  @Exclude()
  @ApiHideProperty()
  hidden: boolean;

  @IsString()
  @MaxLength(3000)
  @ApiHideProperty()
  @Exclude()
  description: string;

  @IsString()
  @MaxLength(255)
  shortDescription: string;
}

export class CompanyFullModel extends OmitType(CompanyModel, ['description']) {
  @IsString()
  description: string;

  constructor(partial: Partial<CompanyFullModel>) {
    super(partial);
    Object.assign(this, partial);
  }
}

export class CompanyMetaModel implements Company {
  @IsNumber()
  @ApiProperty({ description: 'Company id' })
  id: number;

  @IsNumber()
  logoId: number;

  @ApiHideProperty()
  @Exclude()
  description: string;

  @Exclude()
  @ApiHideProperty()
  password: string;

  @Exclude()
  @ApiHideProperty()
  verified: boolean;

  @IsString()
  @ApiProperty({ description: 'Company name' })
  name: string;

  @Exclude()
  @ApiHideProperty()
  email: string;

  @IsString()
  city: string;

  @Exclude()
  @ApiHideProperty()
  phone: string;

  @ApiHideProperty()
  @Exclude()
  tin: string;

  @IsString()
  website: string;

  @IsString()
  shortDescription: string;

  @Exclude()
  @ApiHideProperty()
  hidden: boolean;

  constructor(partial: Partial<CompanyMetaModel>) {
    Object.assign(this, partial);
  }
}

export class CompanyRegisterDto {
  @IsString()
  @MaxLength(128)
  @IsNotEmpty()
  @ApiProperty({ description: 'Company name' })
  name: string;

  @IsEmail()
  @IsNotEmpty()
  @MaxLength(255)
  @ApiProperty({ description: 'Email' })
  email: string;

  @IsString()
  @MaxLength(128)
  @IsNotEmpty()
  @ApiProperty({ description: 'City' })
  city: string;

  @IsString()
  @MaxLength(13)
  @IsNotEmpty()
  @ApiProperty({ description: 'TIN (Russian INN)' })
  tin: string;
}

export class CompanyUpdateDto {
  @IsString()
  @IsOptional()
  @IsNotEmpty()
  @MaxLength(128)
  city?: string;

  @IsString()
  @IsOptional()
  @IsNotEmpty()
  @MaxLength(3000)
  description?: string;

  @IsString()
  @IsOptional()
  @IsNotEmpty()
  @MaxLength(100)
  shortDescription?: string;

  @IsBoolean()
  @IsOptional()
  @IsNotEmpty()
  hidden?: boolean;

  @IsString()
  @IsOptional()
  @IsNotEmpty()
  @MaxLength(100)
  website?: string;
}

export class CompanyJwtDto {
  companyId: number;
  expiration: number;
}

export class AuthCompanyDto {
  @IsEmail()
  @MaxLength(255)
  @MinLength(6)
  email: string;

  @IsString()
  @MaxLength(255)
  password: string;
}

export class AuthCompanyResponse {
  tokens: Token;
  data: CompanyModel;
}

export interface CompanyInsensitiveData extends Omit<Company, 'tin' | 'email' | 'logoId' | 'verified'> {
  logo: Avatar;
}

export interface CompanySerializedData extends Omit<Company, 'tin' | 'email' | 'logoId'> {
  logo?: string;
}
