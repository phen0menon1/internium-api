import { EntityManager } from '@/entity-manager';
import { imageFileEditName } from '@/utils/files';
import { forwardRef, Module } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { MulterModule } from '@nestjs/platform-express';
import { diskStorage } from 'multer';
import { CompanyUserModule } from '../company-user/company-user.module';
import { PasswordModule } from '../password/password.module';
import { PrismaModule } from '../prisma/prisma.module';
import { VacancyModule } from '../vacancy/vacancy.module';
import { CompanyController } from './company.controller';
import { CompanyService } from './company.service';

@Module({
  imports: [
    PrismaModule,
    PasswordModule,
    VacancyModule,
    forwardRef(() => CompanyUserModule),
    MulterModule.registerAsync({
      useFactory: async (config: ConfigService) => {
        return {
          storage: diskStorage({
            destination: config.get('UPLOAD_DEST'),
            filename: imageFileEditName,
          }),
        };
      },
      inject: [ConfigService],
    }),
  ],
  exports: [CompanyService],
  providers: [CompanyService, EntityManager],
  controllers: [CompanyController],
})
export class CompanyModule {}
