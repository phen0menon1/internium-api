import { Injectable } from '@nestjs/common';
import { PrismaService } from 'src/modules/prisma/prisma.service';
import { CompanyRegisterDto, CompanyUpdateDto } from './company.dto';
import { extend, pick } from 'lodash';
import { CompanyUserRole } from '@prisma/client';
import { PasswordService } from '../password/password.service';
import { EntityManager } from '@/entity-manager';

@Injectable()
export class CompanyService {
  constructor(
    private readonly prisma: PrismaService,
    private readonly entityManager: EntityManager,
    private readonly passwordService: PasswordService,
  ) {}

  async get(id: number) {
    const company = await this.prisma.company.findUnique({
      where: { id },
      select: this.entityManager.companyEntity,
    });
    return this.entityManager.serializeCompany(company);
  }

  async updateAvatar(id: number, file: Express.Multer.File) {
    const company = await this.prisma.company.update({
      where: { id },
      data: {
        logo: {
          upsert: {
            create: {
              filename: file.filename,
              filepath: file.path,
            },
            update: {
              filename: file.filename,
              filepath: file.path,
            },
          },
        },
      },
      include: {
        logo: true,
      },
    });

    return { filepath: company.logo.filepath };
  }

  async deleteAvatar(id: number) {
    return await this.prisma.company.update({
      where: { id },
      data: {
        logo: {
          delete: true,
        },
      },
    });
  }

  async updateCompany(data: CompanyUpdateDto, id: number) {
    const company = await this.prisma.company.update({
      where: {
        id,
      },
      data,
      select: this.entityManager.companyEntity,
    });
    return this.entityManager.serializeCompany(company);
  }

  async registerCompany(data: CompanyRegisterDto) {
    const email = data.email.toLowerCase();
    const password = await this.passwordService.hashPassword('example');
    const companyPayload = extend(pick(data, ['city', 'name', 'tin']), { verified: true });

    const company = await this.prisma.company.create({
      data: companyPayload,
      select: this.entityManager.companyEntity,
    });

    await this.prisma.companyUser.create({
      data: {
        email,
        password,
        companyId: company.id,
        role: CompanyUserRole.OWNER,
      },
    });

    return company;
  }
}
