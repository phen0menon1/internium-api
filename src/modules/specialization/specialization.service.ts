import { Injectable } from '@nestjs/common';
import { PrismaService } from '../prisma/prisma.service';

@Injectable()
export class SpecializationService {
  constructor(private readonly prisma: PrismaService) {}

  async getAll() {
    return await this.prisma.specialization.findMany();
  }

  async get(id: number) {
    return await this.prisma.specialization.findUnique({ where: { id }, include: { abilities: true } });
  }
}
