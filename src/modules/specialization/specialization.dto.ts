import { Ability, Specialization } from '@prisma/client';
import { IsArray, IsNumber, IsString } from 'class-validator';

export class AbilityModel implements Ability {
  @IsNumber()
  id: number;

  @IsString()
  title: string;

  @IsNumber()
  position: number;

  @IsNumber()
  specializationId: number;

  @IsString()
  engTranslationTitle: string;
}

export class SpecializationModel implements Specialization {
  @IsNumber()
  id: number;

  @IsString()
  title: string;

  @IsNumber()
  position: number;

  @IsString()
  engTranslationTitle: string;
}

export class GetSpecializationsResponse {
  @IsArray()
  specializations: SpecializationModel[];
}

export class GetSpecializationResponse {
  @IsArray()
  specialization: SpecializationModel;

  @IsArray()
  abilities: AbilityModel[];
}
