import { Module } from '@nestjs/common';
import { PrismaModule } from '../prisma/prisma.module';
import { SpecializationController } from './specialization.controller';
import { SpecializationService } from './specialization.service';

@Module({
  imports: [PrismaModule],
  providers: [SpecializationService],
  exports: [SpecializationService],
  controllers: [SpecializationController],
})
export class SpecializationModule {}
