import { AuthorizedGuard } from '@/guards/authorized.guard';
import { Controller, Get, HttpCode, Param, UseGuards } from '@nestjs/common';
import { ApiNotFoundResponse, ApiOkResponse, ApiOperation, ApiTags, ApiUnauthorizedResponse } from '@nestjs/swagger';
import { GetSpecializationResponse, GetSpecializationsResponse } from './specialization.dto';
import { SpecializationService } from './specialization.service';

@Controller('specializations')
@ApiTags('Specializations')
export class SpecializationController {
  constructor(private readonly specializationService: SpecializationService) {}

  @Get('/')
  @HttpCode(200)
  @ApiOperation({ summary: 'Get specializations' })
  @ApiOkResponse({ description: 'Specializations', type: GetSpecializationsResponse })
  async getSpecializations() {
    const specializations = await this.specializationService.getAll();
    return { specializations };
  }

  @Get('/:id')
  @HttpCode(200)
  @ApiOperation({ summary: 'Get a specialization' })
  @ApiOkResponse({ description: 'Specialization by id', type: GetSpecializationResponse })
  @ApiNotFoundResponse({ description: 'Specialization does not exist' })
  async getSpecialization(@Param('id') id: number) {
    return await this.specializationService.get(id);
  }
}
