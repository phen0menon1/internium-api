import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { PassportModule } from '@nestjs/passport';
import { PrismaModule } from '../prisma/prisma.module';
import { AdminModule as AdminJSModule } from '@adminjs/nestjs';
import { DMMFClass } from '@prisma/client/runtime';
import AdminJS from 'adminjs';
import { Database, Resource } from '@adminjs/prisma';
import { PrismaService } from '../prisma/prisma.service';
import Dashboard from './Dashboard';

AdminJS.registerAdapter({ Resource, Database });

@Module({
  imports: [
    PrismaModule,
    PassportModule,
    ConfigModule.forRoot({ isGlobal: true }),
    AdminJSModule.createAdminAsync({
      imports: [PrismaModule, ConfigModule],
      inject: [PrismaService, ConfigService],
      useFactory: (prisma: PrismaService, config: ConfigService) => {
        const dmmf = (prisma as any)._dmmf as DMMFClass;
        return {
          auth: {
            authenticate: async (email, password) => {
              const isValid = email === config.get('ADMIN_USERNAME') && password === config.get('ADMIN_PASSWORD');
              if (!isValid) return null;
              return Promise.resolve({ email });
            },
            cookieName: 'internium_admin_auth',
            cookiePassword: config.get('ADMIN_SECRET'),
          },
          adminJsOptions: {
            dashboard: {
              handler: async () => {},
              component: AdminJS.bundle('./Dashboard'),
            },
            branding: {
              companyName: 'Панель Интерниум',
              softwareBrothers: false,
              logo: '',
            },
            rootPath: '/',
            resources: [
              {
                resource: {
                  model: dmmf.modelMap.Specialization,
                  client: prisma,
                },
                options: {
                  listProperties: ['id', 'title', 'engTranslationTitle', 'position'],
                  navigation: null,
                },
              },
              {
                resource: {
                  model: dmmf.modelMap.Company,
                  client: prisma,
                },
                options: {
                  navigation: null,
                  listProperties: ['id', 'name', 'city', 'tin', 'hidden', 'verified'],
                  properties: {
                    description: {
                      type: 'richtext',
                    },
                  },
                },
              },
              {
                resource: {
                  model: dmmf.modelMap.CompanyUser,
                  client: prisma,
                },
                options: {
                  navigation: null,
                  properties: {
                    password: {
                      isVisible: false,
                    },
                  },
                  listProperties: ['id', 'email', 'company', 'role', 'userInfo'],
                },
              },
              {
                resource: {
                  model: dmmf.modelMap.Avatar,
                  client: prisma,
                },
                options: {
                  navigation: null,
                },
              },
              {
                resource: {
                  model: dmmf.modelMap.CompanyUserInfo,
                  client: prisma,
                },
                options: {
                  navigation: null,
                  listProperties: ['id', 'firstName', 'lastName', 'middleName', 'position', 'phone', 'birthdate'],
                  properties: {
                    description: {
                      type: 'richtext',
                    },
                  },
                },
              },
              {
                resource: {
                  model: dmmf.modelMap.Ability,
                  client: prisma,
                },
                options: {
                  navigation: null,
                  listProperties: ['id', 'title', 'engTranslationTitle', 'position'],
                },
              },
              {
                resource: {
                  model: dmmf.modelMap.InternReactions,
                  client: prisma,
                },
                options: {
                  navigation: null,
                },
              },
              {
                resource: {
                  model: dmmf.modelMap.VacancyInvitation,
                  client: prisma,
                },
                options: {
                  navigation: null,
                },
              },
              {
                resource: {
                  model: dmmf.modelMap.VacancyChatRoom,
                  client: prisma,
                },
                options: {
                  navigation: null,
                },
              },
              {
                resource: {
                  model: dmmf.modelMap.InternToVacancy,
                  client: prisma,
                },
                options: {
                  navigation: null,
                },
              },
              {
                resource: {
                  model: dmmf.modelMap.Intern,
                  client: prisma,
                },
                options: {
                  navigation: null,
                  properties: {
                    password: {
                      isVisible: false,
                    },
                    description: {
                      type: 'richtext',
                    },
                  },
                  listProperties: [
                    'id',
                    'firstName',
                    'lastName',
                    'middleName',
                    'email',
                    'birthdate',
                    'createdAt',
                    'updatedAt',
                    'avatar',
                  ],
                },
              },
              {
                resource: {
                  model: dmmf.modelMap.CompanyInvitation,
                  client: prisma,
                },
                options: {
                  navigation: null,
                },
              },
              {
                resource: {
                  model: dmmf.modelMap.Vacancy,
                  client: prisma,
                },
                options: {
                  navigation: null,
                  listProperties: [
                    'id',
                    'title',
                    'createdAt',
                    'updatedAt',
                    'company',
                    'archived',
                    'salary',
                    'paid',
                    'location',
                  ],
                  properties: {
                    description: {
                      type: 'richtext',
                    },
                  },
                },
              },
            ],
            locale: {
              language: 'ru',
              translations: {
                actions: {
                  new: 'Создать',
                  edit: 'Редактировать',
                  show: 'Просмотр',
                  search: 'Поиск',
                  delete: 'Удалить',
                  list: 'Список',
                  save: 'Сохранить',
                },
                buttons: {
                  filter: 'Фильтр',
                  save: 'Сохранить',
                  addNewItem: 'Добавить элемент',
                  applyChanges: 'Применить',
                  resetFilter: 'Сброс',
                  confirmRemovalMany: 'Подтвердите сброс {{count}} элемента',
                  confirmRemovalMany_plural: 'Подтвердите сброс {{count}} элементов',
                  logout: 'Выход',
                  login: 'Войти',
                  seeTheDocumentation: 'Справка: <1>документация</1>',
                  createFirstRecord: 'Создать первый элемент',
                },
                resources: {
                  Avatar: {
                    actions: {},
                    properties: {
                      id: '#',
                      filename: 'Название файла',
                      filepath: 'Путь до файла',
                    },
                  },
                  VacancyInvitation: {
                    properties: {
                      id: '#',
                      createdAt: 'Создан',
                      rejected: 'Отклонен',
                      accepted: 'Принят',
                      archived: 'Архивирован',
                      viewed: 'Просмотрен',
                      sender: 'Отправитель',
                      internToVacancy: 'Ссылка',
                    },
                  },
                  InternReactions: {
                    properties: {
                      id: '#',
                      createdAt: 'Создан',
                      rejected: 'Отклонен',
                      accepted: 'Принят',
                      viewed: 'Просмотрен',
                      archived: 'Архивирован',
                      internToVacancy: 'Ссылка',
                    },
                  },
                  Vacancy: {
                    actions: {
                      list: 'Список вакансий',
                    },
                    properties: {
                      id: '#',
                      title: 'Название',
                      createdAt: 'Создан',
                      updatedAt: 'Изменен',
                      company: 'Компания',
                      archived: 'Архивирован?',
                      description: 'Описание',
                      salary: 'Вознаграждение',
                      paid: 'Статус оплаты',
                      location: 'Статус локации',
                    },
                  },
                  Specialization: {
                    actions: {
                      list: 'Список специализаций',
                    },
                    properties: {
                      id: '#',
                      title: 'Название',
                      engTranslationTitle: 'Перевод (англ.)',
                      position: 'Место в списке',
                    },
                  },
                  Ability: {
                    actions: {
                      list: 'Список способностей',
                    },
                    properties: {
                      id: '#',
                      title: 'Название',
                      engTranslationTitle: 'Перевод (англ.)',
                      position: 'Место в списке',
                    },
                  },
                  Company: {
                    actions: {
                      list: 'Список компаний',
                    },
                    properties: {
                      id: '#',
                      city: 'Город',
                      tin: 'ИНН',
                      name: 'Название',
                      hidden: 'Скрыт?',
                      verified: 'Подтвержден?',
                      description: 'Описание',
                      shortDescription: 'Короткое описание',
                      website: 'Вебсайт',
                    },
                  },
                  CompanyUserInfo: {
                    actions: {
                      list: 'Список информаций о пользователях',
                    },
                    properties: {
                      id: '#',
                      firstName: 'Имя',
                      lastName: 'Фамилия',
                      middleName: 'Отчество',
                      position: 'Позиция',
                      phone: 'Телефон',
                      birthdate: 'Дата рождения',
                    },
                  },
                  CompanyUser: {
                    actions: {
                      list: 'Список пользователей компаний',
                    },
                    properties: {
                      id: '#',
                      email: 'Email',
                      company: 'ID компании',
                      role: 'Роль',
                      userInfo: 'ID информации',
                    },
                  },
                  Intern: {
                    actions: {
                      list: 'Список интернов',
                    },
                    properties: {
                      id: '#',
                      email: 'Email',
                      firstName: 'Имя',
                      lastName: 'Фамилия',
                      middleName: 'Отчество',
                      birthdate: 'Дата рождения',
                      createdAt: 'Создан',
                      updatedAt: 'Изменен',
                      gender: 'Пол',
                      location: 'Локация',
                      description: 'Описание',
                      phone: 'Телефон',
                      status: 'Статус',
                    },
                  },
                },
                properties: {
                  length: 'Длина',
                  from: 'От',
                  to: 'До',
                },
                labels: {
                  pages: 'Страницы',
                  selectedRecords: 'Выбрано ({{selected}})',
                  adminVersion: 'Версия: {{version}}',
                  appVersion: 'Версия: {{version}}',
                  password: 'Пароль',
                  loginWelcome: 'Панель управления Интерниум',
                  filters: 'Фильтры',
                  filter: 'Фильтр',
                  apply: 'Применить',
                  reset: 'Сброс',
                  dashboard: 'Панель управления',
                  navigation: 'Панель управления',
                  Specialization: 'Специализации',
                  Ability: 'Способности',
                  Company: 'Компании',
                  CompanyUser: 'Пользователи компаний',
                  CompanyUserInfo: 'Информация о пользователях',
                  CompanyInvitation: 'Приглашение в компанию',
                  Intern: 'Интерны',
                  Vacancy: 'Вакансии',
                  Avatar: 'Аватар',
                  InternReactions: 'Отклики интернов',
                  VacancyInvitation: 'Отклики компаний',
                  VacancyChatRoom: 'Комнаты чатов по вакансии',
                  InternToVacancy: 'Ссылки интерн-вакансия',
                },
                messages: {
                  loginWelcome: 'Панель управления, в которой можно все.',
                  confirmDelete: 'Вы уверены, что хотите удалить эту запись?',
                  invalidCredentials: 'Неверная почта или пароль',
                  noRecordsInResource: 'Пока пусто',
                  noRecords: 'Пока пусто',
                  successfullyCreated: 'Новая запись успешно создана',
                  successfullyBulkDeleted: 'Успешно удалена {{count}} запись',
                  successfullyBulkDeleted_plural: 'Успешно удалено {{count}} записей',
                  successfullyDeleted: 'Запись успешно удалена',
                  successfullyUpdated: 'Запись успешно обновлена',
                  thereWereValidationErrors: 'Валидационные ошибки - см. ниже',
                },
              },
            },
          },
        };
      },
    }),
  ],
  exports: [],
  providers: [],
  controllers: [],
})
export class AdminModule {}
