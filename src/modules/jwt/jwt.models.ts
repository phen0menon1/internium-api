export type AllowedJwtEntity = 'companyUser' | 'intern' | 'verification' | 'companyInvitation';

export interface JwtData {
  id: number;
  entity: AllowedJwtEntity;
}
