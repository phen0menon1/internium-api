import { Module } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { JwtModule as NestJwtModule } from '@nestjs/jwt';
import { PassportModule } from '@nestjs/passport';
import { SecurityConfig } from '@/configs/config.interface';
import { JwtService } from './jwt.service';
import { PrismaModule } from '../prisma/prisma.module';

@Module({
  imports: [
    PrismaModule,
    PassportModule.register({ defaultStrategy: 'jwt' }),
    NestJwtModule.registerAsync({
      useFactory: async (configService: ConfigService) => {
        const { expiresIn } = configService.get<SecurityConfig>('security');
        const secret = configService.get<string>('JWT_ACCESS_SECRET');

        return {
          secret,
          expiresIn,
        };
      },
      inject: [ConfigService],
    }),
  ],
  providers: [JwtService],
  exports: [JwtService],
})
export class JwtModule {}
