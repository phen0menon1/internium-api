import { SecurityConfig } from '@/configs/config.interface';
import { Global, Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { JwtService as NestJwtService, JwtSignOptions } from '@nestjs/jwt';
import { JwtData } from './jwt.models';

@Global()
@Injectable()
export class JwtService {
  securityConfig: SecurityConfig;
  jwtSettings: { accessSecret: string; refreshSecret: string };

  constructor(private readonly configService: ConfigService, private readonly nestJwtService: NestJwtService) {
    this.securityConfig = this.configService.get<SecurityConfig>('security');
    this.jwtSettings = {
      accessSecret: this.configService.get('JWT_ACCESS_SECRET'),
      refreshSecret: this.configService.get('JWT_REFRESH_SECRET'),
    };
  }

  verifyToken(token: string): JwtData {
    return this.nestJwtService.verify(token, {
      secret: this.jwtSettings.accessSecret,
    });
  }

  generateTokens(data: JwtData) {
    return {
      accessToken: this.generateAccessToken(data),
      refreshToken: this.generateRefreshToken(data),
    };
  }

  generateAccessToken(data: JwtData, options: JwtSignOptions = {}) {
    return this.nestJwtService.sign(data, {
      ...options,
      secret: this.jwtSettings.accessSecret,
    });
  }

  generateRefreshToken(data: JwtData, options: JwtSignOptions = {}) {
    return this.nestJwtService.sign(data, {
      ...options,
      secret: this.jwtSettings.refreshSecret,
      expiresIn: this.securityConfig.refreshIn,
    });
  }
}
