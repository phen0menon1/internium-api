import { EntityManager } from '@/entity-manager';
import { Module } from '@nestjs/common';
import { ChatModule } from '../chat/chat.module';
import { CompanyInvitationModule } from '../company-invitation/company-invitation.module';
import { JwtModule } from '../jwt/jwt.module';
import { PasswordModule } from '../password/password.module';
import { PrismaModule } from '../prisma/prisma.module';
import { CompanyUserController } from './company-user.controller';
import { CompanyUserService } from './company-user.service';

@Module({
  imports: [PrismaModule, CompanyInvitationModule, PasswordModule, JwtModule, ChatModule],
  providers: [CompanyUserService, EntityManager],
  exports: [CompanyUserService],
  controllers: [CompanyUserController],
})
export class CompanyUserModule {}
