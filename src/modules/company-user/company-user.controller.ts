import { CurrentCompanyUser } from '@/decorators/current-company-user.decorator';
import { Token } from '@/dto/token.dto';
import { CompanyGuard } from '@/guards/company.guard';
import { CompanyOwnerGuard } from '@/guards/company-owner.guard';
import { CompanyUserFull } from '@/types';
import {
  Body,
  Controller,
  Delete,
  ForbiddenException,
  Get,
  Headers,
  HttpCode,
  Param,
  Post,
  Put,
  UseGuards,
} from '@nestjs/common';
import { ApiOperation, ApiResponse, ApiTags, ApiUnauthorizedResponse } from '@nestjs/swagger';
import {
  AuthorizeCompanyUserDto,
  CreateCompanyUserDto,
  CurrentCompanyUserResponse,
  GetCompanyUserResponse,
  UpdateCompanyUserInfoDto,
} from './company-user.dto';
import { CompanyUserService } from './company-user.service';
import { CompanyUserGuard } from '@/guards/company-user.guard';
import { ChatService } from '../chat/chat.service';

@Controller('company-users')
@ApiTags('Company Users')
export class CompanyUserController {
  constructor(private readonly companyUserService: CompanyUserService, private readonly chatService: ChatService) {}

  @Post('/')
  @ApiOperation({
    summary: 'Create a user by providing an invitation token',
    description:
      'Creates a user by invitation token. A token should be pre-validated at the <a href="https://internium.monkeyhackers.org/api/swagger/#/CompanyInvitations/CompanyInvitationController_verify" target="_blank">/companyInvitations/verify</a> step.',
  })
  @ApiResponse({ status: 201, description: 'The record has been successfully created', type: Token })
  @ApiUnauthorizedResponse({ description: 'Unauthorized' })
  async create(@Body() userData: CreateCompanyUserDto, @Headers('Authorization') token: string) {
    return await this.companyUserService.createUser(userData, token);
  }

  @Delete('/:id')
  @ApiOperation({
    summary: '[CO] Delete company user',
    description: 'Deletes company user. Endpoint is allowed only for company owners',
  })
  @UseGuards(CompanyOwnerGuard)
  @ApiResponse({ status: 200, description: 'The record has been successfully deleted' })
  @ApiUnauthorizedResponse({ description: 'Unauthorized' })
  async delete(@Param('id') id: number, @CurrentCompanyUser() user: CompanyUserFull) {
    const companyUser = await this.companyUserService.get(id, { shouldInclude: true });
    if (companyUser?.company?.id !== user.companyId) {
      throw new ForbiddenException('You have no permission to this resource');
    }
    await this.companyUserService.delete(id);
    return { success: true };
  }

  @Get('/current')
  @UseGuards(CompanyGuard)
  @ApiOperation({ summary: '[C] Get current user info', description: 'Get current user information (JSON)' })
  @ApiResponse({ status: 200, description: 'Current user object', type: CurrentCompanyUserResponse })
  @ApiUnauthorizedResponse({ description: 'Unauthorized' })
  async getCurrentUser(@CurrentCompanyUser() userData: CompanyUserFull) {
    return userData;
  }

  @Get('/current/chats')
  @UseGuards(CompanyGuard)
  @ApiOperation({ summary: '[C] Get current user chats', description: 'Get current user chats' })
  @ApiResponse({ status: 200, description: 'Return current user chats' })
  @ApiUnauthorizedResponse({ description: 'Unauthorized' })
  async getCurrentUserChats(@CurrentCompanyUser() userData: CompanyUserFull) {
    return await this.chatService.getCompanyChatrooms(userData.companyId);
  }

  @Post('/authorize')
  @HttpCode(200)
  @ApiOperation({ summary: 'Authorize company user', description: 'Authorize a user (company user or owner)' })
  @ApiResponse({ status: 200, description: 'Successful authorization', type: Token })
  async authorize(@Body() userData: AuthorizeCompanyUserDto) {
    return await this.companyUserService.authorizeUser(userData);
  }

  @Get('/:id')
  @ApiOperation({ summary: 'Get company user info', description: 'Get user information' })
  @ApiResponse({ status: 200, description: 'Company user object', type: GetCompanyUserResponse })
  async getUser(@Param('id') id: number) {
    return await this.companyUserService.get(id, { shouldInclude: true });
  }

  @Put('/:id')
  @UseGuards(CompanyUserGuard)
  @ApiOperation({ summary: '[C] Update company user info', description: 'Update company user information' })
  @ApiResponse({ status: 200, description: 'Company user object', type: GetCompanyUserResponse })
  @ApiUnauthorizedResponse({ description: 'Unauthorized' })
  async update(
    @Param('id') id: number,
    @Body() data: UpdateCompanyUserInfoDto,
    @CurrentCompanyUser() companyUser: CompanyUserFull,
  ) {
    const user = await this.companyUserService.get(id);

    if (user.id !== companyUser.id) {
      throw new ForbiddenException('You have no permisson to this resource');
    }

    return await this.companyUserService.update(id, data);
  }
}
