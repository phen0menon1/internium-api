import { ApiHideProperty, ApiProperty, OmitType, PartialType } from '@nestjs/swagger';
import { Avatar, Company, CompanyUser, CompanyUserInfo, CompanyUserRole } from '@prisma/client';
import { Exclude, Type } from 'class-transformer';
import {
  IsNotEmpty,
  IsString,
  IsEmail,
  MaxLength,
  IsNumber,
  Matches,
  IsEnum,
  IsISO8601,
  IsArray,
} from 'class-validator';
import { CompanyModel } from '../company/company.dto';

export type CompanyUserInfoModelSerializationParam = Partial<Omit<CompanyUserInfoModel, 'avatar'>> & {
  avatar: Avatar;
};

export class CompanyUserInfoModel implements CompanyUserInfo {
  @IsNumber()
  @ApiProperty({ description: 'Company user info id' })
  id: number;

  @IsString()
  @MaxLength(48)
  @ApiProperty({ description: 'User last name' })
  lastName: string;

  @IsString()
  @MaxLength(48)
  @ApiProperty({ description: 'User first name' })
  firstName: string;

  @IsString()
  avatar: string;

  @IsNumber()
  @Exclude()
  avatarId: number;

  @IsString()
  @MaxLength(48)
  @ApiProperty({ description: 'User middle name' })
  middleName: string;

  @IsString()
  @MaxLength(100)
  @ApiProperty({ description: 'User position' })
  position: string;

  @IsString()
  @MaxLength(20)
  @ApiProperty({ description: 'User phone' })
  phone: string;

  @IsISO8601()
  @ApiProperty({ description: 'Birthdate' })
  birthdate: string;

  constructor(partial: CompanyUserInfoModelSerializationParam) {
    Object.assign(this, partial);
    this.avatar = partial.avatar?.filepath ?? null;
  }
}

export class CompanyUserInfoModelPublic extends OmitType(CompanyUserInfoModel, [
  'id',
  'phone',
  'birthdate',
  'avatar',
]) {}

export class CompanyUserModel {
  @IsNumber()
  @ApiProperty({ description: 'Company user id' })
  id: number;

  @IsEmail()
  @IsNotEmpty()
  @MaxLength(255)
  @ApiProperty({ description: 'Email' })
  email: string;

  @IsEnum(CompanyUserRole)
  @IsNotEmpty()
  @ApiProperty({ enum: CompanyUserRole })
  role: CompanyUserRole;

  @IsNumber()
  companyId: number;

  @IsNumber()
  userInfoId: number;
}

export class CurrentUserCompanyResponse extends OmitType(CompanyModel, ['tin']) {
  @IsString()
  description: string;

  @IsString()
  shortDescription: string;
}

export class CurrentCompanyUserResponse extends CompanyUserModel {
  @Type(() => CurrentUserCompanyResponse)
  company?: CurrentUserCompanyResponse;

  @Type(() => CompanyUserInfoModel)
  userInfo?: CompanyUserInfoModel;
}

export class ListCompanyUsersItem extends OmitType(CurrentCompanyUserResponse, [
  'email',
  'role',
  'companyId',
  'userInfoId',
  'company',
]) {}

export class ListCompanyUsersResponse {
  @IsArray()
  @Type(() => ListCompanyUsersItem)
  @ApiProperty({ type: [ListCompanyUsersItem] })
  users: ListCompanyUsersItem[];
}

export class UpdateCompanyUserInfoDto extends PartialType(OmitType(CompanyUserInfoModel, ['id', 'avatar'])) {}

export class GetCompanyUserResponse extends OmitType(CurrentCompanyUserResponse, [
  'role',
  'email',
  'companyId',
  'userInfoId',
]) {}

export class CreateCompanyUserDto {
  @IsString()
  @MaxLength(128)
  @IsNotEmpty()
  password: string;

  @IsString()
  @MaxLength(48)
  @IsNotEmpty()
  firstName: string;

  @IsString()
  @MaxLength(48)
  @IsNotEmpty()
  lastName: string;

  @IsString()
  @MaxLength(48)
  middleName?: string;

  @IsString()
  @MaxLength(100)
  @IsNotEmpty()
  position: string;

  @Matches(/^\d{4}(-)(((0)[0-9])|((1)[0-2]))(-)([0-2][0-9]|(3)[0-1])$/i, {
    message: '$property must be formatted as YYYY-MM-DD',
  })
  @MaxLength(10)
  @IsNotEmpty()
  birthdate: string;

  @MaxLength(20)
  @IsNotEmpty()
  phone: string;
}

export class AuthorizeCompanyUserDto {
  @IsEmail()
  @IsNotEmpty()
  @ApiProperty({ description: 'Email' })
  email: string;

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ description: 'Password' })
  password: string;
}
