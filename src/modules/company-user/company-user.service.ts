import { Token } from '@/dto/token.dto';
import { EntityManager } from '@/entity-manager';
import { BadRequestException, Injectable, NotFoundException } from '@nestjs/common';
import { CompanyUserRole } from '@prisma/client';
import { omit } from 'lodash';

import { PrismaService } from 'src/modules/prisma/prisma.service';
import { CompanyInvitationService } from '../company-invitation/company-invitation.service';
import { JwtService } from '../jwt/jwt.service';
import { PasswordService } from '../password/password.service';
import { AuthorizeCompanyUserDto, CreateCompanyUserDto, UpdateCompanyUserInfoDto } from './company-user.dto';

@Injectable()
export class CompanyUserService {
  constructor(
    private readonly prisma: PrismaService,
    private readonly jwtService: JwtService,
    private readonly entityManager: EntityManager,
    private readonly passwordService: PasswordService,
    private readonly companyInvitationService: CompanyInvitationService,
  ) {}

  async getFull(id: number, options: { shouldInclude: boolean }) {
    const companyUser = await this.prisma.companyUser.findUnique({
      where: { id },
      select: this.entityManager.companyUserEntity,
    });

    return this.entityManager.serializeCompanyUser(companyUser);
  }

  async getListByCompanyId(companyId: number) {
    const users = await this.prisma.companyUser.findMany({
      where: { companyId, role: CompanyUserRole.USER },
      select: this.entityManager.companyUserEntity,
    });

    return {
      users: users.map((u) => this.entityManager.serializeCompanyUser(u)),
    };
  }

  async update(id: number, data: UpdateCompanyUserInfoDto) {
    const user = await this.prisma.companyUser.update({
      where: { id },
      data: {
        userInfo: {
          update: data,
        },
      },
      select: this.entityManager.companyUserEntity,
    });
    return this.entityManager.serializeCompanyUser(user);
  }

  async delete(id: number) {
    return await this.prisma.companyUser.delete({
      where: { id },
    });
  }

  async get(id: number, options: { shouldInclude: boolean } = { shouldInclude: false }) {
    const entity = await this.prisma.companyUser.findUnique({
      where: { id },
      select: this.entityManager.companyUserEntity,
    });
    return this.entityManager.serializeCompanyUser(entity);
  }

  async deleteAvatar(userId: number) {
    return await this.prisma.companyUser.update({
      where: { id: userId },
      data: {
        userInfo: {
          update: {
            avatar: {
              delete: true,
            },
          },
        },
      },
      select: {
        id: true,
      },
    });
  }

  async updateAvatar(userId: number, file: Express.Multer.File) {
    const user = await this.prisma.companyUser.update({
      where: { id: userId },
      data: {
        userInfo: {
          update: {
            avatar: {
              upsert: {
                create: {
                  filepath: file.path,
                  filename: file.filename,
                },
                update: {
                  filepath: file.path,
                  filename: file.filename,
                },
              },
            },
          },
        },
      },
      select: this.entityManager.companyUserEntity,
    });
    return this.entityManager.serializeCompanyUser(user);
  }

  async createUser(userData: CreateCompanyUserDto, token: string): Promise<Token> {
    const { id: invitationId } = this.jwtService.verifyToken(token);
    const invitation = await this.companyInvitationService.get(invitationId);
    if (!invitation) throw new NotFoundException('Invalid invitation code provided');
    const password = await this.passwordService.hashPassword(userData.password);
    const user = await this.prisma.companyUser.create({
      data: {
        password,
        email: invitation.email,
        company: { connect: { id: invitation.companyId } },
        userInfo: { create: omit(userData, 'password') },
      },
    });
    await this.companyInvitationService.delete(invitationId);
    const tokens = this.jwtService.generateTokens({ id: user.id, entity: 'companyUser' });
    return tokens;
  }

  async authorizeUser(userData: AuthorizeCompanyUserDto): Promise<Token> {
    const companyUser = await this.prisma.companyUser.findUnique({ where: { email: userData.email } });
    if (!companyUser) throw new BadRequestException('User with specified email does not exist');
    const isPasswordValid = await this.passwordService.validatePassword(userData.password, companyUser.password);
    if (!isPasswordValid) throw new BadRequestException('Invalid credentials provided');
    const tokens = this.jwtService.generateTokens({ id: companyUser.id, entity: 'companyUser' });
    return tokens;
  }
}
