import { Module } from '@nestjs/common';
import { KafkaProducerService } from './kafka-producer.provider';

@Module({
  imports: [],
  controllers: [],
  providers: [KafkaProducerService],
  exports: [KafkaProducerService],
})
export class KafkaModule {}
