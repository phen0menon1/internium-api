import { EntityManager } from '@/entity-manager';
import { Module } from '@nestjs/common';
import { ChatModule } from '../chat/chat.module';
import { InternReactionsModule } from '../intern-reactions/intern-reactions.module';
import { JwtModule } from '../jwt/jwt.module';
import { PrismaModule } from '../prisma/prisma.module';
import { VacancyInvitationModule } from '../vacancy-invitation/vacancy-invitation.module';
import { InternController } from './intern.controller';
import { InternService } from './intern.service';
import { SmsService } from './providers/sms.service';

@Module({
  imports: [PrismaModule, JwtModule, InternReactionsModule, VacancyInvitationModule, ChatModule],
  providers: [InternService, SmsService, EntityManager],
  exports: [InternService, SmsService],
  controllers: [InternController],
})
export class InternModule {}
