import { Injectable } from '@nestjs/common';
import { Ability, Avatar, Intern, InternAbility, Prisma } from '@prisma/client';
import { Token } from '@/dto/token.dto';
import { PrismaService } from '@/modules/prisma/prisma.service';
import { JwtService } from '../jwt/jwt.service';
import {
  InternRegisterDto,
  InternVerifyDto,
  InternAuthDto,
  InternAuthResponseDto,
  InternNotVerifiedAuthResponseDto,
  InternFilterDto,
  InternUpdateDto,
} from './intern.dto';
import { SmsService } from './providers/sms.service';
import { PrismaNotFoundException } from '@/exception/prisma-not-found.exception';
import { extend, omit } from 'lodash';
import { InternReactionsService } from '../intern-reactions/intern-reactions.service';
import { VacancyInvitationService } from '../vacancy-invitation/vacancy-invitation.service';
import { EntityManager } from '@/entity-manager';

@Injectable()
export class InternService {
  constructor(
    private readonly prisma: PrismaService,
    private readonly jwtService: JwtService,
    private readonly smsService: SmsService,
    private readonly entityManager: EntityManager,
    private readonly internReactionsService: InternReactionsService,
    private readonly vacancyInvitationService: VacancyInvitationService,
  ) {}

  async get(id: number) {
    const intern = await this.prisma.intern.findUnique({
      where: { id },
      include: this.entityManager.internEntity,
    });
    return this.entityManager.serializeIntern(intern);
  }

  async getWithRelations(id: number) {
    const intern = await this.prisma.intern.findUnique({ where: { id }, include: this.entityManager.internEntity });
    return this.entityManager.serializeIntern(intern);
  }

  async exists(id: number) {
    const intern = await this.prisma.intern.findUnique({
      where: { id },
      select: {
        id: true,
      },
    });
    return Boolean(intern);
  }

  async update(id: number, data: InternUpdateDto) {
    const shouldRemoveAbilities = data.abilities?.length > 0;

    const transactionItems = [];

    if (shouldRemoveAbilities) {
      transactionItems.push(
        this.prisma.intern.update({
          where: { id },
          data: {
            abilities: {
              deleteMany: {},
            },
          },
        }),
      );
    }

    transactionItems.push(
      this.prisma.intern.update({
        where: { id },
        data: extend(omit(data, 'abilities'), {
          abilities: {
            create: data.abilities?.map((id) => ({
              abilityId: id,
            })),
          },
        }),
        include: {
          abilities: { include: { ability: true } },
          avatar: true,
        },
      }),
    );

    const transactionResult = await this.prisma.$transaction(transactionItems);

    const intern = transactionResult.pop() as Intern & {
      abilities: (InternAbility & {
        ability: Ability;
      })[];
      avatar: Avatar;
    };

    return this.entityManager.serializeIntern(intern);
  }

  async getFiltered(filters: InternFilterDto) {
    const filter = {} as Prisma.InternWhereInput;

    if (filters.abilities?.length) {
      filter['abilities'] = { some: { OR: filters.abilities?.map((id) => ({ abilityId: id })) } };
    }

    if (filters.location) {
      filter['location'] = {
        contains: filters.location,
        mode: 'insensitive',
      };
    }

    if (filters.search) {
      filter['OR'] = ['description', 'firstName', 'lastName'].map((field) => ({
        [field]: { contains: filters.search, mode: 'insensitive' },
      }));

      const parts = filters.search.split(' ');

      if (parts.length) {
        filter['OR'].push({
          AND: {
            firstName: { contains: parts[0], mode: 'insensitive' },
            lastName: { contains: parts[1], mode: 'insensitive' },
          },
        });
      }
    }

    const interns = await this.prisma.intern.findMany({
      where: filter,
      include: this.entityManager.internEntity,
    });

    return {
      interns: interns.map((i) => this.entityManager.serializeIntern(i)),
    };
  }

  async createIntern(internData: InternRegisterDto, registerToken: string) {
    const verificationId = await this.smsService.validateVerificationJwt(registerToken);
    const verificationMessage = await this.smsService.getVerificationMsgById(verificationId);
    const intern = await this.prisma.intern.create({
      data: { ...internData, phone: verificationMessage.phone },
    });
    const token = this.jwtService.generateTokens({ id: intern.id, entity: 'intern' });
    return { intern, token };
  }

  async getInternByPhone(phone: string): Promise<Intern> {
    return await this.prisma.intern.findUnique({ where: { phone } });
  }

  async getInternReactions(id: number) {
    return await this.internReactionsService.getListByInternId(id);
  }

  async getInternInvitations(id: number) {
    return await this.vacancyInvitationService.getListByInternId(id);
  }

  async verifyIntern(internData: InternVerifyDto): Promise<Token | InternNotVerifiedAuthResponseDto> {
    let verificationId = null;
    try {
      verificationId = await this.smsService.validateVerificationCode(internData.phone, internData.code);
      const intern = await this.getInternByPhone(internData.phone);
      const token = this.jwtService.generateTokens({ id: intern.id, entity: 'intern' });
      return token;
    } catch (e) {
      if (e instanceof PrismaNotFoundException && verificationId) {
        return { registerToken: await this.smsService.generateVerificationJwt(verificationId) };
      }
      throw e;
    }
  }

  async authIntern(internData: InternAuthDto): Promise<InternAuthResponseDto> {
    return await this.smsService.createVerificationCode(internData.phone);
  }

  async deleteAvatar(internId: number) {
    return await this.prisma.intern.update({
      where: { id: internId },
      data: {
        avatar: {
          delete: true,
        },
      },
    });
  }

  async updateAvatar(internId: number, file: Express.Multer.File) {
    return await this.prisma.intern.update({
      where: { id: internId },
      data: {
        avatar: {
          create: {
            filepath: file.path,
            filename: file.filename,
          },
        },
      },
    });
  }
}
