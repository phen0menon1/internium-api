import { Token } from '@/dto/token.dto';
import { BadRequestException } from '@nestjs/common';
import { ApiProperty } from '@nestjs/swagger';
import { Ability, Avatar, Intern, InternReactions, InternStatus } from '@prisma/client';
import { Exclude, Transform, Type } from 'class-transformer';
import {
  IsNotEmpty,
  IsString,
  IsEmail,
  MaxLength,
  Matches,
  IsBoolean,
  IsNumber,
  IsEnum,
  IsDate,
  IsOptional,
  IsArray,
  ArrayMinSize,
  ArrayMaxSize,
} from 'class-validator';
import { ReactionModel } from '../intern-reactions/intern-reactions.dto';
import { AbilityModel } from '../specialization/specialization.dto';

export class AvatarModel implements Avatar {
  @IsNumber()
  id: number;

  @IsString()
  filename: string;

  @IsString()
  filepath: string;
}

export class InternResponseDto {
  @IsNumber()
  id: number;

  @IsEmail()
  email: string;

  @IsString()
  firstName: string;

  @IsString()
  middleName: string;

  @IsString()
  lastName: string;

  @IsString()
  birthdate: string;

  @IsDate()
  createdAt: Date;

  @IsDate()
  updatedAt: Date;

  @IsBoolean()
  gender: boolean;

  @IsString()
  location: string | null;

  @IsString()
  description: string | null;

  @IsArray()
  @Type(() => AbilityModel)
  @ApiProperty({ type: () => [AbilityModel] })
  abilities: Ability[];

  @IsString()
  avatar: string;

  @IsString()
  phone: string;

  @IsEnum(InternStatus)
  @IsNotEmpty()
  @ApiProperty({ enum: InternStatus })
  status: InternStatus;
}

export class AuthenticateDto {
  @IsString()
  @IsNotEmpty()
  phone: string;
}

export class InternAuthDto {
  @IsString()
  @IsNotEmpty()
  phone: string;
}

export class InternRegisterDto {
  @IsString()
  @IsNotEmpty()
  @MaxLength(100)
  firstName: string;

  @IsString()
  @IsNotEmpty()
  @MaxLength(100)
  lastName: string;

  @IsString()
  @MaxLength(100)
  middleName: string;

  @IsEmail()
  @IsNotEmpty()
  @MaxLength(255)
  email: string;

  @Matches(/^\d{4}(-)(((0)[0-9])|((1)[0-2]))(-)([0-2][0-9]|(3)[0-1])$/i, {
    message: '$property must be formatted as YYYY-MM-DD',
  })
  @MaxLength(10)
  @IsNotEmpty()
  birthdate: string;

  @IsBoolean()
  @IsNotEmpty()
  gender: boolean;
}

export class InternUpdateDto {
  @IsString()
  @IsOptional()
  @MaxLength(100)
  firstName: string;

  @IsString()
  @IsOptional()
  @MaxLength(100)
  lastName: string;

  @IsString()
  @IsOptional()
  @MaxLength(100)
  middleName: string;

  @IsString()
  @IsOptional()
  @MaxLength(100)
  @ApiProperty({ example: 'Moscow' })
  location: string;

  @IsString()
  @IsOptional()
  @MaxLength(8192)
  @ApiProperty({ example: '<ul><li>item</li></ul>' })
  description: string;

  @IsOptional()
  @IsEnum(InternStatus)
  @ApiProperty({ enum: InternStatus, example: 'ACTIVE' })
  status: InternStatus;

  @Matches(/^\d{4}(-)(((0)[0-9])|((1)[0-2]))(-)([0-2][0-9]|(3)[0-1])$/i, {
    message: '$property must be formatted as YYYY-MM-DD',
  })
  @MaxLength(10)
  @IsOptional()
  @ApiProperty({ example: '1994-02-23' })
  birthdate: string;

  @IsBoolean()
  @IsOptional()
  @ApiProperty({ example: true })
  gender: boolean;

  @IsArray()
  @IsOptional()
  @ArrayMinSize(0)
  @ArrayMaxSize(2)
  @ApiProperty({ example: [0, 1] })
  abilities: number[];
}

export class InternFilterDto {
  @IsString()
  @IsOptional()
  @ApiProperty({
    description: 'Text to include (it searches in title/description/shortDescription)',
    example: 'ReactJS',
  })
  search?: string;

  // @IsEnum(VacanciesSortType)
  // @IsOptional()
  // @ApiProperty({ description: 'Sorting type (ascending/descending order)', example: 'asc' })
  // sortType?: VacanciesSortType;

  // @IsEnum(VacanciesSortBy)
  // @IsOptional()
  // @ApiProperty({ description: 'Sorting field', example: 'updatedAt' })
  // sortBy?: VacanciesSortBy;

  @IsOptional()
  @IsArray()
  @ApiProperty({ description: 'Array of abilities id', example: ['0', '1'] })
  // TODO: Move to universal decorator
  @Transform(({ value }) =>
    value.map((v) => {
      if (Number.isNaN(Number(v))) throw new BadRequestException(['abilities must be a valid number array value']);
      return parseInt(v);
    }),
  )
  abilities?: number[];

  @IsString()
  @IsOptional()
  @ApiProperty({ description: 'Location of an intern', example: 'Tver' })
  location?: string;
}

export class InternVerifyDto {
  @IsString()
  @IsNotEmpty()
  @MaxLength(6)
  code: string;

  @IsString()
  @IsNotEmpty()
  phone: string;
}

export class InternJwtDto {
  internId: number;
  expiration: number;
}

export class VerifiedInternDto {
  token: Token;
}

export class InternAuthResponseDto {
  code: string;
  blockTime: number;
}

export class InternNotVerifiedAuthResponseDto {
  registerToken: string;
}

export class InternFilterResponseDto {
  @IsArray()
  @Type(() => InternResponseDto)
  @ApiProperty({ type: () => [InternResponseDto] })
  interns: InternResponseDto[];
}

export class CurrentInternReactionsResponseDto {
  @IsArray()
  @Type(() => ReactionModel)
  reactions: ReactionModel[];
}
