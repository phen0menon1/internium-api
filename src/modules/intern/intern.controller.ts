import { CurrentInternUser } from '@/decorators/current-intern-user';
import { Token } from '@/dto/token.dto';
import { CompanyGuard } from '@/guards/company.guard';
import { InternGuard } from '@/guards/intern.guard';
import { FullInternUser } from '@/types';
import {
  Body,
  Controller,
  ForbiddenException,
  Get,
  Headers,
  HttpCode,
  Param,
  Post,
  Put,
  Query,
  UnauthorizedException,
  UseGuards,
} from '@nestjs/common';
import {
  ApiBadRequestResponse,
  ApiBearerAuth,
  ApiCreatedResponse,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiOperation,
  ApiTags,
  ApiUnauthorizedResponse,
} from '@nestjs/swagger';
import { ChatService } from '../chat/chat.service';
import {
  InternRegisterDto,
  InternAuthDto,
  InternVerifyDto,
  InternAuthResponseDto,
  InternNotVerifiedAuthResponseDto,
  InternFilterDto,
  InternFilterResponseDto,
  InternUpdateDto,
  CurrentInternReactionsResponseDto,
  InternResponseDto,
} from './intern.dto';
import { InternService } from './intern.service';

@Controller('interns')
@ApiTags('Interns')
export class InternController {
  constructor(private readonly internService: InternService, private readonly chatService: ChatService) {}

  @Get('/')
  @UseGuards(CompanyGuard)
  @ApiBearerAuth('Bearer')
  @ApiUnauthorizedResponse({ description: 'Unauthorized' })
  @ApiOperation({ summary: '[C] Get list of interns', description: 'Get list of interns' })
  @ApiOkResponse({ description: 'List of interns', type: InternFilterResponseDto })
  async list(@Query() params: InternFilterDto) {
    return await this.internService.getFiltered(params);
  }

  @Post('/signup')
  @ApiOperation({
    summary: 'Register an intern',
    description: 'Creates a user by invitation token. A token should be pre-validated by the appropriate endpoint',
  })
  @ApiCreatedResponse({ description: 'The record has been successfully created.', type: Token })
  @ApiBadRequestResponse({ description: 'Invalid form data' })
  @ApiUnauthorizedResponse({ description: 'Invalid JWT signature' })
  async registerIntern(@Body() internData: InternRegisterDto, @Headers('Authorization') token: string) {
    return await this.internService.createIntern(internData, token);
  }

  @Post('/auth')
  @ApiOperation({ summary: 'Login an intern', description: 'Authorize an intern' })
  @ApiCreatedResponse({ description: 'Sent SMS code', type: InternAuthResponseDto })
  async loginIntern(@Body() internData: InternAuthDto) {
    return await this.internService.authIntern(internData);
  }

  @Post('/verify')
  @HttpCode(200)
  @ApiOperation({ summary: 'Verify intern via OTP', description: "Verifies intern's phone number" })
  @ApiOkResponse({ description: 'Ok!', type: Token })
  @ApiBadRequestResponse({ description: 'Invalid code provided', type: InternNotVerifiedAuthResponseDto })
  async verifyIntern(@Body() verificationDto: InternVerifyDto) {
    return await this.internService.verifyIntern(verificationDto);
  }

  @Get('/current/reactions')
  @HttpCode(200)
  @UseGuards(InternGuard)
  @ApiOperation({ summary: '[I] Get current intern reactions' })
  @ApiOkResponse({ description: 'List of current intern reactions', type: CurrentInternReactionsResponseDto })
  @ApiUnauthorizedResponse({ description: 'Unauthorized' })
  async getCurrentReactions(@CurrentInternUser() user: FullInternUser) {
    const reactions = await this.internService.getInternReactions(user.id);
    return { reactions };
  }

  @Get('/current/chats')
  @UseGuards(InternGuard)
  @ApiOperation({ summary: '[I] Get current intern chats' })
  @ApiOkResponse({ description: 'List of current intern chats' })
  @ApiUnauthorizedResponse({ description: 'Unauthorized' })
  async getCurrentChats(@CurrentInternUser() user: FullInternUser) {
    return await this.chatService.getInternChatrooms(user.id);
  }

  @Get('/current/invitations')
  @HttpCode(200)
  @UseGuards(InternGuard)
  @ApiOperation({ summary: '[I] Get current intern invitations' })
  @ApiOkResponse({ description: 'List of current intern invitations', type: CurrentInternReactionsResponseDto })
  @ApiUnauthorizedResponse({ description: 'Unauthorized' })
  async getCurrentInvitations(@CurrentInternUser() user: FullInternUser) {
    const invitations = await this.internService.getInternInvitations(user.id);
    return { invitations };
  }

  @Get('/current')
  @HttpCode(200)
  @UseGuards(InternGuard)
  @ApiOperation({ summary: '[I] Get current intern by token' })
  @ApiOkResponse({ description: 'OK!' })
  @ApiUnauthorizedResponse({ description: 'Unauthorized', type: UnauthorizedException })
  async getCurrent(@CurrentInternUser() user: FullInternUser) {
    return user;
  }

  @Get('/:id')
  @UseGuards(CompanyGuard)
  @ApiBearerAuth('Bearer')
  @ApiNotFoundResponse({ description: 'Intern not found' })
  @ApiUnauthorizedResponse({ description: 'Invalid JWT signature' })
  @ApiOkResponse({ description: 'Get an intern', type: InternResponseDto })
  @ApiOperation({ summary: '[C] Get intern by id', description: 'Get intern by id' })
  async getIntern(@Param('id') id: number) {
    return await this.internService.get(id);
  }

  @Put('/:id')
  @UseGuards(InternGuard)
  @ApiBearerAuth('Bearer')
  @ApiOperation({ summary: '[I] Update intern data', description: 'Get interns list' })
  @ApiOkResponse({ description: 'Intern updated', type: InternResponseDto })
  @ApiNotFoundResponse({ description: 'Intern not found' })
  @ApiUnauthorizedResponse({ description: 'Invalid JWT signature' })
  async updateIntern(
    @Param('id') internId: number,
    @Body() internData: InternUpdateDto,
    @CurrentInternUser() intern: FullInternUser,
  ) {
    if (internId !== intern.id) {
      throw new ForbiddenException('You have no permission to this resource');
    }

    return await this.internService.update(internId, internData);
  }
}
