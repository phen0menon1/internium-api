import { BadRequestException, Injectable, UnauthorizedException } from '@nestjs/common';
import { VerificationMessage } from '@prisma/client';
import { PrismaService } from '@/modules/prisma/prisma.service';
import { JwtService } from '@/modules/jwt/jwt.service';

const code = '123456';

@Injectable()
export class SmsService {
  constructor(private readonly prisma: PrismaService, private readonly jwtService: JwtService) {}

  async getVerificationMsgById(id: number): Promise<VerificationMessage> {
    return await this.prisma.verificationMessage.findUnique({ where: { id } });
  }

  async createVerificationCode(phone: string) {
    const verificationMsg = await this.prisma.verificationMessage.create({
      data: {
        code,
        phone,
      },
    });
    return { code: verificationMsg.code, blockTime: 60 };
  }

  async validateVerificationCode(phone: string, code: string): Promise<number> {
    try {
      const { id } = await this.prisma.verificationMessage.findFirst({
        where: {
          phone,
          code,
          verified: false,
        },
      });
      await this.prisma.verificationMessage.update({ where: { id }, data: { verified: true } });
      return id;
    } catch {
      throw new BadRequestException('Invalid code provided');
    }
  }

  async generateVerificationJwt(id: number) {
    return this.jwtService.generateAccessToken({ entity: 'verification', id }, { expiresIn: '15m' });
  }

  async validateVerificationJwt(jwt: string): Promise<number> {
    try {
      const { id } = this.jwtService.verifyToken(jwt);
      return id;
    } catch {
      throw new UnauthorizedException('Invalid JWT signature');
    }
  }
}
