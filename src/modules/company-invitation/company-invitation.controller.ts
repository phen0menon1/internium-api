import { CurrentCompanyUser } from '@/decorators/current-company-user.decorator';
import { CompanyGuard } from '@/guards/company.guard';
import { Body, Controller, Get, Post, Query, UseGuards } from '@nestjs/common';
import { ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { CompanyUser } from '@prisma/client';
import {
  CreateInvitationResponseDto,
  InviteCompanyUserDto,
  VerifyInvitationResponseDto,
} from './company-invitation.dto';
import { CompanyInvitationService } from './company-invitation.service';

@Controller('company-invitations')
@ApiTags('Company Invitations')
export class CompanyInvitationController {
  constructor(private readonly companyInvitationService: CompanyInvitationService) {}

  @Post('/')
  @UseGuards(CompanyGuard)
  @ApiOperation({
    summary: '[C] Create an invination',
    description: 'Creates an invitation token and sends and url to the specified email',
  })
  @ApiResponse({ status: 201, description: 'Invitation has been sent', type: CreateInvitationResponseDto })
  @ApiResponse({ status: 400, description: 'Email is already used' })
  async create(@CurrentCompanyUser() companyOwner: CompanyUser, @Body() userData: InviteCompanyUserDto) {
    const created = await this.companyInvitationService.inviteCompanyUser(companyOwner.companyId, userData);
    return { success: created };
  }

  @Get('/verify')
  @ApiOperation({
    summary: 'Verify invitation',
    description: "It can be used to verify the registration token to allow further steps in the user's register",
  })
  @ApiResponse({ status: 200, description: 'Invitation exists', type: VerifyInvitationResponseDto })
  @ApiResponse({ status: 404, description: "Invitation doesn't exist" })
  async verify(@Query('token') token: string) {
    await this.companyInvitationService.verifyInvitation(token);
    return { valid: true };
  }
}
