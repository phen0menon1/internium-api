import { forwardRef, Module } from '@nestjs/common';
import { CompanyModule } from '../company/company.module';
import { JwtModule } from '../jwt/jwt.module';
import { KafkaModule } from '../kafka/kafka.module';
import { PasswordModule } from '../password/password.module';
import { PrismaModule } from '../prisma/prisma.module';
import { CompanyInvitationController } from './company-invitation.controller';
import { CompanyInvitationService } from './company-invitation.service';

@Module({
  imports: [JwtModule, forwardRef(() => CompanyModule), PrismaModule, PasswordModule, KafkaModule],
  exports: [CompanyInvitationService],
  providers: [CompanyInvitationService],
  controllers: [CompanyInvitationController],
})
export class CompanyInvitationModule {}
