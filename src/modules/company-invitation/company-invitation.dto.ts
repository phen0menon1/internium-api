import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsEmail, MaxLength } from 'class-validator';

export class InviteCompanyUserDto {
  @IsEmail()
  @IsNotEmpty()
  @MaxLength(255)
  @ApiProperty({ description: 'Email' })
  email: string;
}

export class CreateInvitationResponseDto {
  success: boolean;
}

export class VerifyInvitationResponseDto {
  valid: boolean;
}
