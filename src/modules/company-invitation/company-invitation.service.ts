import { MailerService } from '@nestjs-modules/mailer';
import { BadRequestException, Injectable } from '@nestjs/common';
import { JsonWebTokenError } from 'jsonwebtoken';
import { PrismaService } from 'src/modules/prisma/prisma.service';
import { JwtService } from '../jwt/jwt.service';
import { KafkaProducerService } from '../kafka/kafka-producer.provider';
import { InviteCompanyUserDto } from './company-invitation.dto';

@Injectable()
export class CompanyInvitationService {
  constructor(
    private readonly prisma: PrismaService,
    private readonly jwtService: JwtService,
    private readonly kafkaProducer: KafkaProducerService,
  ) {}

  async get(id: number) {
    return await this.prisma.companyInvitation.findUnique({ where: { id }, rejectOnNotFound: false });
  }

  async delete(id: number) {
    return await this.prisma.companyInvitation.delete({ where: { id } });
  }

  async inviteCompanyUser(companyId: number, data: InviteCompanyUserDto) {
    const emailAlreadyExists = await this.prisma.companyUser.findUnique({
      where: { email: data.email },
      rejectOnNotFound: false,
    });
    if (emailAlreadyExists) {
      throw new BadRequestException({ message: 'User with provided email already exists', error: 'user_exists' });
    }
    const invitation = await this.prisma.companyInvitation.create({
      data: { ...data, companyId },
      include: {
        company: true,
      },
    });
    const jwt = this.jwtService.generateAccessToken({ entity: 'companyInvitation', id: invitation.id });

    this.kafkaProducer.produce({
      topic: 'send-invitation',
      messages: [
        {
          value: JSON.stringify({ email: data.email, companyName: invitation.company.name }),
          headers: {
            accessToken: jwt,
          },
        },
      ],
    });

    return jwt;
  }

  async verifyInvitation(token: string) {
    try {
      this.jwtService.verifyToken(token);
      return true;
    } catch (error) {
      if (error instanceof JsonWebTokenError) {
        throw new BadRequestException({ error: 'invalid_token', message: 'Invalid token provided' });
      }
    }
  }
}
