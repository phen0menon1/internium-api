import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { APP_FILTER } from '@nestjs/core';
import config from './configs/config';
import { CompanyModule } from './modules/company/company.module';
import { CompanyUserModule } from './modules/company-user/company-user.module';
import { InternModule } from './modules/intern/intern.module';
import { CompanyInvitationModule } from './modules/company-invitation/company-invitation.module';
import { JwtModule } from './modules/jwt/jwt.module';
import { PrismaModule } from './modules/prisma/prisma.module';
import { PasswordModule } from './modules/password/password.module';
import { JwtStrategy } from './jwt.strategy';
import { PrismaClientExceptionFilter } from './filters/prisma-client-exception.filter';
import { VacancyModule } from './modules/vacancy/vacancy.module';
import { SpecializationModule } from './modules/specialization/specialization.module';
import { InternReactionsModule } from './modules/intern-reactions/intern-reactions.module';
import { AvatarModule } from './modules/avatar/avatar.module';
import { ChatModule } from './modules/chat/chat.module';
import { VacancyInvitationModule } from './modules/vacancy-invitation/vacancy-invitation.module';
import { VacancyAnnouncementModule } from './modules/vacancy-announcement/vacancy-announcement.module';
import { EntityManager } from './entity-manager';
import { KafkaModule } from './modules/kafka/kafka.module';

const PrismaClientExceptionAppFilter = {
  provide: APP_FILTER,
  useClass: PrismaClientExceptionFilter,
};

@Module({
  imports: [
    ConfigModule.forRoot({ isGlobal: true, load: [config] }),
    KafkaModule,
    PrismaModule,
    PasswordModule,
    JwtModule,
    CompanyModule,
    InternModule,
    CompanyUserModule,
    CompanyInvitationModule,
    VacancyModule,
    SpecializationModule,
    InternReactionsModule,
    AvatarModule,
    ChatModule,
    VacancyInvitationModule,
    VacancyAnnouncementModule,
  ],
  controllers: [],
  providers: [JwtStrategy, PrismaClientExceptionAppFilter, EntityManager],
  exports: [JwtStrategy, EntityManager],
})
export class AppModule {}
