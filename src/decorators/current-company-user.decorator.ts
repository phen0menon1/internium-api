import { createParamDecorator, ExecutionContext } from '@nestjs/common';
import { CompanyUserFull } from '../types';

export const CurrentCompanyUser = createParamDecorator((data: unknown, ctx: ExecutionContext) => {
  const request = ctx.switchToHttp().getRequest();
  const u = request.user as CompanyUserFull;
  return u;
});
