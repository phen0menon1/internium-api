import { createParamDecorator, ExecutionContext } from '@nestjs/common';
import { FullInternUser } from '../types';

export const CurrentInternUser = createParamDecorator((_: unknown, ctx: ExecutionContext) => {
  const request = ctx.switchToHttp().getRequest();
  const user = request.user as FullInternUser;
  return user;
});
