import { createParamDecorator, ExecutionContext } from '@nestjs/common';
import { JwtValidEntity } from '../types';

export const CurrentUser = createParamDecorator((data: unknown, ctx: ExecutionContext) => {
  const request = ctx.switchToHttp().getRequest();
  const user = request.user as JwtValidEntity;
  return user;
});
